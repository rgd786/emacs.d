
;; DIRED

;; C-x C-d -> dired-recent - list of last dirs visited
;; 'P' for 'peep-dired' (view file at point in other window temporarily
;; '/...' for filtering
;; S-M2 for sorting menu
;; <tab>/<btab> - on dir, show subtree  

; from https://www.reddit.com/r/emacs/comments/byhf6w/file_management_with_dired_in_emacs/ 
(autoload 'dired-jump "dired-x"
  "Jump to Dired buffer corresponding to current buffer." t)
    
(autoload 'dired-jump-other-window "dired-x"
  "Like \\[dired-jump] (dired-jump) but in other window." t)
(require 'dired-x)
;;(require 'dired+)
(require 'dired-aux) 
(require 'wdired)
(require 'image-dired)
;(use-package casual-dired :ensure t)
;;(require 'casual-dired)

(add-hook 'dired-mode-hook 'hl-line-mode)
;(add-hook 'dired-mode-hook 'context-menu-mode)
(add-hook 'dired-mode-hook 'dired-async-mode)
(add-hook
 'dired-mode-hook
 (lambda ()
   (setq-local mouse-1-click-follows-link 'double)))
   
(define-key dired-mode-map (kbd "M-o") #'dired-omit-mode)
(define-key dired-mode-map (kbd "E") #'wdired-change-to-wdired-mode)
;(define-key dired-mode-map (kbd "C-o") #'casual-dired-tmenu)
;(define-key dired-mode-map (kbd "s") #'casual-dired-sort-by-tmenu)
(define-key dired-mode-map (kbd "M-n") #'dired-next-dirline)
(define-key dired-mode-map (kbd "M-p") #'dired-prev-dirline)
(define-key dired-mode-map (kbd "]") #'dired-next-subdir)
(define-key dired-mode-map (kbd "[") #'dired-prev-subdir)
(define-key dired-mode-map (kbd "S-<mouse-1>") #'browse-url-of-dired-file)
; define-key dired-mode-map (kbd "S-<mouse-2>") #'casual-dired-sort-menu) ; can get menu but doesn't work, even after 'g' refresh

(define-key image-dired-thumbnail-mode-map (kbd "n") #'image-dired-display-next)
(define-key image-dired-thumbnail-mode-map (kbd "p") #'image-dired-display-previous) 


; putting recent dirs edited in dired into recent file list:
; https://www.emacswiki.org/emacs/RecentFiles#h5o-13
;
(eval-after-load "recentf"
'(progn
(defun recentf-track-opened-file ()
  "Insert the name of the dired or file just opened or written into the recent list."
  (let ((buff-name (or buffer-file-name (and (derived-mode-p 'dired-mode) default-directory))))
    (and buff-name
         (recentf-add-file buff-name)))
  ;; Must return nil because it is run from `write-file-functions'.
  nil)

(defun recentf-track-closed-file ()
  "Update the recent list when a file or dired buffer is killed.
That is, remove a non kept file from the recent list."
  (let ((buff-name (or buffer-file-name (and (derived-mode-p 'dired-mode) default-directory))))
    (and buff-name
         (recentf-remove-if-non-kept buff-name))))

(add-hook 'dired-after-readin-hook 'recentf-track-opened-file)))

(defun w32-context-menu (filename)
  (progn
   (message filename)
   (start-process-shell-command "ShellContextMenu" "*ShellContextMenu*" filename )))

(defun w32-context-menu-dired-get-filename (event)
  (interactive "e")
   ;; moves point to clicked row
   (mouse-set-point event)
   (w32-context-menu (concat "ShellContextMenu.exe" " 1 " "\"" (replace-regexp-in-string "/" "\\" (dired-get-filename) t t) "\"")))

;; Bind RMB                 
(define-key dired-mode-map [mouse-3]  'w32-context-menu-dired-get-filename)


;; https://github.com/clemera/dired-git-info:
; - eh, ok but *really* wide, don't see time of mod, would rather see that than commit message 
; 
(setq dgi-auto-hide-details-p nil)
(setq dgi-commit-message-format "%cr (%cn)")
(with-eval-after-load 'dired
  (define-key dired-mode-map ")" 'dired-git-info-mode))
;(add-hook 'dired-after-readin-hook 'dired-git-info-auto-enable)

;; https://github.com/conao3/dired-git.el:
;(add-hook 'dired-mode-hook 'dired-git-mode)
;(add-hook 'dired-mode-hook 'dired-git-mode) - relies on some version of git don't have and xargs etc... not so good on windows

(setq ls-lisp-use-insert-directory-program t)

(setq dired-switches-in-mode-line 'as-is) ; casual-dired doesn't seem to play with mode line well; or dired-sort-set-modeline broke 
