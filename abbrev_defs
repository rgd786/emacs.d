;;-*-coding: utf-8;-*-
(define-abbrev-table 'c++-mode-abbrev-table
  '(
    ("chekcing" "checking" nil :count 0)
   ))

(define-abbrev-table 'global-abbrev-table
  '(
    ("bg" "background" nil :count 1)
    ("zcapi" "Control API" nil :count 0)
    ("zel" ".*\\.el" xah-abbrev-h-f :count 2)
    ("zev" "environment variable" nil :count 2)
    ("zfaq" "frequently asked questions" nil :count 11)
    ("zhapi" "HarmonyAPI" nil :count 6)
    ("zie" "Internet Explorer" nil :count 1)
    ("zomg" "😂" nil :count 5)
    ("zredate" "\\([0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]\\)" nil :count 0)
   ))

(define-abbrev-table 'org-mode-abbrev-table
  '(
    ("autocorrect" "autocorrect" nil :count 0)
    ("comletion" "completion" nil :count 1)
    ("po" "PO" nil :count 7)
    ("tle" "#+title: @@
#+roam_tags:
#+STARTUP: showall
#+OPTIONS: \\\\n:t\"
" nil :count 1)
    ("totalling" "totaling" nil :count 0)
    ("zsrc" "#+begin_src
@@
#+end_src
" nil :count 6)
   ))

