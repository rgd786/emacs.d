;; emacs.comp-vert.el - vertico/consult/marginalia/embark/orderless
;;
;; Emacs selection and completion
;;
;;- (see https://utcc.utoronto.ca/~cks/space/blog/programming/EmacsUnderstandingCompletion)
;; - minibuffer completion - when making selections (file to visit, buffer to switch to, get help on, etc.)
;; - vertico  (previously helm/ivy/etc)
;; - in buffer on demand completion - at-point (expand what you're typing) - M-TAB or M-/ (Dabbrev)
;; - corfu (in-buffer)
;;
;; (later look at lspmode or eglot
;;  - for language server protocol clients to get language-specific data for completion etc)
;; look at mct - https://github.com/protesilaos/mct by prot (not developed any more)



;;(require 'general) ;; general key-binding handling
(use-package general
  :ensure t
)
;; vertico - vertical minibuffer interface
;;
(use-package vertico
  :custom
  (vertico-count 14)                        ; Number of candidates to display
  (vertico-resize nil)                      ; was t  -- might lead to issues
  (vertico-cycle t)                         ; Go from last to first candidate and first to last (cycle)?
  :general
   (:keymaps 'vertico-map
    "<tab>" #'vertico-insert                ; Insert selected candidate into text area
    "<escape>" #'minibuffer-keyboard-quit   ; Close minibuffer
                                            ;; NOTE 2022-02-05: Cycle through candidate groups
    "C-M-n" #'vertico-next-group
    "C-M-p" #'vertico-previous-group)
  :config
  (setq completion-styles '(orderless partial-completion emacs22 substring basic))
  (setq read-file-name-completion-ignore-case t
        read-buffer-completion-ignore-case t
        completion-ignore-case t)
  (vertico-mode))

(use-package savehist
  :init
  (savehist-mode))


(use-package vertico-repeat
  :after vertico
  :load-path "~/.emacs.d/lisp/"
  :bind ( :map vertico-map
          ("M-g r" . vertico-repeat-select)
          ("M-R" . vertico-repeat)
		  ("M-P" . vertico-repeat-previous)
		  ("M-N" . vertico-repeat-next)
		  ("S-<prior>" . vertico-repeat-previous)
		  ("S-<next>" . vertico-repeat-next)
        )
  :config
   (add-to-list 'savehist-additional-variables 'vertico-repeat-history)
  :hook
   (minibuffer-setup . vertico-repeat-save)

)

;(use-package vertico-repeat
;  ;; :straight (vertico-repeat :host github :repo "emacs-straight/vertico" :files ("extensions/vertico-repeat.el"))
;  :after (vertico savehist)
;  :bind
;  ("M-g r" . vertico-repeat-select)
;  :config
;  (add-to-list 'savehist-additional-variables 'vertico-repeat-history)
;  :hook
;  (minibuffer-setup . vertico-repeat-save))

;(keymap-global-set "M-R" #'vertico-repeat)
;(keymap-set vertico-map "M-P" #'vertico-repeat-previous)
;(keymap-set vertico-map "M-N" #'vertico-repeat-next)
;(keymap-set vertico-map "S-<prior>" #'vertico-repeat-previous)
;(keymap-set vertico-map "S-<next>" #'vertico-repeat-next)


;; Marginalia - minibuffer annotations
;;
(use-package marginalia
  :general
  (:keymaps 'minibuffer-local-map
   "M-A" 'marginalia-cycle)
  :custom
  (marginalia-max-relative-age 0)
  (marginalia-margin-threshold 400)
  (marginalia-align 'right)
  (marginalia-annotators '(marginalia-annotators-heavy marginalia-annotators-light nil))
  :init
  (marginalia-mode))  


(use-package emacs
  :init
  ;; Add prompt indicator to `completing-read-multiple'.
  ;; We display [CRM<separator>], e.g., [CRM,] if the separator is a comma.
  ;;
  (defun crm-indicator (args)
    (cons (format "[CRM%s] %s"
                  (replace-regexp-in-string
                   "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" ""
                   crm-separator)
                  (car args))
          (cdr args)))
  (advice-add #'completing-read-multiple :filter-args #'crm-indicator)

  ;; Do not allow the cursor in the minibuffer prompt
  (setq minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

  ;; Emacs 28: Hide commands in M-x which do not work in the current mode.
  ;; Vertico commands are hidden in normal buffers.
  (setq read-extended-command-predicate
         #'command-completion-default-include-p)

  ;; Enable recursive minibuffers
  (setq enable-recursive-minibuffers t))


;(defun +vertico-restrict-to-matches ()
;  (interactive)
;  (let ((inhibit-read-only t))
;    (goto-char (point-max))
;    (insert " ")
;    (add-text-properties (minibuffer-prompt-end) (point-max)
;                         '(invisible t read-only t cursor-intangible t rear-nonsticky t))))
;
;(define-key vertico-map (kbd "S-SPC") #'+vertico-restrict-to-matches)
  
;  (defun vertico-resize--minibuffer ()
;  (add-hook 'window-size-change-functions
;            (lambda (win)
;              (let ((height (window-height win)))
;                (when (/= (1- height) vertico-count)
;                  (setq-local vertico-count (1- height))
;                  (vertico--exhibit))))
;            t t))
;
;(advice-add #'vertico--setup :before #'vertico-resize--minibuffer)


;; vertico truncate - https://github.com/jdtsmith/vertico-truncate
;;
;(require 'vertico-truncate)
;(vertico-truncate-mode 1)


;; vertico long candidate handling  
;;
(defvar +vertico-current-arrow t)

(cl-defmethod vertico--format-candidate :around
  (cand prefix suffix index start &context ((and +vertico-current-arrow
                                                 (not (bound-and-true-p vertico-flat-mode)))
                                            (eql t)))
  (setq cand (cl-call-next-method cand prefix suffix index start))
  (if (bound-and-true-p vertico-grid-mode)
      (if (= vertico--index index)
          (concat #("▶" 0 1 (face vertico-current)) cand)
        (concat #("_" 0 1 (display " ")) cand))
    (if (= vertico--index index)
        (concat
         #(" " 0 1 (display (left-fringe right-triangle vertico-current)))
         cand)
      cand)))



  


;; orderless - a flexible completion-style with multi-component matching
;;    https://github.com/oantolin/orderless
;;
;(use-package orderless
;  :custom
;  (completion-styles '(orderless))      ; Use orderless
;  (completion-category-defaults nil)    ; I want to be in control!
;  (completion-category-overrides
;   '((file (styles basic-remote ; For `tramp' hostname completion with `vertico'
;                   orderless)))))
(use-package orderless
  :ensure t
  :custom
  (completion-styles '(orderless partial-completion emacs22 substring basic))
  (completion-category-overrides '((file (styles basic partial-completion orderless)))))


(defun without-if-bang (pattern _index _total)
    (cond
     ((equal "!" pattern)
      '(orderless-literal . ""))
     ((string-prefix-p "!" pattern)
      `(orderless-without-literal . ,(substring pattern 1)))))
;; https://emacs.stackexchange.com/questions/78808/filter-out-command-completions-with-specific-prefixes

(setq orderless-style-dispatchers '(without-if-bang))


;; all-the-icons icons for minibuffer candidates
;;
(use-package all-the-icons-completion
  :after (marginalia all-the-icons)
  :hook (marginalia-mode . all-the-icons-completion-marginalia-setup)
  :init
  (all-the-icons-completion-mode))


(defun embark-act-noquit ()
  "Run action but don't quit the minibuffer afterwards."
  (interactive)
  (let ((embark-quit-after-action nil))
    (embark-act)))


;;;;;;;;;;;; embark
;;
(use-package embark
  :ensure t

  :bind
  (("C-." . embark-act)         ;; pick some comfortable binding (maybe M-. ?)
   ("C-;" . embark-dwim)        ;; good alternative: M-.
   ("C->" . embark-act-noquit)  
   ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'

  :init

  ;; Optionally replace the key help with a completing-read interface
  (setq prefix-help-command #'embark-prefix-help-command)

  ;; Show the Embark target at point via Eldoc.  You may adjust the Eldoc
  ;; strategy, if you want to see the documentation from multiple providers.
  (add-hook 'eldoc-documentation-functions #'embark-eldoc-first-target)
  ;; (setq eldoc-documentation-strategy #'eldoc-documentation-compose-eagerly)

  :config
  ;; seems already default: (setq embark-prompter 'embark-completing-read-prompter)
  
  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none))))
)


;; Consult users will also want the embark-consult package.
(use-package embark-consult
  :ensure t ; only need to install it, embark loads it after consult if found
  :hook
  (embark-collect-mode . consult-preview-at-point-mode)
)

;; nice but think will try just using embark and not which-key -
;; start command (C-x e.g.) then C-h for embark help on command.
;;
;; (defun embark-which-key-indicator ()
;;   "An embark indicator that displays keymaps using which-key.
;; The which-key help message will show the type and value of the
;; current target followed by an ellipsis if there are further
;; targets."
;;   (lambda (&optional keymap targets prefix)
;;     (if (null keymap)
;;         (which-key--hide-popup-ignore-command)
;;       (which-key--show-keymap
;;        (if (eq (plist-get (car targets) :type) 'embark-become)
;;            "Become"
;;          (format "Act on %s '%s'%s"
;;                  (plist-get (car targets) :type)
;;                  (embark--truncate-target (plist-get (car targets) :target))
;;                  (if (cdr targets) "…" "")))
;;        (if prefix
;;            (pcase (lookup-key keymap prefix 'accept-default)
;;              ((and (pred keymapp) km) km)
;;              (_ (key-binding prefix 'accept-default)))
;;          keymap)
;;        nil nil t (lambda (binding)
;;                    (not (string-suffix-p "-argument" (cdr binding))))))))

;; (setq embark-indicators
;;   '(embark-which-key-indicator
;;     embark-highlight-indicator
;;     embark-isearch-highlight-indicator))


;; ;;;;;;;;; corfu - COmpletion in Region FUnction
;; enhances in-buffer completion with popup
;; minimalistic counterpart to Veritco's minibuffer UI
;;
;; need corfu-terminal for -nw emacs
;;
(use-package corfu
  :ensure t 
  ;; Optional customizations

  :hook
  (eval-expression-minibuffer-setup . corfu-mode)    ;;https://www.patrickdelliott.com/emacs.d/#org542b12b
  
  :custom
  (corfu-cycle t)                     ;; Enable cycling for `corfu-next/previous'
;  (corfu-auto t)                      ;; Enable auto completion
;  (corfu-auto-prefix 3)               ;; enable auto completion (Gavin Freeborn)
;  (corfu-auto-delay 15)               ;; 
;  (corfu-popupinfo-delay '(2.0 . 0.5))  ;; M-t to show/hide popup (initial . subsequent) delay for candidates
  (corfu-separator ?\s)               ;; Orderless field separator
  (corfu-quit-at-boundary 'separator) ;; Gavin Freeborn (or nil or t)
  (corfu-echo-documentation 0.25)     ;; Gavin Freeborn
; (corfu-quit-no-match nil)           ;; Never quit, even if there is no match
  (corfu-preview-current 'insert)     ;; Gavin Freeborn
  (corfu-preselect 'prompt)           ;; Preselect the prompt
  (corfu-on-exact-match nil)          ;; Configure handling of exact matches
  (corfu-scroll-margin 6)             ;; Use scroll margin
  (corfu-count 12)                    ;; max selections to show
  (completion-cycle-threshold nil)    ;; always show all candidates
  (tab-always-indent 'complete)       ;; Enable indentation+completion using the TAB key.
;  (corfu-min-width 40)
;  (corfu-max-width corfu-min-width)
  :bind (:map corfu-map
              ("SPC" . corfu-insert-separator)   ;; https://www.patrickdelliott.com/emacs.d/#org542b12b
              ("M-SPC" . corfu-insert-separator) ;; alt-space to enter sep to use orderless matching
            ; ("RET" . nil)
              ("TAB" . corfu-next) ;; too used to TAB completing but try to get used to RET to select
              ([tab] . corfu-next)
            ;  (<escape> . corfu-quit)
              ("S-TAB" . corfu-previous)
              ([backtab] . corfu-previous)
              ("M-g" . corfu-info-documentation)      ; default
              ("M-h" . corfu-info-location)           ;  [default] 'peek declaration'
              ("S-<return>" . corfu-insert))

  ;; Enable Corfu only for certain modes.
  ;; :hook ((prog-mode . corfu-mode)
  ;;        (shell-mode . corfu-mode)
  ;;        (eshell-mode . corfu-mode))
  ;; Recommended: Enable Corfu globally.  This is recommended since Dabbrev can
  ;; be used globally (M-/).  See also the customization variable
  ;; `global-corfu-modes' to exclude certain modes.
  
  :init
  (global-corfu-mode)
  (corfu-history-mode)
  (corfu-popupinfo-mode)
)

;; Enable Corfu more generally for every minibuffer, as long as no other
;; completion UI is active. If you use Mct or Vertico as your main minibuffer
;; completion UI. From
;; https://github.com/minad/corfu#completing-with-corfu-in-the-minibuffer
(defun corfu-enable-always-in-minibuffer ()
  "Enable Corfu in the minibuffer if Vertico/Mct are not active."
  (unless (or (bound-and-true-p mct--active) ; Useful if I ever use MCT
              (bound-and-true-p vertico--input))
    (setq-local corfu-auto nil)       ; Ensure auto completion is disabled
    (corfu-mode 1)))
(add-hook 'minibuffer-setup-hook #'corfu-enable-always-in-minibuffer 1)


(use-package kind-icon
  :ensure t
  :after corfu
  :custom
  (kind-icon-use-icons t)
  (kind-icon-default-face 'corfu-default) ; Have background color be the same as `corfu' face background
  (kind-icon-blend-background nil)  ; Use midpoint color between foreground and background colors ("blended")?
  (kind-icon-blend-frac 0.08)

  ;; NOTE 2022-02-05: `kind-icon' depends `svg-lib' which creates a cache
  ;; directory that defaults to the `user-emacs-directory'. Here, I change that
  ;; directory to a location appropriate to `no-littering' conventions, a
  ;; package which moves directories of other packages to sane locations.
;  (svg-lib-icons-dir (no-littering-expand-var-file-name "svg-lib/cache/")) ; Change cache dir

  :config
  (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter) ; Enable `kind-icon'
  (add-hook 'kb/themes-hooks #'(lambda () (interactive) (kind-icon-reset-cache)))
)


(global-unset-key (kbd "M-c")) ;; never upcase a word but will use capfs
;
(use-package cape
  :hook ((emacs-lisp-mode . rgd/cape-capf-setup-elisp)
         (org-mode . rgd/cape-capf-setup-org)
         (eshell-mode . rgd/cape-capf-setup-eshell)
         (git-commit-mode . rgd/cape-capf-setup-git-commit)
         (text-mode . rgd/cape-capf-setup-text)
         )
  :general (:prefix "M-c"
                    "p" 'completion-at-point
                    "t" 'complete-tag
                    "d" 'cape-dabbrev
                    "f" 'cape-file
                    "k" 'cape-keyword
;;                    "s" 'cape-symbol   renamed cape-elisp-symbol by Oct 23
                    "a" 'cape-abbrev
                    "l" 'cape-line                ;; think causes problems if in capf use manually
                    "w" 'cape-dict
                    ";" 'cape-emoji
                    "s" 'cape-elisp-symbol
                    "e" 'cape-elisp-block
                    "&" 'cape-sgml)
  :custom
  (cape-dabbrev-min-length 3)
  
  :config ;; :init
  (defun rgd/cape-capf-ignore-keywords-elisp (cand)
     "ignore keywords with forms that begin with \":\" (e.g. :history)."
     (or (not (keywordp cand))
         (eq (char-after (car completion-in-region--data)) ?:)))
  (defun rgd/cape-capf-setup-elisp ()
    "replace the default 'elisp-completion-at-point' capf. Doing it this way will prevent disrupting the
     addition of other capfs (e.g. merely setting the variable entirely, or adding to list)."
    (setf (elt (cl-member 'elisp-completion-at-point completion-at-point-functions) 0)
          #'elisp-completion-at-point)
    (add-to-list 'completion-at-point-functions #'cape-elisp-symbol) 
    (add-to-list 'completion-at-point-functions #'cape-file)
    ;; others? abbrev/dabbrev/tag/keyword/line/dict ?
    )
  (defun rgd/cape-capf-setup-org ()
    (require 'org-roam)
    (if (org-roam-file-p)
        (org-roam--register-completion-functions-h)
      (let (result)
        (dolist (element (list
                          (cape-capf-super #'cape-file))
                         result)
          (add-to-list 'completion-at-point-functions element)))
      ))

  (defun rgd/cape-capf-setup-eshell ()
    (let ((result))
      (dolist (element '(pcomplete-completions-at-point cape-file) result)
        (add-to-list 'completion-at-point-functions element))
      ))

  (defun rgd/cape-capf-setup-git-commit ()  ;; does this inherit text-mode capfs also?
    (general-define-key
     :keymaps 'local
     :states 'insert
     "<tab>" 'completion-at-point)
    (let ((result))
      (dolist (element '(cape-dabbrev cape-keyword) result)
        (add-to-list 'completion-at-point-functions element))))

  (defun rgd/cape-capf-setup-text ()
    (general-define-key
     :keymaps 'local
     :states 'insert
     "<tab>" 'completion-at-point)
    (let ((result))
      (dolist (element '(cape-emoji cape-dabbrev cape-dict cape-abbrev ) result)
        (add-to-list 'completion-at-point-functions element))))

;;  :config
  ;; for eshell
  ;; 
  ;; Silence then pcomplete capf, no errors or messages!
  (advice-add 'pcomplete-completions-at-point :around #'cape-wrap-silent)
  ;;
  ;; Ensure that pcomplete does not write to the buffer
  ;; and behaves as a pure `completion-at-point-function'.
  (advice-add 'pcomplete-completions-at-point :around #'cape-wrap-purify)

)

;;          ("M-c p" . completion-at-point) ;; capf
;;          ("M-c t" . complete-tag)        ;; etags
;;          ("M-c d" . cape-dabbrev)        ;; or dabbrev-completion
;;          ("M-c f" . cape-file)
;;          ("M-c k" . cape-keyword)
;;          ("M-c s" . cape-symbol) 
;;          ("M-c a" . cape-abbrev)
;;          ("M-c l" . cape-line)
;;          ("M-c w" . cape-dict)
;;          ("M-c ;" . cape-emoji)
;;          ("M-c h" . cape-history)     
;;          ("M-c &" . cape-sgml)
;;          ("M-c s" . cape-elisp-symbol)
;;          ("M-c e" . cape-elisp-block)
;;          ("M-c \\" . cape-tex)
;;          ("M-c _" . cape-tex)
;;          ("M-c ^" . cape-tex)
;;          ("M-c r" . cape-rfc1345)   

(setq vertico-multiform-commands
      '((consult-line
         posframe
         (vertico-posframe-poshandler . posframe-poshandler-frame-bottom-center)
         (vertico-posframe-border-width . 8)
         (vertico-posframe-fallback-mode . vertico-buffer-mode))
        (t posframe)))
		
(setq vertico-posframe-parameters '((left-fringe . 4) (right-fringe . 4)))
(setq vertico-posframe-poshandler #'posframe-poshandler-frame-bottom-center)

(use-package vertico-posframe
  :ensure t
  :init
  (vertico-posframe-mode 1)
;  (vertico-multiform-mode 1)
)

