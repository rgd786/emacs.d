;; prototype 'clean' emacs.conf.el
;; divide up into separate .el files:
;; 1) options
;; 2) UI setup
;; 3) machine-specific setup
;; 4) functions
;; 5) keymappings

(message "<root>/.emacs.d/emacs.conf.el - start")


;; from https://emacs.stackexchange.com/questions/539/how-do-i-measure-performance-of-elisp-code 
(defmacro with-timer (title &rest forms)
  "Run the given FORMS, counting the elapsed time.
A message including the given TITLE and the corresponding elapsed
time is displayed."
  (declare (indent 1))
  (let ((nowvar (make-symbol "now"))
        (body   `(progn ,@forms)))
    `(let ((,nowvar (current-time)))
       (message "%s..." ,title)
       (prog1 ,body
         (let ((elapsed
                (float-time (time-subtract (current-time) ,nowvar))))
           (message "%s... done (%.3fs)" ,title elapsed))))))


;; General Emacs options
;;
(setq emacs-opt-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.opt.el")))
(with-timer "emacs-opt"
  (load-file emacs-opt-el))

;; UI setup - frames, geometry, fonts, etc.
;;
(setq emacs-ui-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.ui.el")))
(with-timer "emacs-ui"
  (load-file emacs-ui-el))

;; Define local functions
;;
(setq emacs-fn-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.fn.el")))
(with-timer "emacs-fn"
 (load-file emacs-fn-el))


;; Machine-specific setup
;;
(setq emacs-spec-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.spec.el")))
(with-timer "emacs-spec"
(load-file emacs-spec-el))


;; Define key mappings
;;
(setq emacs-key-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.key.el")))
(with-timer "emacs-key"
  (load-file emacs-key-el))




(unless quick-switch-found ;; things to do if NOT in quick mode
  (progn


    ;; intialize package system and load packages
    ;;
    (setq emacs-pkg-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.pkg.el")))
 	(with-timer "emacs-pkg"
      (load-file emacs-pkg-el))


    ;; load org conf is -org
    ;;
    (if org-switch-found
      (progn
        (message "%s for org-mode config" org-conf-el)
        (if (file-exists-p org-conf-el)
	      (progn
	        (message "org-mode config %s"
					 (expand-file-name (concat config-root-path "/.emacs.d/org.conf.el")))
			(with-timer "org-conf"
	         (load-file org-conf-el))
		  )
		)
        (message "%s not found to load for org-mode configuration" org-conf-el)
      )
    )

    ;; load emacs server if not running as org mode emacs
    (unless org-switch-found 
      (progn
		 (require 'server)
        (message "Loading emacs server")   
        (load "server")

         (unless (and (fboundp 'server-running-p)
		      		  (server-running-p))
          (progn
         	(message "Starting emacs server.")
           	(server-start)))
            	(require 'edit-server)
            	(edit-server-start)
      )
    ) ;; done with non-org stuff


	
  )
) ;; done with non-quick/full-version stuff

(message "<root>/.emacs.d/emacs.conf.el - done")
