
;; MACHINE SPECIFIC SETUP

;; TODO - now that dropbox path is on d drive on work laptop - thanks to new SSD drive - several settings here
;;  (mostly for org) are not always "c:/rgd/dropbox/..." and some things are customized and stuck with hardcoded
;;  c: in emacs.custom.el - these should be removed from there and set on startup here per machine
;;
;;  fortune-dir
;;  locate-command
;;  org-agenda-files
;;  org-mobile-directory
;;  org-mobile-files
;;  org-mobile-inbox-for-pull

;; for now these will work on work laptop but not home or most other machines :(

;; and in dropbox/sys/fortune/fortune.ps1 has hard-coded c drive path

;; Location settings
(setq system-location "Unknown")

(cond
 ( (or (string-match "HOMEPC" (system-name))
	   (string-match "LV423" (system-name))
       (string-match "ROBSLAPTOP" (system-name))
       (string-match "LEGION17" (system-name))
	   )

   ;;   (home stuff)
  (setq system-location "Home")
  (setq calendar-latitude 41.35)  ;; put in site-start.el (?)
  (setq calendar-longitude -81.44)
  (setq calendar-location-name "Aurora")
  (setq calendar-location-airport "CLE")
  (setq calendar-time-zone -300) ;; GMT -5 5*60 = 300; 4*60=240
  (setq calendar-standard-time-zone-name "EST")
  (setq calendar-daylight-time-zone-name "EDT")  
  (setq calendar-daylight-time-offset 60)
  (setq sunshine-location "44202,US")

  ;; mail setup
  (setq user-full-name "Rob Davenport")
  (setq user-mail-address "rob.davenport@gmail.com")
  (setq send-mail-function 'smtpmail-send-it
		message-send-mail-function 'smtpmail-send-it
		smtpmail-starttls-credentials
		'(("smtp.gmail.com" 587 nil nil))
		smtpmail-auth-credentials
		(expand-file-name "~/.authinfo")
		smtpmail-default-smtp-server "smtp.gmail.com"
		smtpmail-smtp-server "smtp.gmail.com"
		smtpmail-smtp-service 587
		smtpmail-debug-info t)
  (require 'smtpmail)
  ;; got working at home after adding gnutls, cacert.pem (to sys\etc\ssl) and configuring gnutls-trustfiles in custom
  ;; and getting 16-digit app password via Google Account > Security > App Passwords > -generate - mail - computer -
  ;;   worklaptop - mail - windows computer - app password (2019-06-25):  jjiwmriuoahfcvgj
  ;; https://myaccount.google.com/security
  ;; https://support.google.com/mail/answer/185833?hl=en&visit_id=636967697266839668-79341861&rd=1
  ;; https://curl.haxx.se/docs/caextract.html
  ;; *** http://xn--9dbdkw.se/diary/how_to_enable_GnuTLS_for_Emacs_24_on_Windows/index.en.html  ****
  ;; https://emacs.stackexchange.com/questions/6105/how-to-set-proper-smtp-gmail-settings-in-emacs-in-order-to-be-able-to-work-with
  ;; could try using Yahoo as well
  
 )
 ( (or (string-match "ComposerDevD20" (system-name))
       (string-match "USWIC-L-0074462" (system-name))
	   (string-match "US-L-7002395" (system-name))
	   (string-match "US-L-7443770" (system-name))
	   (string-match "US-L-PF56W12A" (system-name))
	   (string-match "c64devrob" (system-name))
	   (string-match "w10devrob" (system-name))
	   (string-match "w10devrob24" (system-name))
	   (string-match "w10devrob2" (system-name))
	   (string-match "cebuildvb6" (system-name))
	   (string-match "HAPIDev10" (system-name))
	   (string-match "hapibuild10" (system-name))
	   (string-match "LV426" (system-name))
	   (string-match "LV426g" (system-name))
	   (string-match "hapibuild1" (system-name)))

  ;;   (work stuff)
  (setq system-location "Work")
  (setq calendar-latitude 41.4505)  ;; put in site-start.el (?)
  (setq calendar-longitude -81.5185)
  (setq calendar-location-name "Highland Hills")
  (setq calendar-location-airport "CAK")
  (setq calendar-time-zone -300) ;; GMT -5 5*60 = 300; 4*60=240
  (setq calendar-standard-time-zone-name "EST")
  (setq calendar-daylight-time-zone-name "EDT")  
  (setq calendar-daylight-time-offset 60)
  (setq sunshine-location "44122,US")

  (setq user-full-name "Rob Davenport")
  (setq user-mail-address "rob.davenport@us.abb.com")
  (setq send-mail-function 'smtpmail-send-it
		message-send-mail-function 'smtpmail-send-it
		smtpmail-starttls-credentials
		'(("smtp.gmail.com" 587 nil nil))
		smtpmail-auth-credentials
		(expand-file-name "~/.authinfo")
		smtpmail-default-smtp-server "smtp.gmail.com"
		smtpmail-smtp-server "smtp.gmail.com"
		smtpmail-smtp-service 587
		smtpmail-debug-info t)
  (require 'smtpmail)

  
 )
)

(message "\n\nSetting location for %s location computer\n" system-location)


;; Calendar settings - in the U.S.
;;  (setq calendar-daylight-savings-starts (calendar-nth-named-day 2 0 3 year))
;;  (setq calendar-daylight-savings-ends (calendar-nth-named-day 1 0 11 year))

(cond
 ((string-match "HOMEPC" (system-name))
  (defvar local-site-lisp-path "/usr/local/share/emacs/site-lisp")
  ;; (setq printer-name "CN2462H16F05RQ:") ;;; how to print at home? can't share network printer.
  ;; color theme?
  (defvar fortune-script-path "c:\\rgd\\dropbox\\sys\\fortune\\fortune-c.ps1")
  (defvar default-bell-sound-file (concat config-root-path "/.emacs.d/media/Ding.wav"))
  )
 ((string-match "ROBSLAPTOP" (system-name))
	(defvar local-site-lisp-path (concat user-home-directory "/.emacs.d/lisp"))
;;  (defvar local-site-lisp-path "c:\\users\\rgd\\.emacs.d\\lisp") -- should be the same as above
  (defvar local-find-exe "c:/rgd/dropbox/wbin/find.exe") ;; ??
  (defvar fortune-script-path "c:\\rgd\\dropbox\\sys\\fortune\\fortune-c.ps1")
  (defvar default-bell-sound-file (concat config-root-path "/.emacs.d/media/Ding.wav"))
  (setq epg-gpg-home-directory "c:/users/rgd/AppData/Roaming/gnupg")
  (setq epg-gpg-program "c:/program files (x86)/gnupg/bin/gpg.exe")
  (setq epg-gpgconf-program "c:/program files (x86)/gnupg/bin/gpgconf.exe")

  (set-proxy-nil)
  ;; PRINTING AT HOME
  (require 'printing)
  (pr-update-menus t)
  (setq printer-name "\\\\ROBSLAPTOP\\HP6700") ;;; how to print at home? can't share network printer.
  ;; or using lerup's PrintFile32
  ;; (setq printer-name nil) 
  (setq lpr-command "C:\\Program Files (x86)\\PrintFile\\prfile32.exe") 
  (setq lpr-switches '("/q")) 
  (setq ps-lpr-command "C:\\Program Files (x86)\\PrintFile\\prfile32.exe") 
  (setq ps-lpr-switches '("/q")) 
  ;; color theme?
  )
 ((string-match "LEGION17" (system-name))
	(defvar local-site-lisp-path (concat user-home-directory "/.emacs.d/lisp"))
;;  (defvar local-site-lisp-path "c:\\users\\rgd\\.emacs.d\\lisp") -- should be the same as above
  (defvar local-find-exe "c:/users/rgd/dropbox/wbin/find.exe") ;; ??
  (defvar fortune-script-path "c:\\users\\rgd\\dropbox\\sys\\fortune\\fortune-c.ps1")
  (defvar default-bell-sound-file (concat config-root-path "/.emacs.d/media/Ding.wav"))
  (setq epg-gpg-home-directory "c:/users/rgd/AppData/Roaming/gnupg")
  (setq epg-gpg-program "c:/program files (x86)/gnupg/bin/gpg.exe")
  (setq epg-gpgconf-program "c:/program files (x86)/gnupg/bin/gpgconf.exe")
  (setq everything-cmd "c:/sys/everything/es.exe")        ;; to let everything.el know where to find es.exe
  (setq personal-dict "c:\\sys\\hunspell\\personal.en")
  
  (set-proxy-nil)
  ;; PRINTING AT HOME
  (require 'printing)
  (pr-update-menus t)
  (setq printer-name "\\\\LEGION17\\HPENVY7800") ;;; how to print at home? can't share network printer.
  ;; or using lerup's PrintFile32
  ;; (setq printer-name nil) 
  (setq lpr-command "C:\\Program Files (x86)\\PrintFile\\prfile32.exe") 
  (setq lpr-switches '("/q")) 
  (setq ps-lpr-command "C:\\Program Files (x86)\\PrintFile\\prfile32.exe") 
  (setq ps-lpr-switches '("/q")) 
  ;; color theme?
  )
 ((string-match "LV426" (system-name)) ;; Lenovo ThinkStation D20 Ubuntu box
  (defvar local-site-lisp-path "/usr/local/share/emacs/site-lisp")
  (defvar local-find-exe "/usr/bin/find")
  (defvar fortune-script-path "~/Dropbox/sys/fortune/fortune.ps1")
  (defvar default-bell-sound-file "/usr/share/sounds/sound-icons/percussion-12.wav")
  ;;(setq printer-name "//uswic-s-file001/USCLEPX005-M_PS")
  ;; color theme?
  (set-proxy-abb)
  )
 ((string-match "US-L-PF56W12A" (system-name)) ; 2025 to ?

  (defvar local-site-lisp-path (concat user-home-directory "/.emacs.d/lisp"))
  (message "\nTODO: local find.exe - wbin version !!!\n")
  (defvar local-find-exe "c:/sys/msys64/usr/bin/find.exe") ;; "c:/sys/wbin/find.exe") ;; "c:/program files/Git/usr/bin/find.exe") ;; ??
  ;;(defvar fortune-script-path "c:\\rgd\\dropbox\\sys\\fortune\\fortune.ps1")
  (defvar default-bell-sound-file (concat config-root-path "/.emacs.d/media/Ding.wav"))
  (set-proxy-nil)
 ;; (setq epg-gpg-home-directory "c:/users/USRODAV2/AppData/Roaming/gnupg")
 ;; (setq epg-gpg-program "c:/program files (x86)/gnupg/bin/gpg.exe")
 ;; (setq epg-gpgconf-program "c:/program files (x86)/gnupg/bin/gpgconf.exe")
  (setq everything-cmd "c:/sys/everything/es.exe")        ;; to let everything.el know where to find es.exe
  (setq personal-dict "\"c:\\sys\\ezwinports\\share\\hunspell\\personal.en\"")

  (message "Personal dictionary location [%s]\n" personal-dict)
  )
 ((string-match "US-L-7443770" (system-name)) ; 2022? to ?

  (defvar local-site-lisp-path (concat user-home-directory "/.emacs.d/lisp"))
  ;;  (defvar local-site-lisp-path "c:/rgd/dropbox/.emacs.d/lisp")
  (defvar local-find-exe "c:/rgd/dropbox/wbin/find.exe") ;; ??
  (defvar fortune-script-path "c:\\rgd\\dropbox\\sys\\fortune\\fortune.ps1")
  (defvar default-bell-sound-file (concat config-root-path "/.emacs.d/media/Ding.wav"))
  (set-proxy-nil)
  (setq epg-gpg-home-directory "c:/users/USRODAV2/AppData/Roaming/gnupg")
  (setq epg-gpg-program "c:/program files (x86)/gnupg/bin/gpg.exe")
  (setq epg-gpgconf-program "c:/program files (x86)/gnupg/bin/gpgconf.exe")
  (setq everything-cmd "c:/sys/everything/es.exe")        ;; to let everything.el know where to find es.exe
  (setq personal-dict "\"c:\\sys\\ezwinports\\share\\hunspell\\personal.en\"")

  (message "Personal dictionary location [%s]\n" personal-dict)

  ;; PRINTING AT WORK - various options
  ;;
;;  (require 'printing)
;;  (pr-update-menus t) ;; the menus seem more for ps printing, not lpr printing - couldn't find item that would do (print-buffer)
  ;; doing M-x print-buffer workes with option (3) set for PrFile32.
  ;;
  ;; TODO: how to configure the printing settings for prfile32?
  ;;       by default it's finding the default printer (currently PDF-XChange6 and blapping 'permission denied')
  ;;       maybe set default printer back to the "US-P-CLEPX1032-" - that will probably work nicely but then have no control - it'll print right then.
  ;;       or figure out how to fix option 5
  ;;
  ;;
  ;; TODO - CONFIGURE PRINTING OPTIONS - printer alist CLEPX1032-, PDFPrinter, PDFEdit, PrFile32, etc... some for PS, other LPR

  ;; BE CAREFUL of having pr.exe and lpr.exe clones on your machine, if Emacs finds them it uses them and changes
  ;; its printing behavior (possibly preventing Windows-specific behavior)
  
;;  (setq lpr-headers-switches nil) ;; <-- !! or else get "page headers are not supported"

  ;; ;; (1) print to network printer
  ;;  (setq printer-name "//uswic-s-file001/USCLEPX005-M_PS")
  ;;(setq printer-name "US-P-CLEPX1032-") ;; X_10.127.230.32
  ;; no longer possible after Wickliffe, not existing in Highland Hills
  ;;

  ;; (1c) attempting during work from home pandemic
;;   (setq printer-name "//HPENVY")   ;; ////192.168.254.67//HPENVY
;; nope
  
  ;; ;; (2) print using network printer connected to local LPT3: port
  ;;  (setq printer-name "LPT3:")
  ;; net use lpt3: \\127.0.0.1\US-P-CLEPX1032-
  ;; shared printer on work laptop (but not PS)
  ;;

  ;; (2c) print using LPT3 style during pandemic
  ;;  (setq printer-name "LPT3:")
  ;; nope - keeps trying to write-region to c:/LPT3:
  ;; set HP Envy's printer port on this laptop to LPT3 and see
  
  ;; ;; (3) using lerup's PrintFile32
;;  (setq lpr-command "C:/Program Files (x86)/PrintFile/prfile32.exe") 
;;  (setq lpr-switches '("/q")) 
  ;; (setq ps-lpr-command "C:/Program Files (x86)/PrintFile/prfile32.exe") 
  ;; (setq ps-lpr-switches '("/q")) 
  ;;

  ;; (3c) PrFile during pandemic
  (setq lpr-command "C:/Program Files (x86)/PrintFile/prfile32.exe")
;;  (setq lpr-switches '("/q"))
  (setq lpr-headers-switches nil) ;; <-- !! or else get "page headers are not supported"
  (setq lpr-page-header-program "C:\\Program Files\\Git\\usr\\bin\\pr.exe")
  (setq ps-lpr-command "C:/Program Files (x86)/PrintFile/prfile32.exe") 
;;  (setq ps-lpr-switches '("/q")) 

  
  ;;  (setq printer-name "\\\\US-L-7002395\\PDFPrinter") ;; see a document in that printer queue flicker past but no output.
  
  ;; ;; (4) using a local shared printer (PDFPrinter) the share for PDF-XChange... printer (BCE load)
  ;;(setq printer-name "\\\\us-l-7002395\\PDF-XChange Standard V6") - nope
  ;;(setq ps-printer-name "\\\\us-l-7002395\\PDF-XChange Standard V6") - loser 
  ;;(setq printer-name "\\\\PDF-XChange6") - bad
  ;;(setq ps-printer-name "\\\\PDF-XChange6") - didn't work either
  
  ;; (5) using PDF XChange's "PDF Editor"
;; not working during pandemic from home...
  ;;  (setq printer-name nil)
;;  (setq lpr-headers-switches nil) ;; <-- !! or else get "page headers are not supported"
;;  (setq lpr-command "c:\\program files\\tracker software\\PDF Editor\\PDFXEdit.exe")

  ;; ^--- !! "works" - converts text to PDF but proportional font so it's messed up. :( 
  
    ;;(setq lpr-switches '("/A")) ;; needed?
  ;;(setq ps-lpr-command "c:\\program files\\tracker software\\PDF Editor\\PDFXEdit.exe") ;; but this did not, showed the PS source...
  ;;(setq ps-lpr-switches '("/A")) ;; needed?

;; CHECK FOR lpr-headers-switches - that's where (page headers are not supported) is coming from - set to nil?!
  ;; if that works could use prfile32 or PDFEdit - at least for non-PS printing
  ;; PS printing... eh, probably need the network/share printing to work to connect to real PS printer since emacs is sending PS code
  ;; printing, per Emacs manual, G.9 Printing and MS-Windows
;;  (setq lpr-command "")
;;  (setq printer-name "LPT1") 
  ;; nope - keeps going into debugger complaining about "C:/LPT1"; even after nuking all pr.exe/lpr.exe clones on here (gnuwin32,
  ;; usr/local/wbin, Git,...)

  ;; (6) printing at home from work laptop during pandemic
  ;; (setq ps-printer-name "\\\\192.168.254.67")
  ;; (setq lpr-command nil)
  ;; (setq lpr-headers-switches nil)
  ;; (setq printer-name "\\\\192.168.254.67")


  
  ;; color theme
  )
 ((string-match "C64DEVROB" (system-name))
	(defvar local-site-lisp-path "C:/rgd/dropbox/.emacs.d/lisp")
	(defvar local-find-exe "c:/rgd/dropbox/wbin/find.exe")
        (setq printer-name "//uswic-s-file001/USCLEPX005-M_PS")
    (defvar default-bell-sound-file (concat config-root-path "/.emacs.d/media/Ding.wav"))
	(set-proxy-abb)

	;; using lerup's PrintFile32
	(setq printer-name nil) 
	(setq lpr-command "C:/Program Files (x86)/PrintFile/prfile32.exe") 
	(setq lpr-switches '("/q")) 
	(setq ps-lpr-command "C:/Program Files (x86)/PrintFile/prfile32.exe") 
	(setq ps-lpr-switches '("/q")) 


	)
 ((string-match "W10DEVROB24" (system-name))
	(defvar local-site-lisp-path (concat user-home-directory "/.emacs.d/lisp"))
;;	(defvar local-site-lisp-path "C:/users/Engineer1/OneDri~1/.emacs.d/lisp")
	(defvar local-find-exe "C:/Program Files/Git/usr/bin/find.exe")
	(setq find-program "C:/Progra~1/Git/usr/bin/find.exe")
    ;; apparently local-find-exe is mine and not really used. :/
    ;; not sure why quoting isn't working and have to do Progra~1...
    
;;	(defvar local-find-exe "c:/users/Engineer1/OneDri~1/wbin/find.exe")
        (setq printer-name "//uswic-s-file001/USCLEPX005-M_PS")
    (defvar default-bell-sound-file (concat config-root-path "/.emacs.d/media/Ding.wav"))
    (set-proxy-abb)
;;    (defvar fortune-script-path "c:\\rgd\\dropbox\\sys\\fortune\\fortune-c.ps1")
	(defvar fortune-script-path "C:\\rgd\\sysroot\\sys\\fortune\\fortune-c.ps1")
	;; using lerup's PrintFile32
	(setq printer-name nil) 
	(setq lpr-command "C:/Program Files (x86)/PrintFile/prfile32.exe") 
	(setq lpr-switches '("/q")) 
	(setq ps-lpr-command "C:/Program Files (x86)/PrintFile/prfile32.exe") 
	(setq ps-lpr-switches '("/q")) 
    (setq everything-cmd "c:/sys/everything/es.exe")        ;; to let everything.el know where to find es.exe
    (setq personal-dict "c:\\sys\\hunspell\\personal.en")

	)
 ((string-match "W10DEVROB2" (system-name))
    (defvar default-bell-sound-file (concat config-root-path "/.emacs.d/media/Ding.wav"))
	(defvar local-site-lisp-path (concat config-root-path "/.emacs.d/lisp"))
	;;(defvar local-find-exe "c:/rgd/dropbox/wbin/find.exe")
	(defvar local-find-exe "C:/Program Files/Git/usr/bin/find.exe")
;;	(defvar local-find-exe (concat dropbox-root-path "wbin/find.exe"))
    (setq printer-name "//uswic-s-file001/USCLEPX005-M_PS")
    (set-proxy-abb)
	(defvar fortune-script-path "C:\\rgd\\sysroot\\sys\\fortune\\fortune-c.ps1")
;;    (defvar fortune-script-path (concat dropbox-root-path "/sys/fortune/fortune-c.ps1"))

	;; using lerup's PrintFile32
	(setq printer-name nil) 
	(setq lpr-command "C:/Program Files (x86)/PrintFile/prfile32.exe") 
	(setq lpr-switches '("/q")) 
	(setq ps-lpr-command "C:/Program Files (x86)/PrintFile/prfile32.exe") 
	(setq ps-lpr-switches '("/q")) 
    (setq everything-cmd "c:/sys/everything/es.exe")        ;; to let everything.el know where to find es.exe
    (setq personal-dict "c:\\sys\\hunspell\\personal.en")

	)
 ((string-match "CEBUILDVB6" (system-name))
	(defvar local-site-lisp-path (concat user-home-directory "/.emacs.d/lisp"))
;;	(defvar local-site-lisp-path "C:/users/Engineer1/OneDri~1/.emacs.d/lisp")
	(defvar local-find-exe "C:/Program Files/Git/usr/bin/find.exe")
;;	(defvar local-find-exe "c:/users/Engineer1/OneDri~1/wbin/find.exe")
        (setq printer-name "//uswic-s-file001/USCLEPX005-M_PS")
    (defvar default-bell-sound-file (concat config-root-path "/.emacs.d/media/Ding.wav"))
    (set-proxy-abb)
;;    (defvar fortune-script-path "c:\\rgd\\dropbox\\sys\\fortune\\fortune-c.ps1")
	(defvar fortune-script-path "C:\\rgd\\sysroot\\sys\\fortune\\fortune-c.ps1")
	;; using lerup's PrintFile32
	(setq printer-name nil) 
	(setq lpr-command "C:/Program Files (x86)/PrintFile/prfile32.exe") 
	(setq lpr-switches '("/q")) 
	(setq ps-lpr-command "C:/Program Files (x86)/PrintFile/prfile32.exe") 
	(setq ps-lpr-switches '("/q")) 

	)

 ((string-match "W10DEVROB" (system-name))
	(defvar local-site-lisp-path (concat user-home-directory "/.emacs.d/lisp"))
	(defvar local-find-exe "C:/Program Files/Git/usr/bin/find.exe")
;;	(defvar local-find-exe "c:/rgd/dropbox/wbin/find.exe")
    (setq printer-name "//uswic-s-file001/USCLEPX005-M_PS")
    (defvar default-bell-sound-file (concat config-root-path "/.emacs.d/media/Ding.wav"))
    (set-proxy-abb)
	(defvar fortune-script-path "C:\\rgd\\sysroot\\sys\\fortune\\fortune-c.ps1")
    (setq epg-gpg-home-directory "c:/users/Administrator/AppData/Roaming/gnupg")
    (setq epg-gpg-program "c:/program files (x86)/gnupg/bin/gpg.exe")
    (setq epg-gpgconf-program "c:/program files (x86)/gnupg/bin/gpgconf.exe")

	;; using lerup's PrintFile32
	(setq printer-name nil) 
	(setq lpr-command "C:/Program Files (x86)/PrintFile/prfile32.exe") 
	(setq lpr-switches '("/q")) 
	(setq ps-lpr-command "C:/Program Files (x86)/PrintFile/prfile32.exe") 
	(setq ps-lpr-switches '("/q")) 
    (setq personal-dict "c:\\sys\\hunspell\\personal.en")

	)
 ((string-match "HAPIDev10" (system-name))
	(defvar local-site-lisp-path (concat user-home-directory "/.emacs.d/lisp"))
	(defvar local-find-exe "C:/Program Files/Git/usr/bin/find.exe")
;;	(defvar local-find-exe "c:/rgd/dropbox/wbin/find.exe")
    ;; (setq printer-name "//uswic-s-file001/USCLEPX005-M_PS")
    (defvar default-bell-sound-file (concat config-root-path "/.emacs.d/media/Ding.wav"))
    (set-proxy-abb)
  	(defvar fortune-script-path "C:\\rgd\\sysroot\\sys\\fortune\\fortune-c.ps1")
;;    (defvar fortune-script-path "c:\\rgd\\dropbox\\sys\\fortune\\fortune-c.ps1")


	;; using lerup's PrintFile32
	(setq printer-name nil) 
	(setq lpr-command "C:/Program Files (x86)/PrintFile/prfile32.exe") 
	(setq lpr-switches '("/q"))
	(setq lpr-headers-switches nil)
	(setq ps-lpr-command "C:/Program Files (x86)/PrintFile/prfile32.exe") 
	(setq ps-lpr-switches '("/q")) 
    (setq everything-cmd "c:/sys/everything/es.exe")        ;; to let everything.el know where to find es.exe
    (setq personal-dict "c:\\sys\\hunspell\\personal.en")

	) 
 ((string-match "HAPIBUILD10" (system-name))
	(defvar local-site-lisp-path (concat user-home-directory "/.emacs.d/lisp"))
	(defvar local-find-exe "C:/Program Files/Git/usr/bin/find.exe")
;;	(defvar local-find-exe "c:/rgd/dropbox/wbin/find.exe")
    (setq printer-name "//uswic-s-file001/USCLEPX005-M_PS")
    (defvar default-bell-sound-file (concat config-root-path "/.emacs.d/media/Ding.wav"))
	(set-proxy-abb)
  	(defvar fortune-script-path "C:\\rgd\\sysroot\\sys\\fortune\\fortune-c.ps1")
;;    (defvar fortune-script-path "c:\\rgd\\dropbox\\sys\\fortune\\fortune.ps1")
	;; using lerup's PrintFile32
	(setq printer-name nil) 
	(setq lpr-command "C:/Program Files (x86)/PrintFile/prfile32.exe") 
	(setq lpr-switches '("/q")) 
	(setq ps-lpr-command "C:/Program Files (x86)/PrintFile/prfile32.exe") 
	(setq ps-lpr-switches '("/q"))
	(setq epg-gpg-home-directory "c:/users/admin/AppData/Roaming/gnupg")
	(setq epg-gpg-program "c:/program files (x86)/gnupg/bin/gpg.exe")
	(setq epg-gpgconf-program "c:/program files (x86)/gnupg/bin/gpgconf.exe")
    (setq everything-cmd "c:/sys/everything/es.exe")        ;; to let everything.el know where to find es.exe
    (setq personal-dict "c:\\sys\\hunspell\\personal.en")

	)
 ((string-match "HAPIBUILD1" (system-name))
	(defvar local-site-lisp-path (concat user-home-directory "/.emacs.d/lisp"))
	(defvar local-find-exe "C:/Program Files/Git/usr/bin/find.exe")
;;	(defvar local-find-exe "c:/rgd/dropbox/wbin/find.exe")
    (setq printer-name "//uswic-s-file001/USCLEPX005-M_PS")
    (defvar default-bell-sound-file (concat config-root-path "/.emacs.d/media/Ding.wav"))
    (set-proxy-abb)
  	(defvar fortune-script-path "C:\\rgd\\sysroot\\sys\\fortune\\fortune-c.ps1")
;;    (defvar fortune-script-path "c:\\rgd\\dropbox\\sys\\fortune\\fortune.ps1")
	;; using lerup's PrintFile32
	(setq printer-name nil) 
	(setq lpr-command "C:/Program Files (x86)/PrintFile/prfile32.exe") 
	(setq lpr-switches '("/q")) 
	(setq ps-lpr-command "C:/Program Files (x86)/PrintFile/prfile32.exe") 
	(setq ps-lpr-switches '("/q")) 
	)
 )
;; log results of machine specific setup
(message "= = = System: %s" (system-name))
(message "= = = config-root-path = %s" config-root-path)
(message "= = = local-site-lisp-path = %s" local-site-lisp-path)
(message "= = = local-find-exe = %s" local-find-exe)
(message "= = = emacs-conf-path = %s" emacs-conf-path)
(message "= = = custom file = %s" custom-file)
(message "= = = themes path = %s" custom-theme-load-path)
(message "= = = printer = %s" printer-name)
(message "= = = default bell sound file = %s" default-bell-sound-file)

(setq find-program local-find-exe)
;; Add machine-specific paths to other elisp to the load-path
;;
(add-to-list 'load-path local-site-lisp-path)




