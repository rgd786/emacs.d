;; emacs.consult.el
(require 'linum) ;; deprecated in Emacs 29.1 - require it here so consult fn that need it work

(define-key global-map (kbd "M-g") (make-sparse-keymap))

(define-key global-map (kbd "M-s") (make-sparse-keymap))

(use-package consult
  ;; Replace bindings. Lazily loaded due by `use-package'.
  :bind (
	 ;; Other custom bindings
	 ("M-y" . consult-yank-pop)                ;; ***  orig. yank-pop

     ;; C-c bindings in `mode-specific-map'
	 ("C-c M-x" . consult-mode-command)        ;; **   consult M-x mode specific
	 ("C-c h" . consult-history)               ;; *    paste from history (minibuffer, eshell, comint, term modes)
	 ("C-c k" . consult-kmacro)                ;;      kill-ring?
	 ("C-c m" . consult-man)
	 ("C-c i" . consult-info)                  ;; **** consult info
	 ([remap Info-search] . consult-info)
	 ;; C-x bindings in `ctl-x-map'
	 ("C-x M-:" . consult-complex-command)     ;; *    select cmd from history
	 ("C-x b" . consult-buffer)                ;; **   orig. switch-to-buffer

       ;; b<SPC>  Buffers
       ;; SPC     Hidden buffers
       ;; *<SPC>  Modified buffers
       ;; f<SPC>  Files (Requires recentf-mode)
       ;; r<SPC>  File registers
       ;; m<SPC>  Bookmarks
       ;; p<SPC>  Project
     
	 ("C-x 4 b" . consult-buffer-other-window) ;;      orig. switch-to-buffer-other-window
	 ("C-x 5 b" . consult-buffer-other-frame)  ;;      orig. switch-to-buffer-other-frame
	 ("C-x r b" . consult-bookmark)            ;; **   orig. bookmark-jump
	 ("C-x p b" . consult-project-buffer)      ;; **   orig. project-switch-to-buffer
     ("C-x r f" . consult-recent-file)         ;; **   recent files
	 ;; Custom M-# bindings for fast register access
	 ("M-#" . consult-register-load)
	 ("M-'" . consult-register-store)          ;; orig. abbrev-prefix-mark (unrelated)
	 ("C-M-#" . consult-register)
	 ;; M-g bindings in `goto-map'
        ;; needed define-key global-map (kdb "M-g") (make-sparse-keymap)) before this to work in my Emacs 29
	 ("M-g e" . consult-compile-error)
	 ("M-g f" . consult-flymake)               ;; Alternative: consult-flycheck
	 ("M-g g" . consult-goto-line)             ;; ** orig. goto-line (needs linum)
	 ("M-g M-g" . consult-goto-line)           ;; orig. goto-line
	 ("M-g o" . consult-outline)               ;; ** Alternative: consult-org-heading
     ("M-g h" . consult-org-heading)           ;; ** for org buffers - goto heading
	 ("M-g m" . consult-mark)                  ;; ***
	 ("M-g k" . consult-global-mark)           ;; ** 
	 ("M-g i" . consult-imenu)                 ;; ****
	 ("M-g I" . consult-imenu-multi)           ;; ******
	 ;; M-s bindings in `search-map'
	 ;; ("M-s d" . consult-find) find in windows has problems;; find . -not ( -iwholename */.[a-z]* -prune )
	 ("M-s d" . consult-fd)       
	 ;; ("M-s D" . consult-locate)             ;; locate also problematic on windows
     ;; ("M-s D" . affe-find)                  ;; affe just hangs on my windows work machine
	 ("M-s g" . consult-grep)                  ;; **** like multi-occur-in-matching-buffers but in minibuffer
          ;;   searchpattern#filter  (e.g. mag#log search for mag filter to lines including log
     
	 ("M-s G" . consult-git-grep)              ;; **** like consult-grep but with preview (default)
	 ("M-s r" . consult-ripgrep)               ;; **** same (preview - maybe slower??)
	 ("M-s l" . isearch-forward)               ;; retain for backcompat
     ("C-s"   . consult-line)                  ;; **** i.e swiper 
	 ("M-s s" . consult-line-multi)            ;; **** search all buffers (like multi-occur-in-matching-buffers)
	 ("M-s k" . consult-keep-lines)            ;; **** filter buffer to lines containing x (edits buffer)
	 ("M-s u" . consult-focus-lines)           ;; **** temp filter buffer to lines w/x (doesn't edit buffer)
     ("M-s f" . consult-flyspell)              ;; ** list mispelled words in vertico buffer
     
	 ;; Isearch integration
	 ("M-s e" . consult-isearch-history)
	 :map isearch-mode-map
	 ("M-e" . consult-isearch-history)         ;; orig. isearch-edit-string
	 ("M-s e" . consult-isearch-history)       ;; orig. isearch-edit-string
	 ("M-s l" . consult-line)                  ;; needed by consult-line to detect isearch
	 ("M-s s" . consult-line-multi)            ;; needed by consult-line to detect isearch

	 ;; Minibuffer history
	 :map minibuffer-local-map
	 ("M-s" . consult-history)                 ;; orig. next-matching-history-element
	 ("M-r" . consult-history))                ;; orig. previous-matching-history-element

  ;; Enable automatic preview at point in the *Completions* buffer. This is
  ;; relevant when you use the default completion UI.
  :hook (completion-list-mode . consult-preview-at-point-mode)

  ;; The :init configuration is always executed (Not lazy)
  :init

  ;; Optionally configure the register formatting. This improves the register
  ;; preview for `consult-register', `consult-register-load',
  ;; `consult-register-store' and the Emacs built-ins.
  (setq register-preview-delay 0.5
	register-preview-function #'consult-register-format)

  ;; Optionally tweak the register preview window.
  ;; This adds thin lines, sorting and hides the mode line of the window.
  (advice-add #'register-preview :override #'consult-register-window)

  ;; Use Consult to select xref locations with preview
  (setq xref-show-xrefs-function #'consult-xref
	xref-show-definitions-function #'consult-xref)

  ;; Configure other variables and modes in the :config section,
  ;; after lazily loading the package.
  :config

  ;; Optionally configure preview. The default value
  ;; is 'any, such that any key triggers the preview.
  ;; (setq consult-preview-key 'any)
  ;; (setq consult-preview-key "M-.")
  ;; (setq consult-preview-key '("S-<down>" "S-<up>"))
  ;; For some commands and buffer sources it is useful to configure the
  ;; :preview-key on a per-command basis using the `consult-customize' macro.
  (consult-customize
   consult-theme :preview-key '(:debounce 0.2 any)
   consult-ripgrep consult-git-grep consult-grep
   consult-bookmark consult-recent-file consult-xref
   consult--source-bookmark consult--source-file-register
   consult--source-recent-file consult--source-project-recent-file
   ;; :preview-key "M-."
   :preview-key '(:debounce 0.4 any))

  ;; Optionally configure the narrowing key.
  ;; Both < and C-+ work reasonably well.
  (setq consult-narrow-key "<") ;; "C-+"

  ;; Optionally make narrowing help available in the minibuffer.
  ;; You may want to use `embark-prefix-help-command' or which-key instead.
  ;; (define-key consult-narrow-map (vconcat consult-narrow-key "?") #'consult-narrow-help)

  ;; By default `consult-project-function' uses `project-root' from project.el.
  ;; Optionally configure a different project root function.
  ;;;; 1. project.el (the default)
  ;; (setq consult-project-function #'consult--default-project--function)
  ;;;; 2. vc.el (vc-root-dir)
  ;; (setq consult-project-function (lambda (_) (vc-root-dir)))
  ;;;; 3. locate-dominating-file
  ;; (setq consult-project-function (lambda (_) (locate-dominating-file "." ".git")))
  ;;;; 4. projectile.el (projectile-project-root)
  ;; (autoload 'projectile-project-root "projectile")
  ;; (setq consult-project-function (lambda (_) (projectile-project-root)))
  ;;;; 5. No project support
  ;; (setq consult-project-function nil)

;  (setq completion-in-region-function
;      (lambda (&rest args)
;        (apply (if vertico-mode
;                   #'consult-completion-in-region
;                 #'completion--in-region)
;               args)))

)

;; provide C-u universal-argument to consult-line-multi in a keychord for simple access
;; to the behavior I will normally want to use
;;
(global-set-key (kbd "M-s m") (lambda ()
                                (interactive)
                                (let ((current-prefix-arg 1))
                                  (call-interactively #'consult-line-multi))))
