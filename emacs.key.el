;;; 
;;; KEYBOARD AND BASIC EDITING BEHAVIOR CHANGES
;;;
;; Notes:    F6 (Alt)     F7 (Hyper)
;;
;; Try using H (hyper) - have bound to Apps key (see below; if have Apps key)

;; Try converting/collecting get definitions to here
;; and use general.el: https://github.com/noctuid/general.el#key-features
;;

(message "Configuring keyboard mappings.")


;; MODIFIERS
;;
;; Control C-
;; Meta    M-
;; Hyper   H-
;; Super   s-
;; Alt     A-
;; Shift   S-

;; Set up Super (Windows keys), Hyper (Apps) on Windows ;; from help-emacs-w 6/17:
(setq w32-pass-lwindow-to-system  nil
      w32-pass-rwindow-to-system  nil
      w32-pass-apps-to-system     nil
      w32-lwindow-modifier       'super   ;; Left Windows
      w32-rwindow-modifier       'alt     ;; Right Windows
      w32-apps-modifier          'hyper)  ;; App-Menu (key to right of Right Windows)
(w32-register-hot-key [s-])
;(w32-register-hot-key [snapshot]) -- if want to capture PrtScn

;; accessing modifier keys without physical keys for them:
;; C-x @ a <?>   =>   alt-<?>
;; C-x @ h <?>   =>   hyper-<?>
;; C-x @ s <?>   =>   super-<?>
;;(define-key function-key-map (kbd "<kp-1>") 'event-apply-control-modifier) ; have ctrl key
;;(define-key function-key-map (kbd "<kp-2>") 'event-apply-meta-modifier) ; Alt = meta
;(define-key function-key-map (kbd "<f6>") 'event-apply-super-modifier) ; Left Win = super (see above)
;;(define-key function-key-map (kbd "<kp-4>") 'event-apply-shift-modifier) ; have shift key
(define-key function-key-map (kbd "<f6>") 'event-apply-alt-modifier)
(define-key function-key-map (kbd "<f7>") 'event-apply-hyper-modifier)

;; H-p xxx => hyper commands (Projectile commands)
;; a-xxx => alt commands (entering special characters)
;; s-xxx => super commands -- (?? available)


;; FUNCTION KEYS
;;
(bind-key "<f8>"   'minimap-mode)   ;; use S- M- (or H- (f7+f8) ?
(bind-key "<f11>"  'describe-personal-keybindings)
(global-set-key [f5] 'call-last-kbd-macro)




;; PERSONAL KEYMAP - to avoid major modes stealing keybindings
(defvar rgd/keys-keymap (make-keymap)
  "Keymap for rgd/keys-mode")

(define-minor-mode rgd/keys-mode
  "Minor mode for my personal keybindings."
  :init-value t
  :global t
  :keymap rgd/keys-keymap)

;; The keymaps in `emulation-mode-map-alists' take precedence over
;; `minor-mode-map-alist' so adding this gets ahead of all other
;; modes (minor/major)
(add-to-list 'emulation-mode-map-alists
             `((rgd/keys-mode . ,rgd/keys-keymap)))

; simple key to switch/rotate through windows/frames
;
; was trying ace-window (also M-o) - now putting ace-window in window-frame hydra 'a' 
; so making M-o to next-window-any-frame 
; simpler than "C-x o" - but repeat-mode may help 
;
(define-key rgd/keys-keymap (kbd "M-o") 'next-window-any-frame)   ; not just other-window 


(define-key rgd/keys-keymap (kbd "C-c $") 'hydra-flyspell/body)

;; <key> for 'hydra-magit/body e.g.    ??

;;  (11/23 - switching to consult/vertico - has own goto-line method
;; goto line:        M-g g
;;
;; (global-set-key (kbd "C-c g") 'goto-line)
;; (global-unset-key (kbd "C-c g"))


;; personal function keybinding
;;
(global-set-key (kbd "C-c d b") 'create-scratch-buffer)
(global-set-key (kbd "C-c r e")   'rgd-load-config)
(global-set-key (kbd "C-c r t")   'rgd-org-start)
(global-set-key (kbd "C-c r v")   'rgd-load-etc)
(global-set-key (kbd "C-c r w")   'rgd-writing-mode)

;; have bookmark (C-x r b) for emacs-notes.org - so maybe don't need special keybinding C-c x
(global-set-key (kbd "C-c r x")
    (lambda () (interactive) (find-file (concat org-directory "\\dev\\emacs-notes.org"))))
(global-set-key (kbd "C-c r f")
    (lambda () (interactive) (find-file (concat (getenv "OneDrive" "\\sys\\fortune-powershell\\fortune.txt")))))
(global-set-key (kbd "C-c r h")
    (lambda () (interactive) (find-file (concat org-directory "health.org"))))


;; FUNCTIONALITY

;; bookmarks
;;
;; C-x r m  = set bookmark
;; C-x r b  = consult bookmark
;; C-x t B  = treemacs bookmark
;; C-x r l  = bookmark list
;; C-x r M  = set bookmark named NAME at current location

;; abbreviations
;;
(global-set-key (kbd "C-<tab>") 'dabbrev-expand)
(define-key minibuffer-local-map (kbd "C-<tab>") 'dabbrev-expand)

(global-set-key (kbd "s-<tab>") 'consult-yasnippet)

;; tags searching
;;
(global-set-key (kbd "M-,") 'tags-loop-continue) ;; continue last tags search

;; indenting
;;
(define-key global-map (kbd "RET") 'newline-and-indent) ;; auto indent when hit enter
;; already C-M-\  (global-set-key (kbd "C-c i") 'indent-region)

;; paren navigation: M-[
;;
;(global-set-key "\M-]" 'forward-list) ;; move to matching parentheses
;(global-set-key "\M-[" 'backward-list) ; http://www.uhoreg.ca/programming/emacs - 2-Oct-08

;; buffer navigation
;;
;; tired of C-x o when doing C-x C-b to put focus on buffer list. Trying from stackexchange:
;;   https://emacs.stackexchange.com/questions/38659/remapping-c-x-c-b-to-open-list-of-buffers-in-same-window
;;   https://emacs.stackexchange.com/questions/835/make-buffer-list-take-focus
;; (list-buffers is default command)
(global-set-key [remap list-buffers] #'ibuffer)

;; inserting special characters
;;
;; some Super keys
;; (but A- has bindings for these characters)
;; (global-set-key [(super \,)] (lambda () (interactive) (insert  ?« )))
;; (global-set-key [(super \.)] (lambda () (interactive) (insert  ?» )))
;; (global-set-key [(super \')] (lambda () (interactive) (insert  ?“ )))
;; (global-set-key [(super  \")] (lambda () (interactive) (insert  ?” )))
;; 
;; some Hyper keys
(define-key key-translation-map (kbd "H-3") (kbd "•")) ; bullet
(define-key key-translation-map (kbd "H-4") (kbd "◇")) ; white diamond
(define-key key-translation-map (kbd "H-5") (kbd "†")) ; dagger
(define-key key-translation-map (kbd "s-<") (kbd "«")) ; double-left chevron
(define-key key-translation-map (kbd "s->") (kbd "»")) ; double-right chevron

;; repeat mode - e.g. C-x o .. o .. o   instead of C-x o C-x o C-x of
;; describe-repeat-maps -- for help on what commands can be repeated
(repeat-mode)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;            MOUSE CHANGES                                                    ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; trying to make mouse pasting fit better in Windows
;; from:
;; https://stackoverflow.com/questions/13036155/
;;   how-to-combine-emacs-primary-clipboard-copy-and-paste-behavior-on-ms-windows
;; so now selecting text in Emacs should copy it to Windows clipboard and be available in other Windows apps
;; and if C-y in Emacs it pastes, and mouse-2 also pastes
;; killing (C-w) also copies to Windows clipboard for other apps, yank and mouse-2 paste in Emacs
;; text on clipboard is pasted into Emacs via yank (C-y) or mouse-2.
;; 
(setq select-active-regions nil)
(setq mouse-drag-copy-region t)
(global-set-key [mouse-2] 'mouse-yank-at-click)



(message "Configuring keyboard mappings - done.")


;;;
;;; EDITING BEHAVIOR CHANGES
;;;
(setq scroll-preserve-screen-position 1) ;; keep point's line same in window when scrolling
