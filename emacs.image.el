(message "emacs.image.el - image handling")

;; don't use the default 'convert' on Windows which has silly windows program
;;
(setq image-dired-cmd-create-thumbnail-program "magick.exe") ;;"/usr/local/bin/convert")

;; for more configuration, read the manual:   C-h r    then   g image-mode


;; Image viewer
(auto-image-file-mode 1)
(eval-after-load "image-mode"
  '(progn
     (define-key image-mode-map (kbd "g") 'revert-buffer)
     (define-key image-mode-map (kbd "k") 'image-kill-buffer)
     (define-key image-mode-map (kbd "<right>") 'image-next-file)
     (define-key image-mode-map (kbd "<left>") 'image-previous-file)
   )
)


(use-package emacs
  :bind
;  ((:map image-mode-map
;         ("k" . image-kill-buffer)
;         ("<right>" . image-next-file)
;         ("<left>"  . image-previous-file))
   (:map dired-mode-map
         ("C-<return>" . image-dired-dired-display-external))
)

(use-package image-dired
  :custom
  (image-dired-external-viewer "gimp.exe")
  (image-dired-thumb-margin 10)
  :bind
  (("C-c d i" . image-dired))
  (:map image-dired-thumbnail-mode-map
        ("C-<right>" . image-dired-display-next)
        ("C-<left>" . image-dired-display-previous)))
