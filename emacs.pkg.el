;; ****************************************************************
;; Packages:
; async
; hydra
; completion el
; - vertico
; - consult
; projectile
; backward-forward
; yasnippet
; bbdb
; golden-ratio
; sunshine
; helpful
; dired el
; recentf
; page-break-lines (disabled)
; browse-kill-ring   C-c k
; undo-tree
; whitespace
; sr-speedbar
; deft (disabled)
; ispell
; calfw
; auto-fill / column settings
; rgd/ag-arguments  ?
; ripgrep / rg  (for org-roam)
; powerlines - rgd theme
; which-key
; google-this (disabled)
; popup
; google-translate
; delight
; magit
; treemacs f12
; markdown-mode
; amx
; avy (disabled)
; git-gutter (disabled)
; everything
; plantuml


;; ****************************************************************
;; PACKAGE SETUP
;;
(message "packages start - emacs.pkg.el")

(setq load-prefer-newer t) ;; Load newer version of .el and .elc if both are available ; kmodi

(require 'package)
(setq package-enable-at-startup nil) ;; defer until (package-init)

(defvar config-package-path (concat config-root-path "/.emacs.d/elpa/"))
(setq package-user-dir config-package-path)

(setq package-archive-priorities
      '( ;; ("melpa-stable" . 20)
         ;; ("marmalade" . 20)
        ("org" . 5)
		("notgnu" . 10)
        ("gnu" . 20)
        ("melpa" . 30)
		))

;; comment here <https://www.reddit.com/r/emacs/comments/8i3bd6/how_do_i_automatically_install_packages_from_my/>
;; says with Emacs 27 users should no longer call package-initialize from their init.el... - TRUE?
;; and now getting warning message on startup that this is not needed - that I'll believe
;; but at home it gives a warning - package not initialized... argh! make up your mind!
;;(when (version< emacs-version "27.0") 
 (with-timer "package-initialize"
   (unless package--initialized (package-initialize t))
 )

;; Adding packages sources in case it isn't already.
;; from https://www.reddit.com/r/emacs/comments/8i3bd6/how_do_i_automatically_install_packages_from_my/
(unless (assoc-default "melpa" package-archives)
   (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t))
(unless (assoc-default "notgnu" package-archives)
   (add-to-list 'package-archives '("notgnu" . "https://elpa.nongnu.org/nongnu/") t))
(unless (assoc-default "org" package-archives)
   (add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t))


;(unless (package-installed-p 'use-package)
;   (package-refresh-contents)
;   (package-install 'use-package)
;   )
;
;(unless package-archive-contents
;  (package-refresh-contents))  
 
(message "Refreshing package list...")
(package-refresh-contents)

(message "Installing Use Package...")
(require 'use-package)

(message "Setting use-package settings...")
(setq use-package-verbose t)
(setq use-package-always-ensure t)
;(message "require use-package...")
;(require 'use-package)
(message "setting use-package-always-ensure t...")
(setq use-package-always-ensure t)
(setq load-prefer-newer t)

;(message "using use-package-ensure-system-package...")
;(use-package use-package-ensure-system-package :ensure t)
;(message "done using use-package-ensure-system-package.")

;(when (not minpkg-switch-found)
;    (progn
;	)
;)

;; ****************************************************************
;; PACKAGES 
;;
;; async
(use-package async
  :ensure t
  :init
  (setq async-bytecomp-allowed-packages '(all))
  :config
  (async-bytecomp-package-mode 1)
  (dired-async-mode 1)
  
  ;; limit number of async processes -- from https://gist.github.com/kiennq/cfe57671bab3300d3ed849a7cbf2927c
  (eval-when-compile
    (require 'cl-lib))
  (defvar async-maximum-parallel-procs 4)
  (defvar async--parallel-procs 0)
  (defvar async--queue nil)
  (defvar-local async--cb nil)
  (advice-add #'async-start :around
              (lambda (orig-func func &optional callback)
                (if (>= async--parallel-procs async-maximum-parallel-procs)
                    (push `(,func ,callback) async--queue)
                  (cl-incf async--parallel-procs)
                  (let ((future (funcall orig-func func
                                         (lambda (re)
                                           (cl-decf async--parallel-procs)
                                           (when async--cb (funcall async--cb re))
                                           (when-let (args (pop async--queue))
                                             (apply #'async-start args))))))
                    (with-current-buffer (process-buffer future)
                      (setq async--cb callback)))))
              '((name . --queue-dispatch)))  
)
  

 
;; DIRED
(setq emacs-dired-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.dired.el")))
(with-timer "emacs-dired"
  (load-file emacs-dired-el))


;; ****************************************************************  
;; load hydra package
;;
(setq emacs-hydra-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.hydra.el")))

(if (file-exists-p emacs-hydra-el)
    (progn
     (message "loading Emacs configuration file %s" emacs-hydra-el)
     (load-file emacs-hydra-el)))


;; ****************************************************************
;; load completion system 
;; - counsel/ivy/swiper  
;; - vertico/marginalia/orderless/embark
;; - consult
;;(setq emacs-comp-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.comp-ivy.el")))
(setq emacs-comp-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.comp-vert.el")))

(if (file-exists-p emacs-comp-el)
    (progn
     (message "loading Emacs configuration file %s" emacs-comp-el)
     (load-file emacs-comp-el)))

(setq emacs-consult-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.consult.el")))

(if (file-exists-p emacs-consult-el)
    (progn
     (message "loading Emacs configuration file %s" emacs-consult-el)
     (load-file emacs-consult-el)))


;; Projectile
(unless minpkg-switch-found

	;; H-p f => find file in project
	;; <f7>-p f
	;; click on 'Projectile' in mode line (M-1) to see menu
	(use-package projectile
	  :ensure t
	  :delight '(:eval (concat " P:" (projectile-project-name)))
	  :config
	  (progn
		;; 
		;; hyper-P (apps key on windows if w32-pass-apps-to-system is nil, and w32-apps-modifier set to 'hyper
		;; - see emacs.key.el)
		(define-key projectile-mode-map (kbd "H-p") 'projectile-command-map)
		
		;(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map) ; changing to hydra-projectile
		(setq projectile-indexing-method 'native) ;; elisp - find on some machines is ugly windows find
		;; or try 'alien - will use git ls-files for VC projects,
        ;; can modify projectile-generic-command from 'find' to better find version
		
		 (projectile-mode +1)
	  )
	)

)

; mark ring navigation (global - local buffer?)
;    ref:  https://www.reddit.com/r/emacs/comments/6a49bn/emacs_navigate_backwards_and_forwards/
;          https://github.com/rolandwalker/back-button (nice but don't use toolbar; be nice to have visible marks)
;

(setq set-mark-command-repeat-pop t)
(setq mark-ring-max 100)
(setq global-mark-ring-max 100)

; C-u C-SPC = pop-mark (local)
; C-x C-SPC = pop-global-mark 
;   C-SPC to keep going back.  But would like back and forth.


; backward-forward - package that tries to emulate browser back/for-ward buttons with marks in global mark ring.
;  keeps track of all push-mark ops into backward-forward-mark-ring and fns to navigate through it.
; https://gitlab.com/vancan1ty/emacs-backward-forward 
; https://utcc.utoronto.ca/~cks/space/blog/programming/EmacsBackForward#:~:text=The%20plain%20mark%20ring%20is,%2Dforward%20(also%20gitlab).
;
; C-<left>   = backward-forward-previous-location
; C-<right>  = backward-forward-next-location 
(use-package backward-forward
   :ensure t
   :config
     (backward-forward-mode t)
;
; cks's page says he uses M-left/right - which may be easier but org uses that for promotion/demotion, so not using that.
; but... the idea of mapping other mouse buttons to backward/forward is appealing (if have such a mouse - on all machines)
;   :bind (:map backward-forward-mode-map
;              ("<C-left>" . nil)
;              ("<C-right>" . nil)
;              ("<M-left>" . backward-forward-previous-location)
;              ("<M-right>" . backward-forward-next-location)
;              ("<mouse-8>" . backward-forward-previous-location)
;              ("<mouse-9>" . backward-forward-next-location)
;			  )
)
   


(unless minpkg-switch-found

; NOTE: yasnippet clashes org-mode with using C-c & keybinding that was org-mark-ring-goto 

; added s-<left> to counter that but beware.

	(use-package yasnippet                  ; Snippets
	  :ensure t
	  :config
	  (progn
	 
	   (add-to-list 'yas-snippet-dirs "~/.emacs.d/snippets")	
	   (use-package yasnippet-snippets         ; Collection of snippets
		   :ensure t)

		;(setq yas-verbosity 1)                      ; No need to be so verbose
		(setq yas-wrap-around-region t)

	 ;;   (setq yas-snippet-dirs
	 ;;     '("~/.emacs.d/snippets"                 ;; personal snippets
	 ;;       "/path/to/some/collection/"           ;; foo-mode and bar-mode snippet collection
	 ;;       "/path/to/yasnippet/yasmate/snippets" ;; the yasmate collection
	 ;;       )
	 ;;	)
	  
;	  (with-eval-after-load 'yasnippet
;		(setq yas-snippet-dirs 'yas-snippet-dirs))

	  (yas-reload-all)
	  (yas-global-mode)
	 )
   )
   
;   (use-package consult-yasnippet 
;	      :ensure t
;;		  :bind 
;;		(("s-<TAB>" . consult-yasnippet))  ; super-TAB - i.e. Win+TAB 
;    )
)


(unless minpkg-switch-found

	;; bbdb

	(with-timer "bbdb"
	  (use-package bbdb
		:ensure t
		:config
		(bbdb-initialize 'anniv)
		(add-hook 'diary-list-entries-hook 'bbdb-anniv-diary-entries)
	  )
	)
)
;; when entering data with ivy active, must use M-C-J to end entries without data, regular C-j / RET
;; may not work (like endless address fields when entering a new contact
;;
;; or could try:
   ;; (defadvice bbdb-read-string (before bbdb-read-string-no-ivy activate)
   ;;   (ivy-mode 0))
   ;; (defadvice bbdb-read-string (after bbdb-read-string-yes-ivy activate)
   ;;   (ivy-mode 1))
;; from: https://sourceforge.net/p/bbdb/mailman/bbdb-info/thread/yeg8tkngrni.fsf@iac.es/

;; undoing a mistake
;; C-x b to the .bbdb buffer
;; undo any changes there to you hearts content
;; SAVE .bbdb
;; kill *BBDB* buffer
;; rerun M-x bbdb



(if golden-ratio-switch-found  ;; disable for now

	(with-timer "golden-ratio"
	  (use-package golden-ratio
		:ensure t
		:delight
		:config
		(add-to-list 'golden-ratio-exclude-modes "ediff-mode")
		(add-to-list 'golden-ratio-exclude-modes "helm-mode")
		(add-to-list 'golden-ratio-exclude-modes "dired-mode")
	;;	(add-to-list 'golden-ratio-inhibit-functions '(fboundp 'ivy-posframe--display-p))
		(setq golden-ratio-exclude-buffer-names '("*Ilist*" "*Deft*" "*Help*"))
		(setq golden-ratio-exclude-modes '("ediff-mode"
										   "gud-mode"
										   "gdb-locals-mode"
										   "gdb-registers-mode"
										   "gdb-breakpoints-mode"
										   "gdb-threads-mode"
										   "gdb-frames-mode"
										   "gdb-inferior-io-mode"
										   "gud-mode"
										   "gdb-inferior-io-mode"
										   "gdb-disassembly-mode"
										   "gdb-memory-mode"
										   "magit-log-mode"
										   "magit-reflog-mode"
										   "magit-status-mode"
										   "org-roam-mode"
										   "IELM"
										   "eshell-mode" "dired-mode"))
		(golden-ratio-mode 1)
		:defer 4
		)
	) 
)


(unless minpkg-switch-found

	(with-timer "sunshine"
	  (use-package sunshine
		:ensure t
		:config
		(setq sunshine-appid "9e78b67db4d4e5c678aa0efb35551ac9") ;;  5174550 twinsburg city id
		(setq sunshine-show-icons t)
		(set-face-attribute 'sunshine-forecast-date-face nil :foreground "blue")
		(set-face-attribute 'sunshine-forecast-headline-face nil :foreground "goldenrod" :height 1.5)
		:defer 3
		)
	)
)

(unless minpkg-switch-found

	(with-timer "helpful"
	  (use-package helpful
		:ensure t
		:config
		(defun counsel-helpful-keymap-describe ()
		  "select keymap with ivy, display help with helpful"
		  (interactive)
		  (ivy-read "describe keymap: " (let (cands)
										  (mapatoms
										   (lambda (x)
											 (and (boundp x) (keymapp (symbol-value x))
												  (push (symbol-name x) cands))))
										  cands)
					:require-match t
					:history 'counsel-describe-keymap-history
					:sort t
					:preselect (ivy-thing-at-point)
					:keymap counsel-describe-map
					:caller 'counsel-helpful-keymap-describe
					:action (lambda (map-name)
							  (helpful-variable (intern map-name))) ))
		:bind
		(("C-h f" . helpful-callable)
		 ("C-h v" . helpful-variable)
		 ("C-h k" . helpful-key)
		 ("C-c C-d" . helpful-at-point)
		 ("C-h F" . helpful-function)
		 ("C-h C" . helpful-command)
		 ("C-h M" . counsel-helpful-keymap-describe))
		:defer 3
		)
	  )
)
;; (global-set-key (kbd "C-h M") #'counsel-helpful-keymap-describe)



(unless minpkg-switch-found

	(with-timer "recentf"
	 (use-package recentf
		:ensure t
	   :commands (recentf-open-files)
	   :init
		 (setq recentf-max-menu-items 50)
         (setq recentf-max-saved-items 50)
		 (setq recentf-auto-cleanup 'never)
		 (setq recentf-save-file (expand-file-name (concat config-root-path (concat "/.emacs.d/" (concat system-name "-recentf")) )))
		 (setq recentf-keep '(file-remote-p file-readable-p))
	   :config
		 (recentf-mode 1)
		 (setq recentf-exclude '(".*-autoloads\\.el\\"
								  "[/\\]\\.elpa/"))  ;; https://www.reddit.com/r/emacs/comments/5wydsd/stop_recent_files_showing_elpa_packages/
	   :bind   
		 ("C-x C-r" . recentf-open-files)
	   :defer 3
	 )
	)
)

; having trouble with org-roam backlinks buffer not working, suggestion was to disable page-break-lines mode
;(unless minpkg-switch-found
;
;	(with-timer "page-break-lines"
;	 (use-package page-break-lines
;		:ensure t
;		:defer 5
;		:config
;		   (global-page-break-lines-mode)
;	  )
;	)
;)

;; Browse Kill Ring  (look for browse-kill-ring+.el ... )
;;
;;  or use Edit > Paste from Kill menu (not as nice)
;;
;;  or    ("C-M-y" . counsel-yank-pop)
;;
(unless minpkg-switch-found

	(with-timer "browse-kill-ring"
	  (use-package browse-kill-ring
		:ensure t
		:defer 5
		:commands (browse-kill-ring)
		:config
		(setq browse-kill-ring-replace-yank t) ;; act like yank-pop
		(setq browse-kill-ring-show-preview t) ;; display where item under point in *Kill Ring* buffer would be inserted
		(setq browse-kill-ring-highlight-current-entry t)  ;; highlight current entry
		:bind 
			("C-c k" . browse-kill-ring)
		)
	)
)

(unless minpkg-switch-found

	(with-timer "undo-tree"
	  (use-package undo-tree
		:defer t
		:delight
		:ensure t
		:init
		(global-undo-tree-mode)
		:config
		(defvar --undo-history-directory (concat user-emacs-directory "undos/") "Directory to save undo history files.")
		 (unless (file-exists-p --undo-history-directory) (make-directory --undo-history-directory t))
		;; stop littering with *.~undo-tree~ files everywhere
		(setq undo-tree-history-directory-alist `(("." . ,--undo-history-directory)))
        (setq undo-tree-enable-undo-in-region nil)
		(setq undo-tree-auto-save-history nil)
	  )
	)
)

(unless minpkg-switch-found

	(with-timer "whitespace"
		(use-package whitespace
		:ensure t
		  :bind 
			 (("C-c T w" . whitespace-mode))
		  :config 
			 (setq whitespace-line-column nil)
		  :defer 5
		)
	 )
)

;; SPEEDBAR
(unless minpkg-switch-found

	(with-timer "speedbar"
	   (use-package sr-speedbar
		:ensure t
		  :commands sr-speedbar-toggle
		  :bind
			 (
			   ([f9] . sr-speedbar-toggle)
			   :map speedbar-mode-map
			   ("<right>" . speedbar-expand-line)
			   ("<left>" . speedbar-contract-line)
			 )
		)
	)
)

;(unless minpkg-switch-found
;
;	(with-timer "deft"
;	  (use-package deft
;		:ensure t
;		:bind ("<f8>" . deft)
;		:commands (deft)
;		:config
;		(progn 
;		  (setq deft-extensions '("txt" "tex" "org"))
;		  (setq deft-directory (concat org-root-path "/org"))
;		  (setq deft-recursive t)
;		  (setq deft-ignore-file-regexp "^\#.*")
;		  (setq deft-use-filename-as-title t)
;		  (setq deft-use-filter-string-for-filename t)
;		  (setq deft-auto-save-interval 0) ; default is 1.0, 0 to disable auto-save
;		  (setq deft-file-naming-rules '((nospace . "_")
;										 (case-fn . downcase)))
;         ;; flyspell autocorrect word overrides complete-at-point - boo...
;    	  (define-key flyspell-mode-map "\C-\M-i" nil)
;;;       (define-key flyspell-mode-map "\C-\." nil) ;; and embark-act 
;;; - but this isn't working - not using deft much anyway so removing for a while
;          (define-key flyspell-mode-map "\C-;" nil)
;          (define-key flyspell-mode-map "\C-$" flyspell-auto-correct-word)
;          ;;
;          ;; have left:
;          ;; C-,   -- flyspell goto next error
;          ;; C-c $ -- flyspell correct word before point
;          ;;
;          ;; maybe need whole new prefix key (C-c f?) for flyspell functions
;         ;; in my head C-$ or M-$ are stuck as spelling completions
;          ;;  (M-$ is ispell-word)
; 		)
;	  )
;	)
;)

	  
;; Spell checking
;; https://lists.gnu.org/archive/html/help-gnu-emacs/2014-04/msg00030.html
;;
(unless minpkg-switch-found

	(with-timer "ispell"
	  (use-package ispell
		:ensure t
		:defer 8
		:init
		  (setq ispell-local-dictionary-alist
				'(
				  (nil
				   "[[:alpha:]]"
				   "[^[:alpha:]]"
				   "[']"
				   t
				   ("-d" "default" "-p" "c:\\sys\\hunspell\\personal.en")
				   nil
				   iso-8859-1)
				  ("en_US"
				   "[[:alpha:]]"
				   "[^[:alpha:]]"
				   "[']"
				   t
				   ("-d" "en_US" "-p" "c:\\sys\\hunspell\\personal.en")
				   nil
				   iso-8859-1)
				 )
		  )
		 (setq ispell-dictionary "en_US")
		 (setq ispell-local-dictionary "en_US") 
		 (setq ispell-really-hunspell t)
		 (setq ispell-hunspell-dictionary-alist ispell-local-dictionary-alist)
		:config
		 (progn
		  (dolist (hook '(text-mode-hook))
			  (add-hook hook (lambda () (progn 
                               (flyspell-mode 1)
                               (global-unset-key (kbd "C-c $")) ;; leave that key for hydra-flyspell
                               )
                               )))
		  (dolist (hook '(change-log-mode-hook log-edit-mode-hook))
			  (add-hook hook (lambda () (flyspell-mode -1))))
		  (add-hook 'c++-mode-hook
			  (lambda ()
				 (flyspell-prog-mode) 
			  )
		  )
		  (setq ispell-program-name "hunspell")
		  (setq ispell-extra-args   '("-d en_US"))
		 )
			  
	  )
	)
)

(eval-after-load "flyspell"
  '(define-key flyspell-mode-map (kbd "C-.") nil)) ;; leave that key for embark-act


(unless minpkg-switch-found

	(with-timer "calfw"
	  (use-package calfw
		:ensure t
		:config
		(use-package calfw-cal :ensure t)
		(use-package calfw-org :ensure t)
	  )
	)
)
(message "done with loading package calfw")


;; shouldn't this block be in emacs.opt.el under general options?  Or needed here for some reason?
;;
;; auto-fill for text modes
(setq-default fill-column 100)

;;; turn on auto-fill for all text mode buffers
(add-hook 'text-mode-hook #'(lambda ()
                             (setq fill-column 80)
    ;                        (display-fill-column-indicator-mode)
    ;                        (auto-fill-mode 1)
							)
)

;;; key sequence to enable auto-fill mode (if get used to this, disable above as sometimes you don't want autofill
(global-set-key (kbd "C-c q") 'auto-fill-mode)

; trying olivetti mode over auto-fill-mode 6/24 

;; functions to - lookup word (dict, thesaurus, wikipedia)
;;  hydra to do so
;
; https://codingquark.com/emacs/2023/04/07/a-clean-writing-setup-emacs.html 


(unless minpkg-switch-found

	;;; Default ag arguments
	;; https://github.com/ggreer/the_silver_searcher
	(defconst rgd/ag-arguments
	  '("--nogroup" ;mandatory argument for ag.el as per https://github.com/Wilfred/ag.el/issues/41
		"--skip-vcs-ignores"                ;Ignore files/dirs ONLY from `.ignore'
		"--numbers"                         ;Line numbers
		"--smart-case"
		;; "--one-device"                      ;Do not cross mounts when searching
		"--follow"                          ;Follow symlinks
		"--ignore" "#*#")                   ;Adding "*#*#" or "#*#" to .ignore does not work for ag (works for rg)
	  "Default ag arguments used in the functions in `ag', `counsel' and `projectile' packages.")
)


;;; ripgrep (10/26/2023 when learning org-roam)
;(message "pre-ripgrep - using use-package-ensure-system-package...")
;(use-package use-package-ensure-system-package :ensure t)
;(message "done using use-package-ensure-system-package.")
;(message "on to ripgrep...")
;; 2-5-24 - devrob03 setup - not getting ripgrep rg package from repo for some reason - all others came ok
;(with-timer "ripgrep"
;  (use-package rg
;		:ensure-system-package (rg . ripgrep)
;     :config
;
	 (rg-enable-default-bindings)
;  )
;)

;(require 'rg)   ; wait... already doing use-package above - is this needed? useless?



;(add-to-list
; 'display-buffer-alist
; '("\\*rg\\*" (lambda (buffer alist) 
;                (select-window  (display-buffer-pop-up-window buffer alist)))))
;; move to emacs.wm.el with other window mgmt stuff


(use-package buffer-move
 :ensure t)
 
;; deadgrep - simple, immediate ripgrep search - https://github.com/Wilfred/deadgrep
;; RET / o  - visit result (same/other window)
;; n / p - next/previous hit
;; M-n / M-p - next/prev file
;; C-c C-k  -- stop running search
;; C-u     -- prefix arg to not start search automatically (careful if defaults to ~!!)
;(global-set-key (kbd "<C-f3>") 'deadgrep)



(defun rgd/powerline-my-theme ()
  "Setup my powerline mode-line."
  (interactive)
  (setq-default mode-line-format
                '("%e"
                  (:eval
                   (let* ((active (powerline-selected-window-active))
                          (mode-line-buffer-id (if active 'mode-line-buffer-id 'mode-line-buffer-id-inactive))
                          (mode-line (if active 'mode-line 'mode-line-inactive))
                          (face0 (if active 'powerline-active0 'powerline-inactive0))
                          (face1 (if active 'powerline-active1 'powerline-inactive1))
                          (face2 (if active 'powerline-active2 'powerline-inactive2))
                          (separator-left (intern (format "powerline-%s-%s"
                                                          (powerline-current-separator)
                                                          (car powerline-default-separator-dir))))
                          (separator-right (intern (format "powerline-%s-%s"
                                                           (powerline-current-separator)
                                                           (cdr powerline-default-separator-dir))))
                          (lhs (list (powerline-raw "%*" face0 'l)
                                     (when powerline-display-buffer-size
                                       (powerline-buffer-size face0 'l))
                                     (when powerline-display-mule-info
                                       (powerline-raw mode-line-mule-info face0 'l))
                                     (powerline-buffer-id `(mode-line-buffer-id ,face0) 'l)
                                     (when (and (boundp 'which-func-mode) which-func-mode)
                                       (powerline-raw which-func-format face0 'l))
                                     (powerline-raw " " face0)
                                     (funcall separator-left face0 face1)
                                     (when (and (boundp 'erc-track-minor-mode) erc-track-minor-mode)
                                       (powerline-raw erc-modified-channels-object face1 'l))
                                     (powerline-major-mode face1 'l)
                                     (powerline-process face1)
                                     (powerline-minor-modes face1 'l)
                                     (powerline-narrow face1 'l)
                                     (powerline-raw " " face1)
                                     (funcall separator-left face1 face2)
                                     (powerline-vc face2 'r)
                                     (when (bound-and-true-p nyan-mode)
                                       (powerline-raw (list (nyan-create)) face2 'l))))
                          (rhs (list (powerline-raw global-mode-string face2 'r)
                                     (funcall separator-right face2 face1)
                                     (unless window-system
                                       (powerline-raw (char-to-string #xe0a1) face1 'l))
                                     (powerline-raw "%4l" face1 'l)
                                     (powerline-raw ":" face1 'l)
                                     (powerline-raw "%3c" face1 'r)
                                     (funcall separator-right face1 face0)
                                     (powerline-raw " " face0)
                                     (powerline-raw "%6p" face0 'r)
                                     (when powerline-display-hud
                                       (powerline-hud face0 face2))
                                     (powerline-fill face0 0)
                                     )))
                     (concat (powerline-render lhs)
                             (powerline-fill face2 (powerline-width rhs))
                             (powerline-render rhs))
					 )
				   )
				  )
				)
  )



;; which-key is great - but doesn't play nicely with embark (or vice versa) which has similar functionality
;; taking which-key out for now while trying embark
;;
;; https://github.com/justbur/emacs-which-key
;; https://github.com/yanghaoxie/which-key-posframe - paging doesn't work yet (4/2019)
;; q.v. https://www.reddit.com/r/emacs/comments/b63i6t/new_package_whichkeyposframe/

(unless minpkg-switch-found

	(use-package which-key
	  :ensure t
	  :delight
	  :config
	  ;; (use-package which-key-posframe
	  ;;   :ensure t
	  ;; 	 :config
	  ;; 	 (setq which-key-posframe-poshandler 'posframe-poshandler-window-top-left-corner) 
	  ;; 	 (which-key-posframe-mode))
      (which-key-setup-side-window-bottom) ;; default - custom'd away for me? wasn't working, posframe mode?
	  (which-key-mode)
	)
)


;; (setq google-this-keybind (kbd "C-c ?"))
;; (use-package google-this
;;   :ensure t
;;   :config
;;   (google-this-mode 1)
;;   )


(unless minpkg-switch-found

	(use-package popup
		:ensure t 
		)
)	


;; popper  -- https://github.com/karthink/popper
(use-package popper
  :ensure t ; or :straight t
  :bind (("C-`"   . popper-toggle)
         ("M-`"   . popper-cycle)
         ("C-M-`" . popper-toggle-type))
  :init
  (setq popper-reference-buffers
        '("\\*Messages\\*"
          "Output\\*$"
          "\\*Async Shell Command\\*"
          help-mode
          compilation-mode
		  "^\\*eshell.*\\*$" eshell-mode))
  (popper-mode +1)
  (popper-echo-mode +1))                ; For echo area hints

; popups have no mode lines...
; popups grouped by project...
; echo-area hints to select buffer with number keys
; put popups - help on right, REPL and command at bottom, grep at top ?


(unless minpkg-switch-found

	(use-package google-translate
	  :ensure t
	  :defer 3
	  :bind
	  ("C-c C-t" . google-translate-at-point)
	  ("C-c C-y" . google-translate-at-point-reverse)
	  :custom
	  (google-translate-default-source-language "en")  ;; override with C-u
	  (google-translate-default-target-language "it"))
)


(unless minpkg-switch-found

	(use-package delight
	  :ensure t
	  :config
	  (delight '((abbrev-mode " Abr" abbrev)
				 (overwrite-mode " Ovr" t)
				 (ivy-posframe-mode nil ivy-posframe)
				 ))
	  )
)

(unless minpkg-switch-found

	(use-package magit
	  :ensure t
	  :bind (("C-x g" . magit-status)))

	(use-package git-timemachine
	  :ensure t
	  :bind (("H-g" . git-timemachine))) ;; Hyper-G (Apps-G - see emacs.key.el)
)

(unless minpkg-switch-found

	(use-package treemacs
	  :ensure t
	  :defer t
	  :bind ("<f12>" . treemacs)

	  :init
	  (progn
		(with-eval-after-load 'winum
		  (define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
		(with-eval-after-load 'treemacs
		  (progn
			(define-key treemacs-mode-map [mouse-1] #'treemacs-single-click-expand-action)
			(unbind-key "<next>" treemacs-mode-map)
			(unbind-key "<prior>" treemacs-mode-map)
            ;; want pageup/dn to scroll the *treemacs* buffer if I'm in the treemacs window
            ;; *not* some other window...
			))
	  )
	  :config
	  (progn
		(setq treemacs-collapse-dirs                 (if treemacs-python-executable 3 0)
			  treemacs-deferred-git-apply-delay      0.5
			  treemacs-directory-name-transformer    #'identity
			  treemacs-display-in-side-window        t
			  treemacs-eldoc-display                 'simple     ;; or 't'
			  treemacs-file-event-delay              5000
			  treemacs-file-extension-regex          treemacs-last-period-regex-value
			  treemacs-file-follow-delay             0.2
			  treemacs-file-name-transformer         #'identity
			  treemacs-follow-after-init             t
              treemacs-expand-after-init             t
			  treemacs-git-command-pipe              ""
			  treemacs-goto-tag-strategy             'refetch-index
              treemacs-hide-dot-git-directory        t  
			  treemacs-indentation                   2
			  treemacs-indentation-string            " "
			  treemacs-is-never-other-window         nil
			  treemacs-max-git-entries               5000
			  treemacs-missing-project-action        'ask
              treemacs-move-forward-on-expand        nil 
			  treemacs-no-png-images                 nil
			  treemacs-no-delete-other-windows       t
			  treemacs-project-follow-cleanup        nil
			  treemacs-persist-file                  (expand-file-name ".cache/treemacs-persist" user-emacs-directory)
			  treemacs-position                      'left
              treemacs-read-string-input             'from-child-frame 
			  treemacs-recenter-distance             0.1
			  treemacs-recenter-after-file-follow    nil
			  treemacs-recenter-after-tag-follow     nil
			  treemacs-recenter-after-project-jump   'always
			  treemacs-recenter-after-project-expand 'on-distance
			  treemacs-show-cursor                   nil
			  treemacs-show-hidden-files             t
			  treemacs-silent-filewatch              nil
			  treemacs-silent-refresh                nil
			  treemacs-sorting                       'alphabetic-asc
              treemacs-select-when-already-in-treemacs 'move-back
			  treemacs-space-between-root-nodes      t
			  treemacs-tag-follow-cleanup            t
			  treemacs-tag-follow-delay              1.5
              treemacs-text-scale                    nil
              treemacs-user-header-line-format       nil 
			  treemacs-user-mode-line-format         nil
			  treemacs-width                         35
              treemacs-width-increment               1
              treemacs-width-is-initially-locked     t
              treemacs-workspace-switch-cleanup      nil
          )

		;; The default width and height of the icons is 22 pixels. If you are
		;; using a Hi-DPI display, uncomment this to double the icon size.
		;;(treemacs-resize-icons 44)

		(treemacs-follow-mode t)
		(treemacs-filewatch-mode t)
		(treemacs-fringe-indicator-mode 'always)
        (when treemacs-python-executable
          (treemacs-git-commit-diff-mode t))

		(pcase (cons (not (null (executable-find "git")))
					 (not (null treemacs-python-executable)))
		  (`(t . t)
		   (treemacs-git-mode 'deferred))
		  (`(t . _)
		   (treemacs-git-mode 'simple)))

        (treemacs-hide-gitignored-files-mode nil))
	  :bind
	  (:map global-map
			("M-0"       . treemacs-select-window)
			("C-x t 1"   . treemacs-delete-other-windows)
			("C-x t t"   . treemacs)
            ("C-x t d"   . treemacs-select-directory)
			("C-x t B"   . treemacs-bookmark)
			("C-x t C-t" . treemacs-find-file)
			("C-x t M-t" . treemacs-find-tag)))

	;; (use-package treemacs-evil
	;;   :after treemacs evil
	;;   :ensure t)

;	(use-package treemacs-projectile
;	  :after treemacs projectile
;	  :ensure t)

	(use-package treemacs-icons-dired
      :hook (dired-mode . treemacs-icons-dired-enable-once)
	  :ensure t)
    
	(use-package treemacs-magit
	  :after treemacs magit
	  :ensure t)
)


(unless minpkg-switch-found

	(use-package markdown-mode
	  :ensure t
	  :mode ("README\\.md\\'" . gfm-mode)
	  :init (setq markdown-command "multimarkdown"))
)

(with-timer "amx"   ; alternative M-x   -- present list of choices based on frequency of use
 (use-package amx
   :ensure t
   :init (setq-default amx-save-file (concat config-root-path "/.emacs.d/.amx-items"))
   :bind (("<remap> <execute-extended-command>" . amx))
  )
)


; never really used it - try again someday
;(unless minpkg-switch-found
;	(with-timer "avy" ;; goto char in visible buffer based on char decision tree (faster)
;	  (use-package avy
;		:ensure t
;		:config
;		(setq avy-all-windows 'all-frames)
;		(setq avy-timeout-seconds 1.0)
;		:bind
;		(
;		 ("C-:" . avy-goto-char-timer) ;; ivy-avy C-' use inside ivy C-s search to narrow them down quickly
;		 ;; C-; would be nicer but that's flyspell autocorrect in text mode buffers
;		)
;	  )
;	)
;)


; affects org-roam as git-gutter spawns many processes for each buffer/file and soon exhausts 
; file descriptors and org-roam processing (db-sync) fails part way through with only <100 files
;
;(with-timer "git-gutter"
; (use-package git-gutter
;   :ensure t
;   :init (global-git-gutter-mode +1)
;;; (require 'git-gutter)
; )
;)
;(add-to-list 'git-gutter:update-commands 'other-window)
;
;;; or (add-hook 'elisp-mode-hook 'git-gutter-mode)  ;; to enable when editing el files
;(global-set-key (kbd "C-x v g") 'git-gutter)
;(global-set-key (kbd "C-x v =") 'git-gutter:popup-hunk)
;(global-set-key (kbd "C-x v p") 'git-gutter:previous-hunk)
;(global-set-key (kbd "C-x v n") 'git-gutter:next-hunk)
;(global-set-key (kbd "C-x v s") 'git-gutter:stage-hunk)
;(global-set-key (kbd "C-x v r") 'git-gutter:revert-hunk)
;(global-set-key (kbd "C-x v SPC") #'git-gutter:mark-hunk)
;
;(custom-set-variables
; '(git-gutter:update-interval 2));
;
;;(custom-set-variables
;; '(git-gutter:modified-sign "  ") ;; two space
;; '(git-gutter:added-sign "++")    ;; multiple character is OK
;; '(git-gutter:deleted-sign "--"))
;;(set-face-background 'git-gutter:modified "purple") ;; background color
;;(set-face-foreground 'git-gutter:added "green")
;;(set-face-foreground 'git-gutter:deleted "red")
;
;(custom-set-variables '(git-gutter:disabled-modes '(asm-mode image-mode org-mode)))
;(custom-set-variables '(git-gutter:visual-line t))
;(custom-set-variables '(git-gutter:lighter " GG"))



;; everything.el -- use everything from voidtools

;; not needed - put in ~/.emacs.d/lisp already in load-path
;;    (add-to-list 'load-path "~/path/to/everything.el")
(setq everything-ffap-integration nil)                ;; to disable ffap integration
;; set in emacs.spec.el for individual machines
;; (setq everything-cmd "c:/sys/everything/es.exe")        ;; to let everything.el know where to find es.exe
(require 'everything)

;; use:  M-x everything - enter query pick result shows up in vertico fine



(setq plantuml-local-url "http://localhost:8080/uml") ; Legion17's docker plantuml svr

(unless minpkg-switch-found
 (use-package plantuml-mode
   :ensure t
   :custom
   (plantuml-jar-path (expand-file-name "c:/sys/plantuml/plantuml-1.2024.4.jar"))
   (plantuml-server-url  plantuml-local-url)                                      ;; "http://localhost:8080/uml" 
   (plantuml-default-exec-mode 'jar)
   (plantuml-exec-mode 'jar)
   
   :init
   (add-to-list 'auto-mode-alist '("\\.plantuml\\'" . plantuml-mode))    ;; Enable plantuml-mode for PlantUML files
   (add-to-list 'org-src-lang-modes '("plantuml" . plantuml))
   (setq plantuml-output-type "png")
   (org-display-inline-images)
  )
)

;;; WRITING 
;;

(use-package lorem-ipsum
  :ensure t 
  :init
    ;(lorem-ipsum-use-default-bindings) - C-c l conflicts with org-store-link
)
;  remember the commands:
; C-c l p: lorem-ipsum-insert-paragraphs
; C-c l s: lorem-ipsum-insert-sentences
; C-c l l: lorem-ipsum-insert-list


;; format/display buffer for better reading/writing text
;;
(use-package olivetti
  :ensure t 
)

;; thesaurus
;;
(use-package powerthesaurus
  :ensure t 
  :bind (("s-t"   . powerthesaurus-hydra/body)
         ;("M-`"   . popper-cycle)
         ;("C-M-`" . popper-toggle-type)
		 )
)		 
;	(global-set-key (kbd "C-c TAB") 'hydra-hs/body) ; Example binding

;; dictionary
;;
(global-set-key (kbd "s-d") #'dictionary-lookup-definition)
(setq dictionary-server "dict.org")

;; wikipedia summary
;; 
(use-package wiki-summary
  :ensure t 
  :bind ( ("s-s" . wiki-summary)
        )
)

;;; clean up:

; ;;;;; do have local docker plantuml server working (yay!) 
; ;;;;; now have plantuml-mode working with jar file and *might* work with local server 
; ;;;;; - have to figure out how to configure

; flycheck-plantuml   ??


;(setq plantuml-local-url "http://localhost:8080/uml") ; Legion17's docker plantuml svr
;   ; set per machine in emacs.spec.el if get working on multiple machines 

; plantuml-mode
;
;   C-c C-c  plantuml-preview: renders a PlantUML diagram from the current buffer in the best supported format
;   C-u C-c C-c  plantuml-preview in other window
;   C-u C-u C-c C-c plantuml-preview in other frame

;   #+startup:inlineimages
;   #+BEGIN_SRC plantuml
;   <hit C-c ' here to open a plantuml-mode buffer>
;   #+END_SRC
;   #+results:
;   file:output.png
   
; plantuml-enable-debug
; plantuml-disable-debug
;
; plantuml-output-type "png" ; defaults to txt
