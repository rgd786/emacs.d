(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ac-delay 1.0)
 '(ag-highlight-search t)
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#eee8d5" "#dc322f" "#859900" "#b58900" "#268bd2" "#d33682" "#2aa198" "#839496"])
 '(auto-save-interval 3000)
 '(bm-cycle-all-buffers t)
 '(bm-highlight-style 'bm-highlight-only-fringe)
 '(compilation-message-face 'default)
 '(consult-fd-args
   '((if
         (executable-find "fdfind" 'remote)
         "fdfind" "fd")
     "-I --full-path --color=never"))
 '(counsel-locate-cmd 'counsel-locate-cmd-es)
 '(cua-global-mark-cursor-color "#2aa198")
 '(cua-normal-cursor-color "#657b83")
 '(cua-overwrite-cursor-color "#b58900")
 '(cua-read-only-cursor-color "#859900")
 '(custom-enabled-themes '(adwaita))
 '(custom-safe-themes
   '("7964b513f8a2bb14803e717e0ac0123f100fb92160dcf4a467f530868ebaae3e" "ffafb0e9f63935183713b204c11d22225008559fa62133a69848835f4f4a758c" "a8245b7cc985a0610d71f9852e9f2767ad1b852c2bdea6f4aadc12cce9c4d6d0" "8db4b03b9ae654d4a57804286eb3e332725c84d7cdab38463cb6b97d5762ad26" "31a01668c84d03862a970c471edbd377b2430868eccf5e8a9aec6831f1a0908d" "1297a022df4228b81bc0436230f211bad168a117282c20ddcba2db8c6a200743" "726dd9a188747664fbbff1cd9ab3c29a3f690a7b861f6e6a1c64462b64b306de" "d677ef584c6dfc0697901a44b885cc18e206f05114c8a3b7fde674fce6180879" "8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" "a71be4e5e9e418025daea651f8a1628953abb7af505da5e556e95061b6a6e389" default))
 '(dired-filetype-disabled-diredp-faces t)
 '(dired-sort-menu-saved-config
   '((dired-actual-switches . "-lGh")
     (ls-lisp-ignore-case . t)
     (ls-lisp-dirs-first . t)))
 '(dired-switches-in-mode-line 'as-is)
 '(diredp-hide-details-initially-flag nil)
 '(display-time-day-and-date t)
 '(display-time-mode t)
 '(fci-rule-color "#eee8d5")
 '(filesets-data
   '((".emacs"
      (:files "~/.emacs.d/init.el" "~/.emacs.d/emacs.custom-powerline.el" "~/.emacs.d/emacs.custom-d.el" "~/.emacs.d/emacs.custom-c.el" "~/.emacs.d/emacs.custom-1.el" "~/.emacs.d/emacs.custom.el" "~/.emacs.d/emacs.dashboard.el" "~/.emacs.d/emacs.consult.el" "~/.emacs.d/emacs.comp-vert.el" "~/.emacs.d/emacs.comp-ivy.el" "~/.emacs.d/emacs.cal.el" "~/.emacs.d/emacs.hydra.el" "~/.emacs.d/emacs.dired.el" "~/.emacs.d/emacs.ui-frame.el" "~/.emacs.d/emacs.ui.el" "~/.emacs.d/org.conf.el" "~/.emacs.d/emacs.pkg.el" "~/.emacs.d/emacs.key.el" "~/.emacs.d/emacs.print.el" "~/.emacs.d/emacs.spec.el" "~/.emacs.d/emacs.fn.el" "~/.emacs.d/emacs.cal.el" "~/.emacs.d/emacs.image.el" "~/.emacs.d/emacs.opt.el" "~/.emacs.d/emacs.conf.el"))
     ("dot-files"
      (:files "~/.curlrc" "~/.wgetrc" "~/.bash_profile" "~/.bash_history" "~/.bashrc" "~/.emacs.d/init.el"))
     ("etc-files"
      (:files "~/.emacs.d/projects" "~/.emacs.d/bookmarks" "~/.emacs.d/tramp" "~/.emacs.d/notes" "~/.emacs.d/diary" "~/.emacs.d/dict/c++-mode" "~/.emacs.d/dict/txt" "~/.emacs.d/dict/org" "~/.emacs.d/ispell/alternate-words" "~/.emacs.d/abbrev_defs"))))
 '(find-file-suppress-same-file-warnings t)
 '(flyspell-abbrev-p t)
 '(flyspell-default-dictionary "en_US")
 '(flyspell-issue-message-flag nil)
 '(git-gutter:disabled-modes '(asm-mode image-mode))
 '(git-gutter:lighter " GG")
 '(git-gutter:update-interval 2)
 '(git-gutter:visual-line t)
 '(gnutls-trustfiles
   '("/etc/ssl/certs/ca-certificates.crt" "/etc/pki/tls/certs/ca-bundle.crt" "/etc/ssl/ca-bundle.pem" "/usr/ssl/certs/ca-bundle.crt" "/usr/local/share/certs/ca-root-nss.crt" "/etc/ssl/cert.pem" "c:/sys/etc/ssl/cacert.pem"))
 '(grep-find-ignored-files
   '(".#*" "*.o" "*~" "*.bin" "*.bak" "*.obj" "*.map" "*.ico" "*.pif" "*.lnk" "*.a" "*.ln" "*.blg" "*.bbl" "*.dll" "*.drv" "*.vxd" "*.386" "*.elc" "*.lof" "*.glo" "*.idx" "*.lot" "*.fmt" "*.tfm" "*.class" "*.fas" "*.lib" "*.mem" "*.x86f" "*.sparcf" "*.dfsl" "*.pfsl" "*.d64fsl" "*.p64fsl" "*.lx64fsl" "*.lx32fsl" "*.dx64fsl" "*.dx32fsl" "*.fx64fsl" "*.fx32fsl" "*.sx64fsl" "*.sx32fsl" "*.wx64fsl" "*.wx32fsl" "*.fasl" "*.ufsl" "*.fsl" "*.dxl" "*.lo" "*.la" "*.gmo" "*.mo" "*.toc" "*.aux" "*.cp" "*.fn" "*.ky" "*.pg" "*.tp" "*.vr" "*.cps" "*.fns" "*.kys" "*.pgs" "*.tps" "*.vrs" "*.pyc" "*.pyo" "*.ilk" "*.pch" "*.ocx" "*.exp" "*.pdb" "*.tlb" "*.exe" "*.iobj" "*.idb"))
 '(highlight-changes-colors '("#d33682" "#6c71c4"))
 '(highlight-symbol-colors
   (--map
    (solarized-color-blend it "#fdf6e3" 0.25)
    '("#b58900" "#2aa198" "#dc322f" "#6c71c4" "#859900" "#cb4b16" "#268bd2")))
 '(highlight-symbol-foreground-color "#586e75")
 '(highlight-tail-colors
   '(("#eee8d5" . 0)
     ("#B4C342" . 20)
     ("#69CABF" . 30)
     ("#69B7F0" . 50)
     ("#DEB542" . 60)
     ("#F2804F" . 70)
     ("#F771AC" . 85)
     ("#eee8d5" . 100)))
 '(hl-bg-colors
   '("#DEB542" "#F2804F" "#FF6E64" "#F771AC" "#9EA0E5" "#69B7F0" "#69CABF" "#B4C342"))
 '(hl-fg-colors
   '("#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3"))
 '(image-crop-crop-command '("magick.exe" "+repage" "-crop" "%wx%h+%l+%t" "-" "%f:-"))
 '(image-crop-cut-command
   '("magick.exe" "-draw" "rectangle %l,%t %r,%b" "-fill" "%c" "-" "%f:-"))
 '(image-crop-resize-command '("magick.exe" "-resize" "%wx" "-" "%f:-"))
 '(image-crop-rotate-command '("magick.exe" "-rotate" "%r" "-" "%f:-"))
 '(inhibit-startup-screen t)
 '(line-number-mode nil)
 '(magit-diff-use-overlays nil)
 '(neo-theme 'classic)
 '(org-agenda-files
   '("c:/users/rgd/gitlab/org/work/ABBAdmin.org" "c:/users/rgd/gitlab/org/projects.org" "c:/users/rgd/gitlab/org/rel.org" "c:/users/rgd/gitlab/org/rgd.org" "c:/users/rgd/gitlab/org/todo.org" "c:/users/rgd/gitlab/org/writing.org" "c:/users/rgd/gitlab/org/work/ABBtodo.org" "c:/users/rgd/gitlab/org/work/Composer.org" "c:/users/rgd/gitlab/org/dev/dev.org" "c:/users/rgd/gitlab/org/work/HarmonyAPI.org" "c:/users/rgd/gitlab/org/work/SPE.org"))
 '(org-cycle-separator-lines 1)
 '(org-dropbox-datetree-file "")
 '(org-dropbox-note-dir "")
 '(org-link-frame-setup
   '((vm . vm-visit-folder-other-frame)
     (vm-imap . vm-visit-imap-folder-other-frame)
     (gnus . org-gnus-no-new-news)
     (file . find-file)
     (wl . wl-other-frame)))
 '(org-log-refile 'time)
 '(org-refile-allow-creating-parent-nodes 'confirm)
 '(org-refile-targets '((org-agenda-files :maxlevel . 6) (nil :level . 1)))
 '(org-refile-use-outline-path 'file)
 '(org-return-follows-link t)
 '(package-archives
   '(("gnu" . "http://elpa.gnu.org/packages/")
     ("melpa" . "http://melpa.org/packages/")))
 '(package-selected-packages
   '(casual-re-builder casual-editkit casual-agenda casual dired-git-info dired-git casual-ibuffer casual-bookmarks org-web-tools pandoc-mode ox-pandoc org-transclusion dired-hacks-utils vertico-repeat casual-avy casual-dired casual-calc casual-info casual-isearch wiki-summary powerthesaurus lorem-ipsum olivetti gcmh org-roam-ql-ql org nov org-roam-ql org-ql buffer-move buffer-move backward-forward popper magit-section org-download chatu plantuml-mode doom-themes org-present visual-fill-column websocket wgrep rg use-package-ensure-system-package git-gutter transient kind-icon minimap cape corfu projectile-ripgrep general all-the-icons-completion orderless consult consult-dir consult-flycheck consult-flyspell consult-ls-git consult-notes consult-org-roam consult-projectile consult-yasnippet embark embark-consult embark-vc marginalia vertico vertico-posframe emacsql-sqlite-module emacsql-sqlite-builtin emacsql-sqlite org-roam-timestamps org-roam-ui org-roam elgrep markdown-mode gcode-mode yasnippet-snippets org-contrib treemacs magit org-bookmark-heading org-superstar org-superagenda org-super-agenda git-timemachine org-treeusage google-translate which-key popup treemacs-persp treemacs-magit treemacs-icons-dired treemacs-projectile twittering-mode projectile twittering org-sidebar calfw-org calfw-cal calfw counsel-bbdb bbdb-csv-import bbdb-vcard bbdb org-vcard amx sunshine paradox dired-recent dired-sort dired-filetype-face dired-quick-sort dired-ranger dired-narrow dired-filter dired-collapse dired-subtree peep-dired dired-rainbow google-this undo-tree delight-powerline delight which-key-posframe ivy-yasnippet yasnippet helpful org-wc golden-ration golden-ratio ivy dired+ org-protocol org-mouse mouse3 dired ivy-gitlab deft ivy-rich smex browse-kill-ring bm ivy-posframe use-package fzf avy projectile-speedbar org-bullets names mode-icons ivy-todo ivy-hydra fortune-cookie flyspell-correct dropbox counsel async ivy-xref imenu-anywhere hydra org-dropbox esup dashboard page-break-lines counsel-projectile dired-sort-menu+ dired-sort-menu w32-browser flyspell-correct-ivy powerline sr-speedbar font-lock+ free-keys counsel-gtags edit-server pkg-info))
 '(paradox-github-token t)
 '(pos-tip-background-color "#eee8d5")
 '(pos-tip-foreground-color "#586e75")
 '(powerline-default-separator 'utf-8)
 '(projectile-completion-system 'auto)
 '(projectile-mode-line-prefix " Prj")
 '(projectile-project-root-files
   '("rebar.config" "project.clj" "build.boot" "SConstruct" "pom.xml" "build.sbt" "gradlew" "build.gradle" ".ensime" "Gemfile" "requirements.txt" "setup.py" "tox.ini" "gulpfile.js" "Gruntfile.js" "bower.json" "composer.json" "Cargo.toml" "mix.exs" "stack.yaml" "info.rkt" "DESCRIPTION" "TAGS" "GTAGS" "sla.projectile"))
 '(safe-local-variable-values
   '((org-image-actual-width list 100)
     (org-emphasis-alist
      ("*" bold)
      ("/" italic)
      ("_" underline)
      ("=" org-verbatim verbatim)
      ("~" org-code verbatim)
      ("+"
       (:strike-through nil)))
     (eval load-file org-conf-el)))
 '(tab-width 8)
 '(term-default-bg-color "#fdf6e3")
 '(term-default-fg-color "#657b83")
 '(tool-bar-mode nil)
 '(tooltip-mode nil)
 '(vc-annotate-background nil)
 '(vc-annotate-color-map
   '((20 . "#dc322f")
     (40 . "#c85d17")
     (60 . "#be730b")
     (80 . "#b58900")
     (100 . "#a58e00")
     (120 . "#9d9100")
     (140 . "#959300")
     (160 . "#8d9600")
     (180 . "#859900")
     (200 . "#669b32")
     (220 . "#579d4c")
     (240 . "#489e65")
     (260 . "#399f7e")
     (280 . "#2aa198")
     (300 . "#2898af")
     (320 . "#2793ba")
     (340 . "#268fc6")
     (360 . "#268bd2")))
 '(vc-annotate-very-old-color nil)
 '(warning-suppress-types '((org-element-cache) (org-element-cache))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Consolas" :foundry "outline" :slant normal :weight normal :height 120 :width normal))))
 '(Info-quoted ((t (:inherit fixed-pitch-serif :foreground "steel blue"))))
 '(cursor ((t (:background "navy"))))
 '(dired-directory ((t (:foreground "#0084C8"))))
 '(dired-filetype-execute ((t (:foreground "red"))))
 '(dired-filetype-plain ((t (:foreground "sea green"))))
 '(dired-filetype-source ((t (:foreground "Dark Green"))))
 '(diredp-dir-heading ((t (:background "gainsboro" :foreground "Blue"))))
 '(diredp-dir-name ((t (:background "LightGray" :foreground "blue"))))
 '(diredp-dir-priv ((t (:foreground "DarkRed"))))
 '(diredp-exec-priv ((t (:foreground "green"))))
 '(diredp-read-priv ((t (:foreground "red"))))
 '(diredp-write-priv ((t (:foreground "purple"))))
 '(fixed-pitch ((t (:family "Consolas" :slant normal :weight normal :height 1.0 :width normal))))
 '(highlight ((t (:background "LightSkyBlue1" :foreground "black"))))
 '(ivy-posframe ((t (:inherit default :background "light gray" :foreground "black"))))
 '(org-block ((t (:inherit fixed-pitch))))
 '(org-code ((t (:inherit fixed-pitch))))
 '(org-document-info ((t (:foreground "dark orange"))))
 '(org-document-info-keyword ((t (:inherit (fixed-pitch)))))
 '(org-document-title ((t (:inherit default :weight bold :foreground "black" :height 2.0 :underline nil))))
 '(org-ellipsis ((t (:foreground "DarkGoldenrod"))))
 '(org-headline-done ((t (:weight bold :strike-through "#a33538"))))
 '(org-indent ((t :inherit (org-hide fixed-pitch))))
 '(org-level-1 ((t (:inherit default :weight bold :foreground "black" :height 1.75))))
 '(org-level-2 ((t (:inherit default :weight bold :foreground "black" :height 1.5))))
 '(org-level-3 ((t (:inherit default :weight bold :foreground "black" :height 1.25))))
 '(org-level-4 ((t (:inherit default :weight bold :foreground "black" :height 1.1))))
 '(org-level-5 ((t (:inherit default :weight bold :foreground "black"))))
 '(org-level-6 ((t (:inherit default :weight bold :foreground "black"))))
 '(org-level-7 ((t (:inherit default :weight bold :foreground "black"))))
 '(org-level-8 ((t (:inherit default :weight bold :foreground "black"))))
 '(org-link ((t (:foreground "royal blue" :underline t))))
 '(org-meta-line ((t (:inherit font-lock-comment-face :height 0.8))))
 '(org-property-value ((t (:inherit fixed-pitch))))
 '(org-special-keyword ((t (:inherit font-lock-keyword-face :height 0.7))))
 '(org-table ((t (:foreground "black" :inherit fixed-pitch))))
 '(org-tag ((t (:inherit (fixed-pitch) :weight bold :height 0.8))))
 '(org-verbatim ((t (:inherit (fixed-pitch)))))
 '(powerline-active0 ((t (:background "SteelBlue1" :foreground "black"))))
 '(powerline-active1 ((t (:inherit mode-line :background "SteelBlue3" :foreground "black"))))
 '(powerline-active2 ((t (:inherit mode-line :background "SteelBlue4" :foreground "white"))))
 '(powerline-inactive0 ((t (:inherit mode-line-inactive :background "Grey60" :foreground "black"))))
 '(powerline-inactive1 ((t (:inherit mode-line-inactive :background "Grey50" :foreground "black"))))
 '(powerline-inactive2 ((t (:inherit mode-line-inactive :background "Grey25" :foreground "white"))))
 '(variable-pitch ((t (:family "Source Sans Pro" :height 180 :weight light)))))
