;; do things requiring a frame (i.e. not for daemon mode)

(defvar startup-width-ratio 0.75)
(defvar startup-height-ratio 0.85)
(defvar startup-top-ratio 0.005)
(defvar startup-left-ratio 0.10)

(defun rgd/force-update ()
  (force-mode-line-update t))

;; (run-at-time "60 sec" 60 #'rgd/force-update "Update clock")
;;  -- from https://emacs.stackexchange.com/questions/31208/mode-line-time-not-updating

(defun rgd/pre-setup-frame (frame)
  "Frame setup code; to be done before a frame is created in before-make-frame-hook; 
  called every time a new frame is created (take note - or else remove-hook somewhere)"
  (progn
    ; (message "executing pre-frame creation UI setup code") ;; mostly title bar setup currently

	(rgd/default-frame-setup frame)
  )
)
	
(defun rgd/post-setup-frame (&optional frame)
  "Do frame setup that can't be done in daemon mode startup; that requires fonts/frames to be initialized.
  ******** TODO *******  do a remove-hook somewhere to stop it
  
  based on https://lists.gnu.org/archive/html/help-gnu-emacs/2016-05/msg00182.html 
  it seems for initial frame in daemon mode it doesn't get called with frame parameters
  12/2/20 - have it adjusting frame based on monitor params"
 
  (progn
	(message "executing post-frame creation UI setup code - options, fonts, modeline, geometry")
	
	(display-time-mode 1)

	(setq x-stretch-cursor t)

	(setq cursor-in-non-selected-windows '(hbar . 3))
	
	(if (and (display-graphic-p)  (eq system-type 'windows-nt))
	  (progn
	    ;; default ------
		  
	      ;; DejaVu Sans Mono - nice but a tad bit narrow
          ;; (set-face-font 'default "-outline-DejaVu Sans Mono-normal-normal-normal-mono-14-*-*-*-c-*-iso8859-1")

		  ;; Consolas
		  (set-face-font 'default "-outline-Consolas-normal-normal-normal-mono-18-*-*-*-c-*-iso8859-1")
		
	    ;; modeline ------
		  ;; Droid Sans Mono - a little ragged
		  ;; (set-face-attribute 'mode-line nil :font
		  ;;   "-outline-Droid Sans Mono for Powerline-normal-normal-normal-mono-14-*-*-*-c-*-iso8859-1")

		  ;; Source Code Pro for Powerline
		  (set-face-attribute 'mode-line nil :font
			  "-outline-Source Code Pro Semibold-semibold-normal-normal-mono-18-*-*-*-c-*-iso8859-1")
		  (set-face-attribute 'mode-line-inactive nil :font
			  "-outline-Source Code Pro Semibold-semibold-normal-normal-mono-18-*-*-*-c-*-iso8859-1")

		  ;; D2Coding - some oddities on right-end of mode-line 
          ;; (set-face-attribute 'mode-line nil :font
		  ;;  "-outline-D2Coding for Powerline-normal-normal-normal-mono-15-*-*-*-c-*-iso8859-1")

		  (if (featurep 'org-bullets)
			  (org-bullets-mode 1))

		  ;; Set frame size according to monitor dimensions - 12/2/20 - finally!
		  ;; workarea - 0=X 1=Y 2=WIDTH 3=HEIGHT
		  ;; not worrying about left (X) now - letting make-frame-on-monitor handle that 
		  ;; 
		  (setq fwidth (nth 2 (frame-monitor-geometry)))
		  (setq fheight (nth 3 (frame-monitor-geometry)))
          (message "fheight: %d" fheight)
		  (if (> fwidth 1600 ) 
			  (setq startup-width-ratio 0.60) ;; don't go so big on big monitors
		  )
		  ;(message "Startup width ratio: %f" startup-width-ratio)
		  
		  (setq new-frame-width  (/ (round (* fwidth startup-width-ratio))  (frame-char-width)))
		  (setq new-frame-height (/ (round (* (- fheight 5) startup-height-ratio)) (frame-char-height)))
	 
		  (message "new-frame-width  %d" new-frame-width)
		  (message "new-frame-height %d" new-frame-height)

 		  (set-frame-size (selected-frame) new-frame-width new-frame-height)
		  (set-frame-position (selected-frame) (nth 0 (frame-position)) 5)
	  	  (setq ivy-posframe-width (round (* (frame-width) 0.95)) )

          ;; Let the desktop background show through
          ;; well what do you know - this works on Windows in Emacs 29!
          ;; 
          (set-frame-parameter (selected-frame) 'alpha '(97 . 100))
          (add-to-list 'default-frame-alist '(alpha . (97 . 90)))

          
	  )
	)

	(set-scroll-bar-mode 'left)

    
	;; 9/20/19 - after changing font size of headlines the tags are *way* off to the right;
	;; adjusting them back (changing - 4 to - 20)
    (setq org-agenda-tags-column (- 4 (window-width))) ;; set tags to right side of window
	(setq org-tags-column 75)
	;;(setq org-tags-column (- 20 (window-width)))        ;; use: C-u C-c C-q to realign tags (org-align-all-tags)
	(message "window width %d" (window-width))
	(message "org-tags-column %d" org-tags-column)
	(message "org-agenda-tags-column %d" org-agenda-tags-column)

    (if (display-graphic-p)
	 (progn
       (with-timer "mode icons"
	     (use-package mode-icons
		   :ensure t
		   :config
		   (mode-icons-mode))) 	
	
       (with-timer "powerline"
         (use-package powerline
		   :ensure t
		   :config
		   (require 'delight-powerline)
		   (rgd/powerline-my-theme)                  	;;	(powerline-default-theme)
		  )
	    )
	  )
	)
  )
)


