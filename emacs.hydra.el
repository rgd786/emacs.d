;; hydra and transient both display popup menus with temporary keymaps to present menus
;;
;; red        - call command and continue
;;   pink     - allow and continue (non-heads) continue (heads)
;;   amaranth - disallow and continue (non-heads) continue (heads)
;; blue       - call the command and stop
;;   teal     - allow and quit
;; 
;;
;; hydra (abo/abo) easy to make, colorful, can remain up to do multiple ops without reentering callup key chord.
;;       flyspell good use case, hide-show maybe
;;       org-roam?
;;           org-roam-buffer-toggle       C-c n l **
;;            consult-org-roam-backlinks
;;           org-roam-graph               C-c n g
;;            org-roam-ui-node-zoom
;;            org-roam-ui-node-local
;;            org-roam-ui-follow-mode
;;           org-roam-refile
;;           org-roam-capture             C-c n c
;;           org-roam-extract-subtree (convert subtree at point to a node)
;;           org-roam-db-sync             C-c n s
;;           org-roam-db-query
;;           org-roam-ref-find            C-c n r 
;;           org-roam ref-add             C-c n R
;;           org-roam-tag-add             C-c n t
;;           org-roam-alias-add           C-c n a
;;           org-roam-node-find           C-c n f **
;;           org-roam-node-open
;;           org-roam-rg-search           C-c n x *
;;            consult-org-roam-search
;;           org-roam-grep-visit
;;           org-roam-node-visit
;;           org-roam-ref-remove
;;           org-roam-tag-remove
;;           org-roam-alias-remove
;;           org-roam-buffer-refresh
;;           org-roam-diagnostics
;;           org-roamdb-diagnose-node
;;           org-roam-node-insert         C-c n i
;;           org-roam-node-random
;;           org-roam-
;;            consult-org-roam-...
;;
;;           consult-notes-org-roam-find-node-relation  - navigate org-roam notes by link relation
;;
;;           org-roam-dailies-capture-today C-c n j
;;
;;
;; hydra for modes
;;       git-gutter, linenum?,
;;       golden ratio, whitespace (C-c T w)
;;        fill-column-indicator-mode fill-column
;;       minimap-mode
;;       projectile
;;       yasnippet
;;       bbdb
;;       sunshine
;;       helpful
;;       unicode char insertions (H-3/4/5)
;;       calfw
;;       rg
;;       google translate / google-this
;;       git-timemachine
;;       profiling
;;       gpg
;;       fonts?
;;       
;;
;; transient (magit) good for presenting options for a command
;;       like rg - searching with options
;;

(unless minpkg-switch-found

	;; HYDRA package configuration
	(with-timer "hydra"
	  (use-package hydra
			:ensure t
	  )
	)

	;; HYDRAS

	;; hideshow hydra
	(defhydra hydra-hs (:idle 1.0)
	   "
	Hide^^            ^Show^            ^Toggle^    ^Navigation^
	----------------------------------------------------------------
	_h_ hide all      _s_ show all      _t_oggle    _n_ext line
	_d_ hide block    _a_ show block              _p_revious line
	_l_ hide level                      _e_nable

	_SPC_ cancel
	"
	   ("s" hs-show-all)
	   ("h" hs-hide-all)
	   ("a" hs-show-block)
	   ("d" hs-hide-block)
	   ("t" hs-toggle-hiding)
	   ("l" hs-hide-level)
	   ("n" forward-line)
	   ("p" (forward-line -1))
	   ("e" hs-minor-mode)
	   ("SPC" nil)
	)
	(global-set-key (kbd "C-c TAB") 'hydra-hs/body) ; Example binding
)




;; YANK-POP HYDRA

;;(defhydra hydra-yank-pop ()
;;"hydra-yank-pop
;;   C-y yank
;;   M-y yank-pop
;;   y   yank-pop, next
;;   Y   yank-pop, prev
;;   c   counsel-yank-pop
;;   l   browse kill ring
;;  "
;;  ("C-y" yank nil)
;;  ("M-y" yank-pop nil)
;;  ("y" (yank-pop 1) "next")
;;  ("Y" (yank-pop -1) "prev")
;;  ("c" (counsel-yank-pop) "counsel" :color blue)
;;  ("l" browse-kill-ring "list" :color blue))
;;(global-set-key (kbd "M-y") #'hydra-yank-pop/yank-pop)
;;(global-set-key (kbd "C-y") #'hydra-yank-pop/yank)


;; ;; GOTO LINE HYDRA - consult has goto line M-g g that's as good.
;;
;; (defhydra hydra-goto-line (goto-map ""
;;                            :pre (linum-mode 1)
;;                            :post (linum-mode -1))
;;   "goto-line hydra, e.g.:
;;    M-g 50 RET => goto line 50        5 5 g  => jump ahead to line 55
;;    m  => set mark                    6 5 g  => extend marked region to line 65
;;    M-w => save region to kill-ring and exit hydra
;;   "
;;   ("g" goto-line "go")
;;   ("m" set-mark-command "mark" :bind nil)
;;   ("q" nil "quit"))
;; (global-set-key (kbd "M-g") #'hydra-goto-line/body) ;; or #'hydra-goto-line/goto-line ?



;; RECTANGLE HYDRA
;;
;(defhydra hydra-rectangle (:body-pre (rectangle-mark-mode 1)
;                                     :color pink
;                                     :hint nil
;                                     :post (deactivate-mark))
;  "
;  ^_k_^       _w_ copy      _o_pen       _N_umber-lines            |\\     -,,,--,,_
;_h_   _l_     _y_ank        _t_ype       _e_xchange-point          /,`.-'`'   ..  \-;;,_
;  ^_j_^       _d_ kill      _c_lear      _r_eset-region-mark      |,4-  ) )_   .;.(  `'-'
;^^^^          _u_ndo        _g_ quit     ^ ^                     '---''(./..)-'(_\_)
;"
;  ("k" rectangle-previous-line)
;  ("j" rectangle-next-line)
;  ("h" rectangle-backward-char)
;  ("l" rectangle-forward-char)
;  ("d" kill-rectangle)                    ;; C-x r k
;  ("y" yank-rectangle)                    ;; C-x r y
;  ("w" copy-rectangle-as-kill)            ;; C-x r M-w
;  ("o" open-rectangle)                    ;; C-x r o
;  ("t" string-rectangle)                  ;; C-x r t
;  ("c" clear-rectangle)                   ;; C-x r c
;  ("e" rectangle-exchange-point-and-mark) ;; C-x C-x
;  ("N" rectangle-number-lines)            ;; C-x r N
;  ("r" (if (region-active-p)
;           (deactivate-mark)
;         (rectangle-mark-mode 1)))
;  ("u" undo nil)
;  ("g" nil))      ;; ok
;(global-set-key (kbd "C-x SPC") 'hydra-rectangle/body)


;; UNICODE HYDRA

;(defun my/insert-unicode (unicode-name)
;  "Same as C-x 8 enter UNICODE-NAME."
;  (insert-char (cdr (assoc-string unicode-name (ucs-names)))))

;(global-set-key
; (kbd "C-x 9")
; (defhydra hydra-unicode (:hint nil)
;   "
;        Unicode  _e_ €  _s_ ZERO WIDTH SPACE
;                 _f_ ♀  _o_ °   _m_ µ
;                 _r_ ♂  _a_ →
;        "
;   ("e" (insert-char #x20ac))
;   ("r" (my/insert-unicode "MALE SIGN"))
;   ("f" (my/insert-unicode "FEMALE SIGN"))
;   ("s" (my/insert-unicode "ZERO WIDTH SPACE"))
;   ("o" (insert-char #xb0))   ;; "DEGREE SIGN"
;   ("a" (insert-char #x2192)) ;; "RIGHTWARDS ARROW"
;   ("m" (insert-char #xb5))   ;; "MICRO SIGN"
; )
;)

(global-unset-key (kbd "C-c $")) ;; is flyspell taking it over??

(global-set-key (kbd "C-c $") 'hydra-flyspell/body)

(defhydra hydra-flyspell (:color amaranth
                                 :body-pre
                                 (progn
                                   (when mark-active
                                     (deactivate-mark))
                                   (when (or (not (mark t))
                                             (/= (mark t) (point)))
                                     (push-mark (point) t)))
                                 :hint nil)
  "
 ^Flyspell^         ^Errors^            ^Word^
---------------------------------------------------------
 _b_ check buffer   _c_ correct         _s_ save (buffer)
 _d_ change dict    _n_ goto next       _l_ lowercase (buffer)
 _u_ undo           _p_ goto previous   _a_ accept (session)
 _q_ quit       SPC/DEL - next/prev  M-> M-< C-v M-v
"
  ("b" flyspell-buffer)
  ("d" ispell-change-dictionary)
  ("u" undo-tree-undo)
  ("q" nil :color blue)
  ("C-/" undo-tree-undo)

  ("c" my/flyspell-correct-at-point-maybe-next)
  ("n" my/flyspell-goto-next-error)
  ("p" my/flyspell-goto-previous-error)
  ("." my/flyspell-correct-at-point-maybe-next)
  ("SPC" my/flyspell-goto-next-error)
  ("DEL" my/flyspell-goto-previous-error)

  ("s" my/flyspell-accept-word-buffer)
  ("l" my/flyspell-accept-lowercased-buffer)
  ("a" my/flyspell-accept-word)

  ("M->" end-of-buffer)
  ("M-<" beginning-of-buffer)
  ("C-v" scroll-up-command)
  ("M-v" scroll-down-command))

(defun my/flyspell-error-p (&optional position)
  "Return non-nil if at a flyspell misspelling, and nil otherwise."
  ;; The check technique comes from 'flyspell-goto-next-error'.
  (let* ((pos (or position (point)))
         (ovs (overlays-at pos))
         r)
    (while (and (not r) (consp ovs))
      (if (flyspell-overlay-p (car ovs))
          (setq r t)
        (setq ovs (cdr ovs))))
    r))

(defvar my/hydra-flyspell-direction 'forward)
(defun my/flyspell-correct-at-point-maybe-next ()
  (interactive)
  (cond ((my/flyspell-error-p)
         (save-excursion
           (flyspell-correct-at-point)))
        ((equal my/hydra-flyspell-direction 'forward)
         (my/flyspell-goto-next-error)
         ;; recheck, for 'my/flyspell-goto-next-error' can legitimately stop
         ;; at the end of buffer
         (when (my/flyspell-error-p)
           (save-excursion
             (flyspell-correct-at-point))))
        ((equal my/hydra-flyspell-direction 'backward)
         (my/flyspell-goto-previous-error)
         ;; recheck, for 'my/flyspell-goto-previous-error' can legitimately
         ;; stop at the beginning of buffer
         (when (my/flyspell-error-p)
           (save-excursion
             (flyspell-correct-at-point))))))

;; Just an adapted version of 'flyspell-goto-next-error'.
(defun my/flyspell-goto-previous-error ()
  "Go to the previous previously detected error.
In general FLYSPELL-GOTO-PREVIOUS-ERROR must be used after
FLYSPELL-BUFFER."
  (interactive)
  (setq my/hydra-flyspell-direction 'backward)
  (let ((pos (point))
        (min (point-min)))
    (if (and (eq (current-buffer) flyspell-old-buffer-error)
             (eq pos flyspell-old-pos-error))
        (progn
          (if (= flyspell-old-pos-error min)
              ;; goto end of buffer
              (progn
                (message "Restarting from end of buffer")
                (goto-char (point-max)))
            (backward-word 1))
          (setq pos (point))))
    ;; seek the previous error
    (while (and (> pos min)
                (not (my/flyspell-error-p pos)))
      (setq pos (1- pos)))
    (goto-char pos)
    (when (eq (char-syntax (preceding-char)) ?w)
      (backward-word 1))
    ;; save the current location for next invocation
    (setq flyspell-old-pos-error (point))
    (setq flyspell-old-buffer-error (current-buffer))
    (if (= pos min)
        (message "No more miss-spelled word!")))
  ;; After moving, check again if we are at a misspelling (accepting a word
  ;; might have changed this, since the last check).  If not, go to the next
  ;; error again, unless we are at point-min (otherwise we might enter into
  ;; infinite loop, if there are no remaining errors).
  (flyspell-word)
  (unless (or (= (point) (point-min))
              (my/flyspell-error-p))
    (my/flyspell-goto-previous-error))
  (when (my/flyspell-error-p)
    (swiper--ensure-visible)))

(defun my/flyspell-goto-next-error ()
  "Go to the next previously detected error.
In general FLYSPELL-GOTO-NEXT-ERROR must be used after
FLYSPELL-BUFFER."
  ;; Just a recursive wrapper on the original flyspell function, which takes
  ;; into account possible changes of accepted words since the last check.
  (interactive)
  (setq my/hydra-flyspell-direction 'forward)
  (flyspell-goto-next-error)
  ;; After moving, check again if we are at a misspelling.  If not, go to
  ;; the next error again.
  (flyspell-word)
  (unless (or (= (point) (point-max))
              (my/flyspell-error-p))
    (my/flyspell-goto-next-error))
  (when (my/flyspell-error-p)
    (swiper--ensure-visible)))

(defun my/flyspell-accept-word (&optional local-dict lowercase)
  "Accept word at point for this session.
If LOCAL-DICT is non-nil, also add it to the buffer-local
dictionary. And if LOWERCASE is non-nil, do so with the word
lower-cased."
  (interactive)
  (let ((word (flyspell-get-word)))
    (if (not (and (consp word)
                  ;; Check if we are actually at a flyspell error.
                  (my/flyspell-error-p (car (cdr word)))))
        (message "Point is not at a misspelling.")
      (let ((start (car (cdr word)))
            (word (car word)))
        (when (and local-dict lowercase)
          (setq word (downcase word)))
        ;; This is just taken (slightly adjusted) from
        ;; 'flyspell-do-correct', which is essentially what
        ;; 'ispell-command-loop' also does.
        (ispell-send-string (concat "@" word "\n"))
        (add-to-list 'ispell-buffer-session-localwords word)
        (or ispell-buffer-local-name ; session localwords might conflict
            (setq ispell-buffer-local-name (buffer-name)))
        (flyspell-unhighlight-at start)
        (if (null ispell-pdict-modified-p)
            (setq ispell-pdict-modified-p
                  (list ispell-pdict-modified-p)))
        (if local-dict
            (ispell-add-per-file-word-list word))))))

(defun my/flyspell-accept-word-buffer ()
  "See `my/flyspell-accept-word'."
  (interactive)
  (my/flyspell-accept-word 'local-dict))
(defun my/flyspell-accept-lowercased-buffer ()
  "See `my/flyspell-accept-word'."
  (interactive)
  (my/flyspell-accept-word 'local-dict 'lowercase))


;;;;;;;;;;;;; Window management


(defun hydra-move-splitter-left (arg)
  "Move window splitter left."
  (interactive "p")
  (if (let ((windmove-wrap-around))
        (windmove-find-other-window 'right))
      (shrink-window-horizontally arg)
    (enlarge-window-horizontally arg)))

(defun hydra-move-splitter-right (arg)
  "Move window splitter right."
  (interactive "p")
  (if (let ((windmove-wrap-around))
        (windmove-find-other-window 'right))
      (enlarge-window-horizontally arg)
    (shrink-window-horizontally arg)))

(defun hydra-move-splitter-up (arg)
  "Move window splitter up."
  (interactive "p")
  (if (let ((windmove-wrap-around))
        (windmove-find-other-window 'up))
      (enlarge-window arg)
    (shrink-window arg)))

(defun hydra-move-splitter-down (arg)
  "Move window splitter down."
  (interactive "p")
  (if (let ((windmove-wrap-around))
        (windmove-find-other-window 'up))
      (shrink-window arg)
    (enlarge-window arg)))

(defun joe-scroll-other-window()
  (interactive)
  (scroll-other-window 1))

(defun joe-scroll-other-window-down ()
  (interactive)
  (scroll-other-window-down 1))


(defun ace-tear-off-window ()
  "Select a window with ace-window and tear it off the frame.

This displays the window in a new frame, see `tear-off-window'."
  (interactive)
  (when-let ((win (aw-select " ACE"))
             (buf (window-buffer win))
             (frame (make-frame)))
    (select-frame frame)
    (pop-to-buffer-same-window buf)
    (delete-window win)))

(defhydra hydra-window-frame ()
  "Frame"
  ("f" make-frame "new frame")
  ("x" delete-frame "delete frame"))




(global-set-key (kbd "s-w") 'hydra-window/body)
;
; ^-- move this to emacs.key.el ?  (load key after hydra?)
;
; or M-p ?  close to M-o ( no - ace-window)

; see https://github.com/abo-abo/ace-window/wiki for other ideas
; and https://github.com/abo-abo/hydra/wiki/Window-Management 

; quick move point to other window (windmove)
; when moving point cycle to windows in other frames?  C-= 
; split/unsplit windows
; resize splits 
; frame - new, delete 
; winner undo/redo

;  consult-buffer-other-frame (C-x 5 b) 

;(require 'buffer-move)

(defhydra hydra-window (:color pink :hint nil :timeout 20)
  "
         Move                    Resize                      Swap              Split
╭─────────────────────────────────────────────────────────────────────────────────────────┐
         ^_<up>_^                    ^_C-<up>_^                      ^_M-<up>_^            [_v_]ertical
          ^^▲^^                         ^^▲^^                           ^^▲^^              [_h_]orizontal
 _<left>_ ◀   ▶ _<right>_    _C-<left>_ ◀   ▶ _C-<right>_   _M-<left>_ ◀   ▶ _M-<right>_
          ^^▼^^                         ^^▼^^                           ^^▼^^              ╭──────────┐
        ^_<down>_^                  ^_C-<down>_^                    ^_M-<down>_^           quit : [_SPC_]
s-◀ winner-undo   s-▶ winner-redo  ^_a_^ ace ^_t_^ tear   ^_f_^ new frame  ^_x_^ delete frame
"
  ("<left>" windmove-left)
  ("<down>" windmove-down)
  ("<up>" windmove-up)
  ("<right>" windmove-right)
  ("h" split-window-below)
  ("v" split-window-right)
  ("C-<up>" hydra-move-splitter-up) 
  ("C-<down>" hydra-move-splitter-down)
  ("C-<left>" hydra-move-splitter-left)
  ("C-<right>" hydra-move-splitter-right)
  ("M-<up>" buf-move-up)
  ("M-<down>" buf-move-down)
  ("M-<left>" buf-move-left)
  ("M-<right>" buf-move-right)
  ("SPC" nil)
  ("a" ace-window)
  ("t" ace-tear-off-window)
  ("f" make-frame)
  ("x" delete-frame))

; ("a" ace-window "ace") -- call ace-window from this hydra   ; https://oremacs.com/2015/01/29/more-hydra-goodness/

; source: https://www.reddit.com/r/emacs/comments/7evidd/windmove_shortcuts/


(defhydra hydra-window2 ()
   "
Movement^^        ^Split^         ^Switch^		^Resize^        ^Frame^
-------------------------------------------------------------------------
_h_ ←       	_v_ertical    	_b_uffer		_q_ X←         _f_rame new
_j_ ↓        	_x_ horizontal	_F_ind files	_w_ X↓         _X_ del frm
_k_ ↑        	_z_ undo      	_a_ce 1		_e_ X↑
_l_ →        	_Z_ reset      	_s_wap		_r_ X→
_F_ollow		_D_lt Other   	_S_ave		max_i_mize
_SPC_ cancel	_o_nly this   	_d_elete
_n_ scr o dn
_p_ scr o up
"
   ("h" windmove-left )
   ("j" windmove-down )
   ("k" windmove-up )
   ("l" windmove-right )
   ("n" joe-scroll-other-window)
   ("p" joe-scroll-other-window-down)
   ("q" hydra-move-splitter-left)  
   ("w" hydra-move-splitter-down)
   ("e" hydra-move-splitter-up)
   ("r" hydra-move-splitter-right)
   ("b" consult-buffer)
   ("F" find-file)
   ("f" make-frame) ; hydra-window-frame)
   ("X" delete-frame) 
   ("a" (lambda ()
          (interactive)
          (ace-window 1)
          (add-hook 'ace-window-end-once-hook
                    'hydra-window/body))
       )
   ("v" (lambda ()
          (interactive)
          (split-window-right)
          (windmove-right))
       )
   ("x" (lambda ()
          (interactive)
          (split-window-below)
          (windmove-down))
       )
   ("s" (lambda ()
          (interactive)
          (ace-window 4)
          (add-hook 'ace-window-end-once-hook
                    'hydra-window/body)))
   ("S" save-buffer)
   ("d" delete-window)
   ("D" (lambda ()
          (interactive)
          (ace-window 16)
          (add-hook 'ace-window-end-once-hook
                    'hydra-window/body))
       )
   ("o" delete-other-windows)
   ("i" ace-maximize-window)
   ("z" (progn
          (winner-undo)
          (setq this-command 'winner-undo))
   )
   ("Z" winner-redo)
   ("SPC" nil)
)


;; transients
;;
;; see: https://github.com/positron-solutions/transient-showcase/tree/master
;;
;; https://magit.vc/manual/transient/index.html  - the manual



(defhydra hydra-projectile-other-window (:color teal)
  "projectile-other-window"
  ("f"  projectile-find-file-other-window        "file")
  ("g"  projectile-find-file-dwim-other-window   "file dwim")
  ("d"  projectile-find-dir-other-window         "dir")
  ("b"  projectile-switch-to-buffer-other-window "buffer")
  ("q"  nil                                      "cancel" :color blue))

(defhydra hydra-projectile (:color teal
                            :hint nil)
  "
     PROJECTILE: %(projectile-project-root)

     Find File            Search/Tags          Buffers                Cache
------------------------------------------------------------------------------------------
_s-f_: file            _a_: ag                _i_: Ibuffer           _c_: cache clear
 _ff_: file dwim       _g_: update gtags      _b_: switch to buffer  _x_: remove known project
 _fd_: file curr dir   _o_: multi-occur     _s-k_: Kill all buffers  _X_: cleanup non-existing
  _r_: recent file                                               ^^^^_z_: cache current
  _d_: dir

"
  ("a"   projectile-ag)
  ("b"   projectile-switch-to-buffer)
  ("c"   projectile-invalidate-cache)
  ("d"   projectile-find-dir)
  ("s-f" projectile-find-file)
  ("ff"  projectile-find-file-dwim)
  ("fd"  projectile-find-file-in-directory)
  ("g"   ggtags-update-tags)
  ("s-g" ggtags-update-tags)
  ("i"   projectile-ibuffer)
  ("K"   projectile-kill-buffers)
  ("s-k" projectile-kill-buffers)
  ("m"   projectile-multi-occur)
  ("o"   projectile-multi-occur)
  ("s-p" projectile-switch-project "switch project")
  ("p"   projectile-switch-project)
  ("s"   projectile-switch-project)
  ("r"   projectile-recentf)
  ("x"   projectile-remove-known-project)
  ("X"   projectile-cleanup-known-projects)
  ("z"   projectile-cache-current-file)
  ("`"   hydra-projectile-other-window/body "other window")
  ("q"   nil "cancel" :color blue))
  
(global-set-key (kbd "C-c p") 'hydra-projectile/body)  

(use-package ibuffer
  :hook (ibuffer-mode . ibuffer-auto-mode)
  :defer t)

;; Charles Choi's Casual packages - transients but like hydra
(use-package casual
  :ensure t
  :after org
  :bind (:map
         ibuffer-mode-map
         ("C-o" . casual-ibuffer-tmenu)
         ("F" . casual-ibuffer-filter-tmenu)
         ("s" . casual-ibuffer-sortby-tmenu)
         ("<double-mouse-1>" . ibuffer-visit-buffer) ; optional
         ("M-<double-mouse-1>" . ibuffer-visit-buffer-other-window) ; optional
         ("{" . ibuffer-backwards-next-marked) ; optional
         ("}" . ibuffer-forward-next-marked)   ; optional
         ("[" . ibuffer-backward-filter-group) ; optional
         ("]" . ibuffer-forward-filter-group)  ; optional
         ("$" . ibuffer-toggle-filter-group))  ; optional
)

(message "Adding casual-isearch")
(keymap-set isearch-mode-map "<f2>" #'casual-isearch-tmenu)  

;(message "Adding casual-info")
;(keymap-set Info-mode-map "C-o" #'casual-info-tmenu)   

(message "Adding casual-dired")
(keymap-set dired-mode-map "C-o" #'casual-dired-tmenu)
(keymap-set dired-mode-map "s" #'casual-dired-sort-by-tmenu) ; optional
(keymap-set dired-mode-map "/" #'casual-dired-search-replace-tmenu) ; optional  
   
; bindings to make jumping consistent between Org Agenda and Casual Agenda
;(keymap-set org-agenda-mode-map "M-j" #'org-agenda-clock-goto) ; optional
;(keymap-set org-agenda-mode-map "J" #'bookmark-jump) ; optional
  
;(message "Adding casual-editkit")
;(keymap-global-set "C-o" #'casual-editkit-main-tmenu)

;(message "Adding casual-info")
;(keymap-set Info-mode-map "C-o" #'casual-info-tmenu)

;(message "Adding casual-bookmarks")
;(require 'casual-bookmarks)
;(keymap-set bookmark-bmenu-mode-map "C-o" #'casual-bookmarks-tmenu)

;(message "Adding casual-calc")
;(require 'casual-calc)
;(keymap-set calc-alg-map "C-o" #'casual-calc-tmenu)
;(keymap-set calc-mode-map "C-o" #'casual-calc-tmenu)  

;(message "Adding casual-re-builder")
;(require 'casual-re-builder)
;(keymap-set reb-mode-map "C-o" #'casual-re-builder-tmenu)
;(keymap-set reb-lisp-mode-map "C-o" #'casual-re-builder-tmenu)
