;; org.conf.el
;;
;;  see org.conf.archive.el - for previous stuff and extra comments, hints, etc.

(message "Configuring for org-mode")

;; chatu - when drawing with draw.io or plantuml and want to insert svg image exported from the file the operation is non-trivial
;;         this extension provides two commands to reduce the manual work - 'chatu-new', 'chatu-add', 'chatu-open'
;;         in chatu-mode   C-c C-o => chatu-open
;;                         C-c C-c => chatu-add
(use-package chatu
    :ensure t
    :delight
)

(use-package org
	:ensure t
	:init
	  (setq org-directory (downcase(expand-file-name (concat org-root-path "/org") )))
	:bind
	  (("\C-cl" . org-store-link)
	   ("\C-cc" . org-capture)
	   ("\C-ca" . org-agenda)
	   ("\C-cb" . org-switchb)
	   ("s-<left>" . org-mark-ring-goto))  ; yasnippet uses org's C-c & as a prefix for several functions.
    :hook
       (org-mode . chatu-mode)
    :config
	  (require 'org-mouse)
	  (require 'org-protocol)
	  (require 'org-tempo)
	  (setq org-link-file-path-type 'relative)
	  (setq org-use-speed-commands t)
            ;; use when on headline, '?' to see list
            ;; customize 'org-speed-commands-user' to modify
	  (setq org-agenda-include-diary t)
          ;; dropbox blocked at work, not using mobile-org since using Organice
          ;; (fingers-crossed it lives a long life)
      ;; (setq org-mobile-directory (concat dropbox-root-path "/Apps/MobileOrg"))
      ;; dropbox/org/mobile ?? not needed since organice
      ;;
      (setq org-mobile-inbox-for-pull (concat org-root-path "/org/mobileorg.org"))
      (setq org-goto-interface 'outline-path-completion) ;; For Ivy support in org-goto:
      (setq org-outline-path-complete-in-steps nil)
	  (setq org-clock-clocked-in-display 'frame-title)
	  (setq org-hide-emphasis-markers t)
      (setq org-adapt-indentation 'headline-data)

      (setq org-hide-block-startup t)
      (setq org-startup-folded "fold")
      
	  ;; Place tags close to the right-hand side of the window
	  (add-hook 'org-finalize-agenda-hook 'place-agenda-tags)
	  (defun place-agenda-tags ()
		"Put the agenda tags by the right border of the agenda window."
		(setq org-agenda-tags-column (- 4 (window-width)))
		(org-agenda-align-tags))
	  
      (set-face-attribute 'org-tag nil :foreground "goldenrod" :height 0.8)
	  
	(if (display-graphic-p)
	 (progn 
	   ; from: https://zzamboni.org/post/beautifying-org-mode-in-emacs/
	   ; prettify the faces used for headlines
	  (let* ((variable-tuple
			  (cond ((x-list-fonts "Source Sans Pro") '(:font "Source Sans Pro"))
					((x-list-fonts "Lucida Grande")   '(:font "Lucida Grande"))
					((x-list-fonts "Verdana")         '(:font "Verdana"))
					((x-family-fonts "Sans Serif")    '(:family "Sans Serif"))
					(nil (warn "Cannot find a Sans Serif Font.  Install Source Sans Pro."))))
			 (base-font-color     (face-foreground 'default nil 'default))
			 (headline           `(:inherit default :weight bold :foreground ,base-font-color)))

		(custom-theme-set-faces
		 'user
		 `(variable-pitch ((t (:family "Source Sans Pro" :height 120 :weight light))))
		 `(fixed-pitch ((t (:family "Consolas" :slant normal :weight normal :height 1.0 :width normal))))
		 `(org-level-8 ((t (,@headline ,@variable-tuple))))
		 `(org-level-7 ((t (,@headline ,@variable-tuple))))
		 `(org-level-6 ((t (,@headline ,@variable-tuple))))
		 `(org-level-5 ((t (,@headline ,@variable-tuple))))
		 `(org-level-4 ((t (,@headline ,@variable-tuple :height 1.1))))
		 `(org-level-3 ((t (,@headline ,@variable-tuple :height 1.25))))
		 `(org-level-2 ((t (,@headline ,@variable-tuple :height 1.5))))
		 `(org-level-1 ((t (,@headline ,@variable-tuple :height 1.75))))
		 `(org-document-title ((t (,@headline ,@variable-tuple :height 2.0 :underline nil))))
		 `(org-block ((t (:inherit fixed-pitch))))
		 `(org-code ((t (:inherit fixed-pitch))))
		 `(org-document-info ((t (:foreground "dark orange"))))
         `(org-document-info-keyword ((t (:inherit (fixed-pitch)))))
		 `(org-headline-done ((t (:weight bold :strike-through "red"))))
	     `(org-indent ((t :inherit (org-hide fixed-pitch))))
		 `(org-link ((t (:foreground "royal blue" :underline t))))
		 `(org-meta-line ((t (:inherit (font-lock-comment-face fixed-pitch)))))
		 `(org-property-value ((t (:inherit fixed-pitch))) t)
		 `(org-special-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)))))
		 `(org-table ((t (:inherit fixed-pitch :foreground "#83a598"))))
		 `(org-tag ((t (:inherit (fixed-pitch) :weight bold :height 0.8))))
		 `(org-verbatim ((t (:inherit (fixed-pitch)))))
		 ))
	  )
	)
	
	  (setq org-agenda-text-search-extra-files
            (list
			 (expand-file-name (concat org-directory "/work/ABBTodo.org_archive"))
			 (expand-file-name (concat org-directory "/work/HarmonyAPI.org_archive"))
			 (expand-file-name (concat org-directory "/work/NG.org_archive"))
			 (expand-file-name (concat org-directory "/work/SPE.org_archive"))
			 (expand-file-name (concat org-directory "/todo.org_archive"))
			 (expand-file-name (concat org-directory "/notes.org_archive"))
			 ) )

      (cond
        ((string= system-location "Work")
          (setq org-agenda-files
            (list
			  (expand-file-name (concat org-directory "/work/ABBAdmin.org"))
			  (expand-file-name (concat org-directory "/work/ABBtodo.org"))
			  (expand-file-name (concat org-directory "/work/Composer.org"))
			  (expand-file-name (concat org-directory "/dev/dev.org"))
			  (expand-file-name (concat org-directory "/work/HarmonyAPI.org"))
			  (expand-file-name (concat org-directory "/work/NG.org"))
			  (expand-file-name (concat org-directory "/work/SPE.org"))
			  (expand-file-name (concat org-directory "/work/Training.org"))
			  )) )
          ((string= system-location "Home")
            (setq org-agenda-files
             (list
			  (expand-file-name (concat org-directory "/projects.org"))
			  (expand-file-name (concat org-directory "/rel.org"))
			  (expand-file-name (concat org-directory "/rgd.org"))
			  (expand-file-name (concat org-directory "/todo.org"))
			  (expand-file-name (concat org-directory "/home.org"))
			  (expand-file-name (concat org-directory "/mom.org"))
			  (expand-file-name (concat org-directory "/writing.org"))
			  )) )
          (message "WARNING - On unknown computer - no agenda files set\n")
      )

	  ;; org capture templates
	  ;; https://orgmode.org/manual/Template-elements.html 
	  ;; ( "<key>"      - key to activate this template
	  ;;   "description"
	  ;;   <type>       - entry      - org node
	  ;;                - item       - plain list item
	  ;;                - checkitem  - checkbox list item
	  ;;                - table-line - new line in table
	  ;;                - plain      - text inserted as is
	  ;;  <target>      - '(file "path/to/file")'
	  ;;                - '(id "id of existing org entry")'
	  ;;                - '(file+headline "filename" "node headline")'
	  ;;                - '(file+olp "filename" "level1 heading" "level2" ...)'
	  ;;                - '(file+regexp "filename" "regexp to locate location")'
	  ;;                - '(file+olp+datetree "filename" [ "Level 1 heading" ...])’
	  ;;                - '(file+function "filename" function-finding-location)'
	  ;;                - '(clock)'  - file to entry currently being clocked
	  ;;                - '(function function-finding-location)'
	  ;; <template>

;;	            ("d" "Diary" entry
;;		   (file+olp+datetree (lambda () (concat org-directory "/todo.org")) "Calendar")
;;;;            "%K - %a\n%i\n%?\n")
;;		   "* Diary %^g
;;            %i
;;            %?")

     (setq org-capture-templates
      '(
        ("m" "Scheduled work meeting (ABBtodo Calendar)" entry
              (file+headline (lambda () (concat org-directory "/work/ABBtodo.org")) "Calendar")
              "* TODO %^{Brief description} %^g
              SCHEDULED: %^T
              %i
              %?
              %a")

        ("i" "Work task - interruption (ABBtodo Calendar)" entry
             (file+headline (lambda () (concat org-directory "/work/ABBtodo.org")) "Calendar")
             "* TODO %^{Brief description} %^g
              SCHEDULED: %^T
              %i
              %?
              %a")

        ("s" "SPE task - (SPE.org Calendar)" entry
             (file+headline (lambda () (concat org-directory "/work/SPE.org")) "Calendar")
             "* TODO %^{Brief description} %^g
              SCHEDULED: %^T
              %i
              %?
              %a")

        ("c" "CAPI task - (HarmonyAPI.org Calendar)" entry
             (file+headline (lambda () (concat org-directory "/work/HarmonyAPI.org")) "Calendar")
             "* TODO %^{Brief description} %^g
              SCHEDULED: %^T
              %i
              %?
              %a")


        ("e" "Email (ABBtodo email)" entry
             (file+headline (lambda () (concat org-directory "/work/ABBtodo.org")) "general email questions etc.")
             "* TODO %^{Brief description} %^g
              SCHEDULED: %^T
              %i
              %?
              %a")
        )
      )

     (setq org-capture-templates
      (append org-capture-templates
       '(
           ("h" "home")

           ("hd" "Diary (todo.org Calendar)" entry
       (file+olp+datetree (lambda () (concat org-directory "/todo.org")) "Calendar")
       "* Diary %a\n%i\n%?\n")

           ("hn" "Note (notes.org) Notes" entry (file+headline "" "Notes")
                 "* Note: %?\n  %U\n  %i\n  %a\n\n\n\n" :empty-lines 1)
           ("hL" "Link (notes.org) Links" entry (file+headline "" "Links")
                 "* Link: %?\n  %U\n  %i\n  %a\n\n\n\n" :empty-lines 1)
           ("hp" "Protocol" entry (file+headline "" "Inbox")
                 "* %^{Title}\nSource: %u, %c\n #+BEGIN_QUOTE\n%i\n#+END_QUOTE\n\n\n%?")
           ("ha" "Appointment (todo.org) Calendar" entry
               (file+olp+datetree (lambda () (concat org-directory "/todo.org")) "Calendar")
          "* TODO %^{Brief description} %^g
              SCHEDULED: %^T
              %i
              %?")

           ("ho" "mOm stuff (mom.org) Calendar" entry (file+headline
             (lambda () (concat org-directory "/mom.org")) "Calendar")
             "* TODO %^{Brief description} %^g
              SCHEDULED: %^T
              %i
              %?
              %a")
           ("ht" "Todo (todo.org) Tasks" entry
              (file+headline (lambda () (concat org-directory "/todo.org")) "Tasks")
                "* TODO %?\n  %i\n  %a")
         )
       )
     )

      (setq org-tag-persistent-alist '(
				 ("ABB" .       ?A)
				 ("Cars" .      ?a)
				 ("call" .      ?c)
				 ("Craig" .     ?C)
				 ("Deb" .       ?D)
				 ("Emacs" .     ?e)
				 ("Finances" .  ?F)
				 ("Family" .    ?f)
				 ("get" .       ?g)
				 ("HAPI" .      ?H)
				 ("Home" .      ?h)
				 ("Insurance" . ?i)
				 ("Jamie" .     ?J)
				 ("lookup" .    ?l)
				 ("Mom" .       ?M)
				 ("Medical" .   ?m)
				 ("org" .       ?o)
				 ("Church" .    ?p) ;; pioneer
				 ("Rel" .       ?r)
				 ("Sean" .      ?S)
				 ("College" .   ?s) ;; school
				 ("Traveller" . ?T)
				 ("School" .    ?w)
				 ("Composer" .  ?y) ;; coyote
				 ("Computer" .  ?z)
				 ))
)
	 
;(require 'org-mouse)	

;(require 'org-protocol)
(setq org-default-notes-file (concat org-directory "/notes.org")) 

;(require 'org-tempo)

(use-package org-wc
  :ensure t
  :after (org)
  )

(use-package org-superstar
  :ensure t
  :after (org)
  :hook (org-mode . org-superstar-mode)
  :config
  (setq org-superstar-headline-bullets-list '("⚫" "◉" "◎" "●" "○" "⯍" "⯎" "⦁" "⬥" "⬦" "⬧" "⬨" "⬩" "⬪" "⬫" )  )


  )

(setq org-ellipsis "…");; ⤵ ≫
     ;; or
     ;;right arrows
     ;; “↝” “⇉” “⇝” “⇢” “⇨” “⇰” “➔” “➙” “➛” “➜” “➝” “➞”
     ;; “➟” “➠” “➡” “➥” “➦” “➧” “➨”
     ;; “➩” “➪” “➮” “➯” “➱” “➲”
     ;; “➳” “➵” “➸” “➺” “➻” “➼” “➽”
     ;; arrow heads
     ;; “➢” “➣” “➤” “≪”, “≫”, “«”, “»”
     ;; other arrows
     ;; “↞” “↠” “↟” “↡” “↺” “↻”
     ;; lightening
     ;; “⚡”
     ;; other symbols
     ;; …, ▼, ↴, , ∞, ⬎, ⤷, ⤵


     ;; (use-package org-bullets 
     ;;   :ensure t
     ;;   :init
     ;;   (setq org-bullets-bullet-list  ;; most of these are from Symbola font q.v.
     ;;   ;; '("◉" "◎" "○" "►" "◇" "⚫"))
     ;;   ;; '("⓵" "⓶" "⓷" "⓸" "⓹" "⓺" "⓻" "⓼" "⓽" "⓾" "⨀" "⭙" "⨷" "◈"))
     ;;   ;; '("➊" "➋" "➌" "➍" "➎" "➏" "➐" "➑" "➒" "➓"))
     ;;   ;; '("⬤" "☀" "⬢" "⬣" "⬟" "⬠" "⬡" "⯁" "⯀" "⯍" "⯎" "⦁" "⬥" "⬦" "⬧" "⬨" "⬩" "⬪" "⬫"))
     ;; 		;; '("⬤" "◯" "⚫" "◉" "◎" "●" "○" "⯍" "⯎" "⦁" "⬥" "⬦" "⬧" "⬨" "⬩" "⬪" "⬫"))
     ;; 		;; - big bullet just a tad too big... (custom font size?)
     ;;  	 '("⚫" "◉" "◎" "●" "○" "⯍" "⯎" "⦁" "⬥" "⬦" "⬧" "⬨" "⬩" "⬪" "⬫"))
     ;;   :after (org)
     ;;   :config
     ;;   (setcdr org-bullets-bullet-map nil)
     ;;   (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
     ;; ;;   (org-bullets-mode 1)
     ;;   )

(use-package org-bookmark-heading
  :ensure t
  :after (org)
  )

(use-package org-treeusage
  :ensure t
  :bind ("C-c u". org-treeusage-mode)
  :after (org)
  )

(use-package org-super-agenda
  :ensure t
  :after (org)
  :config
  (setq org-agenda-span 'day)
  (setq org-agenda-window-setup 'only-window)
  (setq org-super-agenda-groups
		'(
		  (:name "Log" :log t :order 1) ;; group logged items in own group here, not spreadout in Schedule grid
          (:name "Schedule"
                 :time-grid t
				 :order 2)
          (:name "Scheduled earlier"
                :scheduled past
				:order 3)
		  (:name "Work Today"
		         :and (:scheduled today :tag "work" :priority "A")
				 :order 4)
          (:name "Work"
		         :and (:tag "work" :priority<= "A")
				 :order 5)
		  (:name "Home" :tag "home" :order 6)
		  (:name "Mom" :tag "mom" :order 7)
		  (:name "Overdue"   :deadline past :order 8)
		  (:name "Due today" :deadline today :order 9)
          (:name "Due soon"  :deadline future :order 10)
		  (:name "Habits" :habit t :order 11)
          )
  )
  (org-super-agenda-mode 1)
)

(use-package org-ql
  :ensure t
  :after (org)
)

;; assuming casual has been loaded...
(keymap-set org-agenda-mode-map "C-o" #'casual-agenda-tmenu)

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ORG-ROAM
;;
;; functions
(defun org-roam-rg-search ()  
  "Search org-roam directory using consult-ripgrep. With live-preview."  
  (interactive) 
  (require 'org-roam)
  (counsel-rg nil org-roam-directory nil nil))


;; org-roam setup 
;;

(setq org-roam-database-connector 'sqlite-builtin)

(setq org-roam-directory (downcase (file-truename (concat cloud-root-path "/org-roam/")))) 

(setq org-roam-db-gc-threshold most-positive-fixnum) ; reduce number of gc during org-roam db sync

;; org-roam-buffer customization
;;
(setq org-roam-mode-sections
	(list #'org-roam-backlinks-section
	      #'org-roam-reflinks-section
		  ;; #'org-roam-unlinked-references-section
		  ))

(use-package org-roam
    :ensure t
    :custom
    (org-roam-directory (downcase (file-truename (concat cloud-root-path "/org-roam/"))))
    (org-roam-database-connector 'sqlite-builtin)
    (org-roam-completion-everywhere t)

    :init
    (setq org-roam-db-location (downcase (concat user-emacs-directory "/org-roam/org-roam.db")))

    :bind 
     (("C-c n f" . org-roam-node-find)
      ("C-c n z" . org-roam-node-random)
      ("C-c n l" . org-roam-buffer-toggle)
      ("C-c n s" . org-roam-db-sync)
      ("C-c n c"   . org-roam-capture)
      ("C-c n j" . org-roam-dailies-capture-today)
      ("C-c n d ." . org-roam-dailies-goto-today)
      (:map org-mode-map
        (
          ("C-c n i"   . org-roam-node-insert)
          ("C-c n o"   . org-id-get-create)
          ("C-c n t"   . org-roam-tag-add)
          ("C-c n a"   . org-roam-alias-add)
          ("C-c n g"   . org-roam-graph)      ;; generate graph - need graphviz
          ("C-c n u"   . org-roam-ui-mode)   ;; start web view
          ("C-c n r"   . org-roam-ref-find)
          ("C-c n R"   . org-roam-ref-add)
          ("C-c n x"   . org-roam-rg-search)
          ;; ("C-M-i"     . completion-at-point)
          ;;    -- C-M-i taken by flyspell-autocorrect-word
          ("C-c n d Y" . org-roam-dailies-capture-yesterday)
          ("C-c n d T" . org-roam-dailies-capture-tomorrow)
          ("C-c n d D" . org-roam-dailies-capture-date)
          ("C-c n d y" . org-roam-dailies-goto-yesterday)
          ("C-c n d t" . org-roam-dailies-goto-tomorrow)
          ("C-c n d d" . org-roam-dailies-goto-date)
          ("C-c n d f" . org-roam-dailies-goto-next-note)
          ("C-c n d b" . org-roam-dailies-goto-previous-note)
        )
     ))
    :config
	(require 'org-roam-protocol)
    (require 'org-roam-dailies)
	
    ; https://github.com/org-roam/org-roam/wiki/User-contributed-Tricks
    (setq org-roam-node-display-template
      (concat "${title:60} " (propertize "${tags:40}" 'face 'org-tag))
       org-roam-node-annotation-function
       (lambda (node) (marginalia--time (org-roam-node-file-mtime node)))) 
		
	; bookmark:  might have to start org-roam first?
	;
	;javascript:location.href='org-protocol://roam-ref?template=r&ref=%27+encodeURIComponent(location.href)+%27&title=%27+encodeURIComponent(document.title)+%27&body=%27+encodeURIComponent(window.getSelection());
	; javascript:location.href='org-protocol://roam-ref?template=r&ref='+encodeURIComponent(location.href)+'&title='+encodeURIComponent(document.title)+'&body='+encodeURIComponent(window.getSelection());
		
		
		
    (setq org-roam-capture-ref-templates
        '(("r" "ref" plain
          "%?"
          :target
          (file+head "${slug}.org" "#+title: ${title}\n${body}")
          :unnarrowed t)))		
		
    (define-key org-roam-node-map [mouse-1] #'org-roam-node-visit)

    (org-roam-db-autosync-enable)     
)

;; Make the backlinks buffer easier to peruse by folding leaves by default.
;;
(add-hook 'org-roam-buffer-postrender-functions #'magit-section-show-level-2)
  

(use-package org-roam-ql
  :ensure t
  :after (org-roam)
  :bind ((:map org-roam-mode-map
          ("v" . org-roam-ql-buffer-dispatch) ;; have org-roam-ql's transient in org-roam mode buffers
;		  :map minibuffer-mode-map
;		  ("C-c n i" . org-roam-ql-insert-node-title) ;; add titles in queries while in minibuffer 
		 )
        )
)

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; CONSULT-ORG-ROAM
(use-package consult-org-roam
   :ensure t
   :delight
   :after org-roam
   :init
   (require 'consult-org-roam)
   ;; Activate the minor mode
   (consult-org-roam-mode 1)
   :custom
   ;; Use `ripgrep' for searching with `consult-org-roam-search'
   (consult-org-roam-grep-func #'consult-ripgrep)
   ;; Configure a custom narrow key for `consult-buffer'
   (consult-org-roam-buffer-narrow-key ?r)
   ;; Display org-roam buffers right after non-org-roam buffers
   ;; in consult-buffer (and not down at the bottom)
   (consult-org-roam-buffer-after-buffers t)
   :config
   ;; Eventually suppress previewing for certain functions
   (consult-customize
    consult-org-roam-forward-links
    :preview-key "M-.")
   :bind
   ;; Define some convenient keybindings as an addition
   ("C-c n e" . consult-org-roam-file-find)
   ("C-c n b" . consult-org-roam-backlinks)
   ("C-c n B" . consult-org-roam-backlinks-recursive)
   ("C-c n k" . consult-org-roam-forward-links)
   ("C-c n v" . consult-org-roam-search)
)

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
;; ORG-ROAM-UI - ORG-ROAM-UI-MODE / C-c n g
;;
;;

(use-package websocket
  :after org-roam)

(use-package org-roam-ui
  :after (org-roam websocket)
  :config
  (setq org-roam-ui-sync-theme t
        org-roam-ui-follow t
        org-roam-ui-update-on-save t
        org-roam-ui-open-on-start t))



;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; ORG SCREENSHOTS / IMAGES / DOWNLOAD
;;
;;
(message "Loading org-download")
(use-package org-download
  :ensure t
; :after org
  :config
  (setq org-download-method 'directory)
  (setq org-download-heading-level nil)
  (setq-default org-download-image-dir "./img")
  (setq org-download-timestamp "%Y%m%d-%H%M%S_")
  (setq org-image-actual-width 600)
  (setq org-download-screenshot-method "nircmd clipboard saveimage %s")
  (add-hook 'dired-mode-hook 'org-download-enable)
  :bind
  (:map org-mode-map
   (("C-M-y" . rgd-org-download-screenshot)
    ("s-Y" . org-download-screenshot)
	("s-y" . org-download-yank)))
  )

;; SCREENSHOT INTO ORG
;;
(defun rgd-org-download-screenshot (&optional basename)
   "Call org-download-screenshot after setting the org-download-image-dir based on current org buffer."
   (interactive)
   (progn
        (message "rgd-org-download-screenshot start")
        (setq-local org-download-image-dir (concat (file-name-directory (buffer-file-name)) "/img/" (file-name-base buffer-file-name) "/") )
		(org-download-screenshot basename)
        (message "rgd-org-download-screenshot end")
   )
)

(defun rgd-org-screenshot ()
  (interactive)
                                        ; create .pic directory if does not exist.
  (unless (file-exists-p "./img")
    (make-directory "./img"))
  (setq filename
        (concat
         (make-temp-name
          (concat (buffer-file-name)
                  "_"
                  (format-time-string "%Y%m%d_%H%M%S_")) ) ".png"))
  (setq filename (concat "./img/" (file-name-nondirectory filename)))
  (shell-command "powershell.exe snippingtool /clip")
  (shell-command (concat "powershell.exe -command \"Add-Type -AssemblyName System.Windows.Forms;if ([System.Windows.Forms.Clipboard]::ContainsImage()) {\$image = [System.Windows.Forms.Clipboard]::GetImage();[System.Drawing.Bitmap]\$image.Save('" filename "',[System.Drawing.Imaging.ImageFormat]::Png); Write-Output 'clipboard content saved as file'} else {Write-Output 'clipboard does not contain image data'}\""))
  (insert (concat "[[file:" filename "]]"))
  (org-display-inline-images))
;; from Jeff-avatar: https://gist.github.com/cdaven/135298087efb0ffc5dab93fd56249b38?permalink_comment_id=4242464 

(global-set-key [s-f7] 'rgd-org-screenshot)
(message "Done loading org-download/screenshot")



;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; PRESENTING
;;
;; org-present
;;
(setq org-present-el (expand-file-name (concat config-root-path "/.emacs.d/org-present-conf.el")))
;(with-timer "org-present"
; (load-file org-present-el))



;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; PLANTUML
;;
(message "config org-plantuml")
;
; org-plantuml / babel only supports 'jar or 'plantuml-executable  - doesn't support server/URL :( 

(setq org-plantuml-jar-path (expand-file-name "c:/sys/plantuml/plantuml-1.2024.4.jar")) 
(with-eval-after-load 'org
 (add-to-list 'org-src-lang-modes '("plantuml" . plantuml)))
(org-babel-do-load-languages 'org-babel-load-languages '((plantuml . t)))
(org-display-inline-images)

(setq org-plantuml-exec-mode 'jar)
(message "org-plantuml done")


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; org-transclusion
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-key global-map (kbd "C-c n A") #'org-transclusion-add)

(define-key global-map (kbd "C-c n T") #'org-transclusion-mode)
