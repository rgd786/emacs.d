;;
;; init.el is LOCAL to PC - must copy to each local emacs init
(message "<%s %s/.emacs.d/init.el - start" system-name (getenv "HOME") )

;; ***************************
;; NATIVE COMPILATION SETTINGS

(setq native-comp-async-report-warnings-errors 'silent)
(setq native-comp-verbose 0)   ;; 0-3
(setq native-comp-debug 0)     ;; 0-3


;; ********************************************
;; STARTUP TIMING - GARBAGE COLLECTION SETTINGS

(defun efs/display-startup-time ()
  (message "Emacs loaded in %s with %d garbage collections."
           (format "%.2f seconds"
                   (float-time
                   (time-subtract after-init-time before-init-time)))
           gcs-done))

(add-hook 'emacs-startup-hook #'efs/display-startup-time)

;; GARBAGE COLLECTION SETTINGS 
;; try gcmh - garbage collection magic hack - 
;
;; increase GC limit temporarily during initialization to speed it up 
;; (https://github.com/nilcons/emacs-use-package-fast )
(setq garbage-collection-messages t)
;(defvar gc-cons-threshold--orig gc-cons-threshold)
;(setq gc-cons-threshold (* 32 1024 1024)) 
;(setq gc-cons-percentage 0.8)
;;(run-with-idle-timer 10 t #'garbage-collect)  ; run gc after 10s of idle time

(setq gc-cons-threshold most-positive-fixnum ; 2^61 bytes
      gc-cons-percentage 0.6)
(defvar config:file-name-handler-alist-cache file-name-handler-alist)
(setq file-name-handler-alist nil)
(defun config:restore-post-init-settings ()
  (setq gc-cons-threshold 16777216 ; 16mb
        gc-cons-percentage 0.1)
  (setq file-name-handler-alist config:file-name-handler-alist-cache))
(add-hook 'emacs-startup-hook #'config:restore-post-init-settings)
;
(defun config:defer-gc ()
  (setq gc-cons-threshold most-positive-fixnum))
(defun config:-do-restore-gc ()
  (setq gc-cons-threshold 16777216))
(defun config:restore-gc ()
  (run-at-time 1 nil #'config:-do-restore-gc))
(add-hook 'minibuffer-setup #'config:defer-gc)
(add-hook 'minibuffer-exit #'config:restore-gc)
;; --https://www.reddit.com/r/emacs/comments/yzb77m/an_easy_trick_i_found_to_improve_emacs_startup/


(setq w32-pipe-buffer-size (* 64 2048)) ;; error creating pipe in org-roam db sync 04/01/24

(setq ulimit 10240)       ;; same issue - org-roam db syncing, with git-gutter gave the too many files, trying this.

  ;; suggestion was from :
  ;; https://news-hada-io.translate.goog/topic?id=5888&_x_tr_sl=ko&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=sc

(let ((file-name-handler-alist nil))                              ;; to speed up, avoid tramp etc
 
(setq inhibit-compacting-font-caches t)       ;; bug#24918 - slow scrolling

;; timestamps in *Messages*                   ;; via http://www.reddit.com/r/emacs/comments/1auqgm/speeding_up_your_emacs_startup/
(defun current-time-microseconds ()
  (let* ((nowtime (current-time))
         (now-ms (nth 2 nowtime)))
    (format-time-string "[%Y-%m-%d %T] " nowtime) ) )

(defadvice message (before when-was-that activate)
    "Add timestamps to `message' output."
    (ad-set-arg 0 (concat (current-time-microseconds) 
                          (ad-get-arg 0)) ))

;; *****************************************************************
;; MACHINE SPECIFIC SETUP -or set this as a 'class' of installation: work, home; windows, gnu and pick paths down in emacs.conf.el

;; one drive - for work ?
;; google drive - ?
;; dropbox-root-path  - c:/rgd/dropbox           - used to have org files et al, blocked at work so can't use for org files
;; home-root-path     - c:/users/<acct>          - should contain the .emacs.d subdir 
;; cloud-root-path    - c:/users/<acct>/gitlab   - where gitlab subdir is; for org files
;; config-root-path   - c:/users/<acct>/.emacs.d - where .emacs.d is

;;             git repo:     home                           work
;; .emacs.d  -  gitlab       c:/users/rgd/.emacs.d          c:/users/usrodav2/.emacs.d     
;; org files -  gitlab       c:/users/rgd/gitlab/org        c:/users/usrodav2/gitlab/org
;; etc files -  gitlab       c:/users/rgd/gitlab/etc        c:/users/usrodav2/gitlab/etc 

(cond
 ((string-match "LEGION17"     (system-name))
  (defvar home-root-path       (downcase (getenv "HOME")))
  (defvar dropbox-root-path    "c:/rgd/dropbox")
  (defvar cloud-root-path      (downcase (concat home-root-path "/gitlab")))
  (defvar org-root-path        cloud-root-path)            ;; everywhere adds /org - should do it here...
  (setq org-roam-directory     "c:/users/rgd/gitlab/org-roam/")
  (defvar config-root-path     home-root-path)
  (setq custom-file            (concat home-root-path "/.emacs.d/emacs.custom-c.el"))
  (setq package-quickstart t)
  )
 ((string-match "US-L-7443770" (system-name))
  (defvar home-root-path       "c:/users/USRODAV2") ; (downcase (getenv "HOME")))
  (defvar dropbox-root-path    "c:/rgd/dropbox")        ;; dropbox blocked at work 2-2024
  (defvar cloud-root-path      (downcase (concat home-root-path "/gitlab")))
  (defvar org-root-path        cloud-root-path)     ;; everywhere adds "/org" - but should do it here...
  (setq org-roam-directory     "c:/users/USRODAV2/gitlab/org-roam/")
  (message "init.el 66 - org-roam-directory=[%s]" org-roam-directory)  
  (setq custom-file            (concat home-root-path "/.emacs.d/emacs.custom-c.el"))
  (defvar config-root-path     home-root-path)
  ;;  (defvar cloud-emacs-path     (concat home-root-path ".emacs.d"))
  (setq package-quickstart t)
  )
   ((string-match "US-L-PF56W12A" (system-name))
  (defvar home-root-path       "c:/users/USRODAV2") ; (downcase (getenv "HOME")))
  (defvar dropbox-root-path    "c:/rgd/dropbox")        ;; dropbox blocked at work 2-2024
  (defvar cloud-root-path      (downcase "c:/rgd/gitlab"))
  (defvar org-root-path        cloud-root-path)     ;; everywhere adds "/org" - but should do it here...
  (setq org-roam-directory     "c:/rgd/gitlab/org-roam/")
  (message "init.el 66 - org-roam-directory=[%s]" org-roam-directory)  
  (setq custom-file            (concat home-root-path "/.emacs.d/emacs.custom-c.el"))
  (defvar config-root-path     home-root-path)
  ;;  (defvar cloud-emacs-path     (concat home-root-path ".emacs.d"))
  (setq package-quickstart t)
  )
  
;;  LAB MACHINES - where Dropbox and OneDrive no longer work
 ((string-match "HAPIDEV10" (system-name))
  	(defvar config-root-path (getenv "HOME"))
                                     
	(defvar cloud-root-path "c:/rgd/gitlab")                ;;	(defvar dropbox-root-path "c:/rgd")  - blocked
	(defvar cloud-emacs-path (concat cloud-root-path ".emacs.d"))
	
    (defvar org-root-path cloud-root-path)
    (setq custom-file (concat config-root-path "/.emacs.d/emacs.custom-c.el"))
	(setq package-quickstart t)
  )
 ((string-match "HAPIBUILD10" (system-name))          
  	(defvar config-root-path (downcase (getenv "HOME")))
	(defvar cloud-root-path "c:/rgd")                    ;;; blocked: (defvar dropbox-root-path "c:/rgd") 
    (defvar org-root-path cloud-root-path)
    (setq custom-file (concat config-root-path "/.emacs.d/emacs.custom-c.el"))
	(setq package-quickstart t)
  )
  ((string-match "devrob03" (system-name))          
  	(defvar config-root-path (downcase (getenv "HOME")))
	(defvar cloud-root-path "c:/rgd")                    ;;; blocked: (defvar dropbox-root-path "c:/rgd") 
    (defvar org-root-path "c:/rgd/")
    (setq custom-file (concat config-root-path "/.emacs.d/emacs.custom-c.el"))
	(setq package-quickstart t)
  )
  ((string-match "w10devrob24" (system-name))          
  	(defvar config-root-path (downcase (getenv "HOME")))
	(defvar cloud-root-path "c:/rgd")                    ;;; blocked: (defvar dropbox-root-path "c:/rgd") 
    (defvar org-root-path "c:/rgd/")
    (setq custom-file (concat config-root-path "/.emacs.d/emacs.custom-c.el"))
	(setq package-quickstart t)
  )
  ((string-match "CEBUILDVB6" (system-name))          
  	(defvar config-root-path (getenv "HOME"))
	(defvar cloud-root-path "c:/rgd")                    ;;; blocked: (defvar dropbox-root-path "c:/rgd") 
    (defvar org-root-path "c:/rgd/")
    (setq custom-file (concat config-root-path "/.emacs.d/emacs.custom-c.el"))
	(setq package-quickstart t)
  )
  ((string-match "w10devrob2" (system-name))          
  	(defvar config-root-path (downcase (getenv "HOME")))
	(defvar cloud-root-path "c:/rgd")                    ;;; blocked: (defvar dropbox-root-path "c:/rgd") 
    (defvar org-root-path cloud-root-path)
    (setq custom-file (concat config-root-path "/.emacs.d/emacs.custom-c.el"))
	(setq package-quickstart t)
  )
  ((string-match "w10devrob" (system-name))          
  	(defvar config-root-path (downcase (getenv "HOME")))
	(defvar cloud-root-path "c:/rgd")                    ;;; blocked: (defvar dropbox-root-path "c:/rgd") 
    (defvar org-root-path cloud-root-path)
    (setq org-roam-directory "c:/users/Administrator/gitlab/org-roam/")
    (setq custom-file (concat config-root-path "/.emacs.d/emacs.custom-c.el"))
	(setq package-quickstart t)
  )
)
(message "System name=[%s]" system-name)
(message "config-root-path=[%s]" config-root-path)
(when (boundp 'org-roam-directory)
 (message "init.el 130 - org-roam-directory=[%s]" org-roam-directory))

;; *****************************************************************
;; home/personal directory setup [kmodi]
(defvar user-home-directory (downcase (concat (getenv "HOME") "/"))) ; must end with /
(setq user-emacs-directory (downcase (concat user-home-directory ".emacs.d/"))) ; must end with /

(defvar user-personal-directory (let ((dir (concat user-emacs-directory "personal/"))) ; must end with /
                                  (make-directory dir :parents) dir)
   "User's personal directory to contain non-git-controlled files.")  ;; <-- this is important ** and why I might want to use this

(add-to-list 'load-path user-personal-directory)

(setq org-directory (downcase (expand-file-name (concat org-root-path "/org"))))
(setq org-conf-el (expand-file-name (concat config-root-path "/.emacs.d/org.conf.el")))  ;; TODO: change to gitlab location reference

;; COMMAND LINE PARAMETER HANDLING **********************************
;;
(setq org-switch-found (member "-org" command-line-args))
(setq command-line-args (delete "-org" command-line-args))
(setq quick-switch-found (member "-quick" command-line-args)) 
(setq command-line-args (delete "-quick" command-line-args)) 
(setq quick-switch-found (member "-wiki" command-line-args))
(setq command-line-args (delete "-wiki" command-line-args))
(setq news-switch-found (member "-news" command-line-args))
(setq command-line-args (delete "-news" command-line-args))
(setq minpkg-switch-found (member "-minpkg" command-line-args))
(setq command-line-args (delete "-minpkg" command-line-args))

(setq golden-ratio-switch-found (member "-gr" command-line-args))
(setq command-line-args (delete "-minpkg" command-line-args))


;; *****************************************************************
;; LOAD CUSTOM FILE FOR MACHINE
(defvar emacs-conf-path (concat config-root-path "/.emacs.d/emacs.conf.el"))

;; setting machine-specific since some have org agenda files in dropbox on D, some on C
(message "Loading %s custom file" custom-file)
(when (file-exists-p custom-file)
  (load custom-file))
(message "Done loading %s custom file" custom-file)

;; *****************************************************************
;; GLOBAL CUSTOMIZATION - <config-root>/.emacs.d/emacs.conf.el
;;

;; load global customization file emacs.conf.el from config root
(if (file-exists-p emacs-conf-path)
    (progn
     (message "loading Emacs configuration file %s" emacs-conf-path)
       (load-file emacs-conf-path)
    ))

(message "<%s home>/.emacs.d/init.el - done loading %s" system-name emacs-conf-path)

(ad-disable-advice 'message 'before 'when-was-that)
(ad-update 'message)

) ;; end (let file-name-handler-alist nil...

(message "<%s home>/.emacs.d/init.el - done" system-name)

(message "emacs init time = %s" (emacs-init-time))

;;; (toggle-debug-on-error)

(if news-switch-found
	(progn
      (require 'newsticker)
	  (newsticker-start)
	  (newsticker-treeview)
  )
)
