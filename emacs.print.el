
;; SAVING PRINTING AT WORK CONFIGURATION
  ;; PRINTING AT WORK - various options
  ;;
;;  (require 'printing)
;;  (pr-update-menus t) ;; the menus seem more for ps printing, not lpr printing - couldn't find item that would do (print-buffer)
  ;; doing M-x print-buffer workes with option (3) set for PrFile32.
  ;;
  ;; TODO: how to configure the printing settings for prfile32?
  ;;       by default it's finding the default printer (currently PDF-XChange6 and blapping 'permission denied')
  ;;       maybe set default printer back to the "US-P-CLEPX1032-" - that will probably work nicely but then have no control - it'll print right then.
  ;;       or figure out how to fix option 5
  ;;
  ;;
  ;; TODO - CONFIGURE PRINTING OPTIONS - printer alist CLEPX1032-, PDFPrinter, PDFEdit, PrFile32, etc... some for PS, other LPR

  ;; BE CAREFUL of having pr.exe and lpr.exe clones on your machine, if Emacs finds them it uses them and changes
  ;; its printing behavior (possibly preventing Windows-specific behavior)
  
;;  (setq lpr-headers-switches nil) ;; <-- !! or else get "page headers are not supported"

  ;; ;; (1) print to network printer
  ;;  (setq printer-name "//uswic-s-file001/USCLEPX005-M_PS")
  ;;(setq printer-name "US-P-CLEPX1032-") ;; X_10.127.230.32
  ;; no longer possible after Wickliffe, not existing in Highland Hills
  ;;

  ;; (1c) attempting during work from home pandemic
;;   (setq printer-name "//HPENVY")   ;; ////192.168.254.67//HPENVY
;; nope
  
  ;; ;; (2) print using network printer connected to local LPT3: port
  ;;  (setq printer-name "LPT3:")
  ;; net use lpt3: \\127.0.0.1\US-P-CLEPX1032-
  ;; shared printer on work laptop (but not PS)
  ;;

  ;; (2c) print using LPT3 style during pandemic
  ;;  (setq printer-name "LPT3:")
  ;; nope - keeps trying to write-region to c:/LPT3:
  ;; set HP Envy's printer port on this laptop to LPT3 and see
  
  ;; ;; (3) using lerup's PrintFile32
;;  (setq lpr-command "C:/Program Files (x86)/PrintFile/prfile32.exe") 
;;  (setq lpr-switches '("/q")) 
  ;; (setq ps-lpr-command "C:/Program Files (x86)/PrintFile/prfile32.exe") 
  ;; (setq ps-lpr-switches '("/q")) 
  ;;

  ;; (3c) PrFile during pandemic
;;  (setq lpr-command "C:/Program Files (x86)/PrintFile/prfile32.exe")
;;  (setq lpr-switches '("/q"))
;;  (setq lpr-headers-switches nil) ;; <-- !! or else get "page headers are not supported"
;;  (setq lpr-page-header-program "C:\\Program Files\\Git\\usr\\bin\\pr.exe")
;;  (setq ps-lpr-command "C:/Program Files (x86)/PrintFile/prfile32.exe") 
;;  (setq ps-lpr-switches '("/q")) 

  
  ;;  (setq printer-name "\\\\US-L-7002395\\PDFPrinter") ;; see a document in that printer queue flicker past but no output.
  
  ;; ;; (4) using a local shared printer (PDFPrinter) the share for PDF-XChange... printer (BCE load)
  ;;(setq printer-name "\\\\us-l-7002395\\PDF-XChange Standard V6") - nope
  ;;(setq ps-printer-name "\\\\us-l-7002395\\PDF-XChange Standard V6") - loser 
  ;;(setq printer-name "\\\\PDF-XChange6") - bad
  ;;(setq ps-printer-name "\\\\PDF-XChange6") - didn't work either
  
  ;; (5) using PDF XChange's "PDF Editor"
;; not working during pandemic from home...
  ;;  (setq printer-name nil)
;;  (setq lpr-headers-switches nil) ;; <-- !! or else get "page headers are not supported"
;;  (setq lpr-command "c:\\program files\\tracker software\\PDF Editor\\PDFXEdit.exe")

  ;; ^--- !! "works" - converts text to PDF but proportional font so it's messed up. :( 
  
    ;;(setq lpr-switches '("/A")) ;; needed?
  ;;(setq ps-lpr-command "c:\\program files\\tracker software\\PDF Editor\\PDFXEdit.exe") ;; but this did not, showed the PS source...
  ;;(setq ps-lpr-switches '("/A")) ;; needed?

;; CHECK FOR lpr-headers-switches - that's where (page headers are not supported) is coming from - set to nil?!
  ;; if that works could use prfile32 or PDFEdit - at least for non-PS printing
  ;; PS printing... eh, probably need the network/share printing to work to connect to real PS printer since emacs is sending PS code
  ;; printing, per Emacs manual, G.9 Printing and MS-Windows
;;  (setq lpr-command "")
;;  (setq printer-name "LPT1") 
  ;; nope - keeps going into debugger complaining about "C:/LPT1"; even after nuking all pr.exe/lpr.exe clones on here (gnuwin32,
  ;; usr/local/wbin, Git,...)

  ;; (6) printing at home from work laptop during pandemic
  ;; (setq ps-printer-name "\\\\192.168.254.67")
  ;; (setq lpr-command nil)
  ;; (setq lpr-headers-switches nil)
  ;; (setq printer-name "\\\\192.168.254.67")

