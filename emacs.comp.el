;; emacs.comp.el - completion packages and configuration - ivy/swiper/counsel/hydra, vertico/consult/marginalia/embark/orderless
;;
;;

;; disable smex (in preference for amx below)
;; (with-timer "smex"
;; (use-package smex
;;   :disabled t
;;   :init (setq-default smex-save-file (no-littering-expand-var-file-name ".smex-items"))
;;   :bind (("<remap> <execute-extended-command>" . smex)))
;; )
;(unless minpkg-switch-found

	(with-timer "amx"
	(use-package amx
	  :ensure t
	  :init (setq-default amx-save-file (concat config-root-path "/.emacs.d/.amx-items"))
	  :bind (("<remap> <execute-extended-command>" . amx)))
	)
;)


;; neat -- use M-o while in counsel-M-x to get menu to get help, goto definition etc on current highlighted command !
;(unless minpkg-switch-found

	(with-timer "counsel"
	 (use-package counsel 
		:ensure t
		:bind
		;; counsel-bookmark
		;; counsel-faces
		;; counsel-cd
		;; counsel-git
		;; counsel-org-tag
		;; counsel-dired-jump
		;; counsel-ag
		;; counsel-grep
		;; counsel-apropos
		
		(("M-x" . counsel-M-x) ;; maybe don't use this - trying to not have counsel used with dired C-x d
		 ("C-s" . swiper)
		 ("C-x C-f" . counsel-find-file)
		 ("C-x r f" . counsel-recentf)
		 ("C-x l" . counsel-locate)
	;    ("C-c g s" . counsel-grep-or-swiper)
		 ("M-y" . counsel-yank-pop) ;; was "C-M-y" but M-y was just yank-pop which isn't as useful it looks like
		 ("C-c C-r" . ivy-resume)
	;    ("C-c i m" . counsel-imenu)
	;    ("C-c i M" . ivy-imenu-anywhere)
		 ("C-h f" . counsel-describe-function)
		 ("C-h v" . counsel-describe-variable)
		 ("C-h s" . counsel-info-lookup-symbol)
		 ("<f1> f" . describe-function)
		 ("<f1> v" . describe-variable)
		 ("<f1> s" . info-lookup-symbol)
		 ("C-c d d" . counsel-descbinds)                        ;; <---- Nice!
		 ("C-c d s" . describe-symbol)
		 ("C-c d u" . counsel-unicode-char)
		 ("C-c g g" . counsel-git)
	;    ("C-c s s" . counsel-ag)
	;    ("C-c s d" . counsel-ag-projectile)
		 ("C-c g G" . counsel-git-grep)
		 :map ivy-minibuffer-map
		   ("M-y" . ivy-next-line-and-call))
	  :config
		(defun reloading (cmd)
		  (lambda (x)
		   (funcall cmd x)
		   (ivy--reset-state ivy-last)))
		(defun given-file (cmd prompt) ; needs lexical-binding
		  (lambda (source)
		   (let ((target
			(let ((enable-recursive-minibuffers t))
			  (read-file-name
			(format "%s %s to:" prompt source)))))
			(funcall cmd source target 1))))
		(defun confirm-delete-file (x)
		  (dired-delete-file x 'confirm-each-subdirectory))
		(ivy-add-actions
		  'counsel-find-file
		   `(("c" ,(given-file #'copy-file "Copy") "copy")
			 ("d" ,(reloading #'confirm-delete-file) "delete")
			 ("m" ,(reloading (given-file #'rename-file "Move")) "move")))

		(setq counsel-find-file-at-point t)
	  
		:defer 2
	 )
	)
;)

;(unless minpkg-switch-found
	(with-timer "ivy"

	  (use-package ivy
		:ensure t
		:delight
		:defer 1
											;	:after (hydry swiper ivy-posframe ivy-rich)
		:init
		(use-package swiper)
		(use-package ivy-posframe)
		:config
		(setq ivy-re-builders-alist '((swiper . ivy--regex-plus)
									  (t      . ivy--regex-plus)
									  ))

		;; (setq ivy-re-builders-alist '((t . ivy--regex-fuzzy))) not liking this as the default
		;; so added the above so swiper uses just regular matching
		;; note: hydra (C-o) offers 'm' to toggle matching
		;; use fuzzy searching (spaces not needed)

		(setq ivy-initial-inputs-alist nil)     ;; not needed if using ivy--regex-fuzzy
		(ivy-mode 1)
		(setq ivy-height 13)                    ;; number of result lines to display (was 14)
		(setq ivy-posframe-height 13)           ;; (was 15)
												;; (setq ivy-posframe-width 80) - don't know why I added that.  Too narrow at work usually
		(setq ivy-count-format "(%d/%d) ")
		(setq ivy-use-selectable-prompt t)
		(defun ivy-posframe-display-at-window-top-left-corner (str)
		  (ivy-posframe--display str #'posframe-poshandler-window-top-left-corner))

		(setq ivy-posframe-style 'window-bottom-center)
		(setq ivy-posframe-height-alist '((swiper . 13)
										  (t      . 13)))

		(setq ivy-posframe-display-functions-alist
			  '((swiper          . ivy-display-function-fallback) ;; in a minibuffer
				(complete-symbol . ivy-posframe-display-at-point)
				(counsel-M-x     . ivy-posframe-display-at-frame-bottom-window-center)
				(t               . ivy-posframe-display-at-frame-bottom-window-center)))

		(setq ivy-posframe-parameters
		  '((left-fringe . 16)
			(right-fringe . 16)))

		(define-key ivy-minibuffer-map (kbd "S-SPC") (lambda () (interactive) (insert " ")) )
		
		(ivy-posframe-mode 1)
		)
	  )
;)

;; avy
(unless minpkg-switch-found

	(with-timer "avy"
	  (use-package avy
		:ensure t
		:config
		(setq avy-all-windows 'all-frames)
		(setq avy-timeout-seconds 1.0)
		:bind
		(
		 ("C-:" . avy-goto-char-timer) ;; ivy-avy C-' use inside ivy C-s search to narrow them down quickly
		 ;; C-; would be nicer but that's flyspell autocorrect in text mode buffers
		)
	  )
	)
)
