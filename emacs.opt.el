;; options
;;

(setq ring-bell-function (lambda ()
						   (progn
  							 (message "DING! Command [%s] [%s]" this-command last-command)
                             (if (memq this-command '(nil)) ;; for appointments etc
								 (play-sound-file default-bell-sound-file)
							   (play-sound-file default-bell-sound-file)
                           ))))


(setq disabled-command-function nil) ;; enable all disabled commands like narrow-to-region, et al.

(setq auto-window-vscroll nil)

;; truncate long lines in grep buffers - command line gets so long, takes up most of window otherwise
(add-hook 'grep-mode-hook 'my-grep-mode-hook)
(defun my-grep-mode-hook ()
  (setq truncate-lines t))

;; Quitting emacs via `C-x C-c` or the GUI 'X' button ; from kmodi
(setq confirm-kill-emacs #'y-or-n-p)

;; don't require full 'yes' or 'no' - just accept 'y' or 'n'
(defalias 'yes-or-no-p 'y-or-n-p)

;; don't let indent use tabs, force to be spaces, like it should be
(setq-default indent-tabs-mode nil)

;; coding systems
(prefer-coding-system 'utf-8)
(modify-coding-system-alist 'file "\\.wer\\'" 'utf-16le) ;; for windows error reporting files
  (setq utf-translate-cjk-mode nil) ; disable CJK coding/encoding (Chinese/Japanese/Korean characters)
  (set-language-environment 'utf-8)
  (set-keyboard-coding-system 'utf-8-mac) ; For old Carbon emacs on OS X only
  (setq locale-coding-system 'utf-8)
  (set-default-coding-systems 'utf-8)
  (set-terminal-coding-system 'utf-8)
  (unless (eq system-type 'windows-nt)
   (set-selection-coding-system 'utf-8))
  (prefer-coding-system 'utf-8)
  

(cd (getenv "HOME"))

;; Configure Backups
(setq backup-directory-alist '(("." . "~/.emacs.d/.backup")))
(setq backup-by-copying t)
(setq delete-old-versions t
      kept-new-versions 6
      kept-old-versions 2
      version-control t)

;; Configure autosave location
(setq auto-save-file-name-transforms
  `((".*" "~/.emacs.d/.autosaves/" t)))

;; Configure interlock file location
(setq lock-file-name-transforms
  `((".*" "~/.emacs.d/.interlock/" t)))

;; abbrev settings
(setq abbrev-file-name (concat config-root-path "/.emacs.d/abbrev_defs"))
(setq save-abbrevs t)        ;; save abbrevs when files saves and quit emacs
(setq-default abbrev-mode t) ;; turn on abbrev-mode globally
(setq save-abbrevs 'silently)

(defadvice expand-abbrev (after my-expand-abbrev activate)
  ;; https://stackoverflow.com/a/15389612 - control cursor position in abbrev expansion
  ;; use '@@' in abbrev for cursor location (or look at skeletons/yasnippet)
  (if ad-return-value
      (run-with-idle-timer 0 nil
                           (lambda ()
                             (let ((cursor "@@"))
                               (if (search-backward cursor last-abbrev-location t)
                                   (delete-char (length cursor))))))))


;; diary
;(appt-activate 1)
;(add-hook 'diary-hook 'appt-make-list)
;(diary 0)

;; Mode line options
;; from https://emacs.stackexchange.com/questions/31208/mode-line-time-not-updating :
(setq display-time-default-load-average nil) ;; remove the load value
(setq display-time-mail-string "") ;; remove mail indicator
   (setq display-time-use-mail-icon nil)


;; modes (built-in modes)
;;       maybe move to emacs.modes.el?

(with-eval-after-load 'cc-mode
  (defun c-indent-then-complete ()
    (interactive)
    (if (= 0 (c-indent-line-or-region))
	(completion-at-point)))
  (dolist (map (list c-mode-map c++-mode-map))
    (define-key map (kbd "<tab>") #'c-indent-then-complete)))
