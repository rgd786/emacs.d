;; calendar settings
(setq mark-holidays-in-calendar t)

;; Italian localization
;; (setq calendar-week-start-day 1
;;       calendar-day-name-array ["Domenica" "Lunedì" "Martedì" "Mercoledì" 
;;                                "Giovedì" "Venerdì" "Sabato"]
;;       calendar-month-name-array ["Gennaio" "Febbraio" "Marzo" "Aprile" "Maggio"
;;                                  "Giugno" "Luglio" "Agosto" "Settembre" 
;;                                  "Ottobre" "Novembre" "Dicembre"])

;; UK Holidays -- see http://www.gnomon.org.uk/diary.html 
(setq holiday-uk-holidays
      '((holiday-fixed 1 1 "UK New Year's Day")
;;	(holiday-new-year-bank-holiday)
	(holiday-fixed 2 14 "UK Valentine's Day")
	(holiday-fixed 3 17 "UK St. Patrick's Day")
	(holiday-fixed 4 1 "UK April Fools' Day")
	(holiday-easter-etc -47 "UK Shrove Tuesday")
	(holiday-easter-etc -21 "UK Mother's Day")
	(holiday-easter-etc -2 "UK Good Friday")
	(holiday-easter-etc 0 "UK Easter Sunday")
	(holiday-easter-etc 1 "UK Easter Monday")
	(holiday-float 5 1 1 "UK Early May Bank Holiday")
	(holiday-float 5 1 -1 "UK Spring Bank Holiday")
	(holiday-float 6 0 3 "UK Father's Day")
	(holiday-float 8 1 -1 "UK Summer Bank Holiday")
	(holiday-fixed 10 31 "UK Halloween")
	(holiday-fixed 12 24 "UK Christmas Eve")
	(holiday-fixed 12 25 "UK Christmas Day")
	(holiday-fixed 12 26 "UK Boxing Day")
;;	(holiday-christmas-bank-holidays)
	(holiday-fixed 12 31 "UK New Year's Eve")))
(setq holiday-other-holidays (append holiday-other-holidays holiday-uk-holidays))

;; Scottish holidays
(setq  holiday-scottish-holidays 
       '((holiday-float 1 2 1 "Day after New Year's Day (Scotland)")
		 (holiday-fixed 1 25 "Robert Burns Night (Scotland)")
		 (holiday-fixed 2 13 "Pancake Tuesday / Shrove Tuesday (Scotland)")
		 (holiday-fixed 3 11 "Mothering Day (Scotland)")
		 (holiday-float 6 1 1 "June Bank Holiday (Scotland)")
		 (holiday-float 11 0 2 "Remembrance Sunday (Scotland)") ;; sunday closest to Nov-11
		 (holiday-fixed 11 30 "St. Andrew's Day (Scotland)")
        ))
(setq holiday-other-holidays (append holiday-other-holidays holiday-scottish-holidays))

;; Irish Holidays
(setq  holiday-irish-holidays 
       '((holiday-float 5 1 1 "May Day (Ireland)")
         (holiday-float 6 1 1 "June Bank Holiday (Ireland)")
         (holiday-float 8 1 1 "August Bank Holiday (Ireland)")
         (holiday-float 10 1 -1 "October Bank Holiday (Ireland)")
         (holiday-fixed 12 26 "St. Stephen's Day (Ireland)")))
(setq holiday-other-holidays (append holiday-other-holidays holiday-irish-holidays))

(setq holiday-italian-holidays
      '((holiday-fixed 1 1 "Capodanno (Italy)")
        (holiday-fixed 5 1 "1 Maggio (Italy)")
        (holiday-fixed 4 25 "Liberazione (Italy)")
        (holiday-fixed 6 2 "Festa Repubblica (Italy)")
		))
(setq holiday-other-holidays (append holiday-other-holidays holiday-italian-holidays))

(setq holiday-italian-christian-holidays
      '((holiday-fixed 12 8 "Immacolata Concezione (Italy)")
		(holiday-fixed 12 25 "Natale (Italy)")
		(holiday-fixed 12 26 "Santo Stefano (Italy)")
		(holiday-fixed 1 6 "Epifania (Italy)")
		(holiday-easter-etc -52 "Giovedì grasso (Italy)")
		(holiday-easter-etc -47 "Martedì grasso (Italy)")
		(holiday-easter-etc  -2 "Venerdì Santo (Italy)")
		(holiday-easter-etc   0 "Pasqua (Italy)")
		(holiday-easter-etc  +1 "Lunedì Pasqua (Italy)")
		(holiday-fixed 8 15 "Assunzione di Maria (Italy)")
		(holiday-fixed 11 1 "Ognissanti (Italy)")
		))
(setq holiday-other-holidays (append holiday-other-holidays holiday-italian-christian-holidays))

(setq holiday-indian-holidays
  '((holiday-fixed 1 1   "English New Year (India)")
    (holiday-fixed 1 14  "Lohri (India)")
    (holiday-fixed 1 15  "Pongal, Mahar Sankranthi (India)")
    (holiday-fixed 1 16  "Guru Gobind Singh Jayanti (India)")
    (holiday-fixed 1 20  "Tailang Swami Jayanti (India)")
    (holiday-fixed 1 23  "Subhas Chandra Bose Jayanti (India)")
    (holiday-fixed 1 26  "Republic Day (India)")
    (holiday-fixed 1 31  "Sunday Swami Vivekananda Jayanti (India)")
    (holiday-fixed 2 12  "Vasant Panchami (India)")
    (holiday-fixed 2 22  "Guru Ravidas Jayanti (India)")
    (holiday-fixed 3 4   "Maharishi Dayanand Saraswati Jayanti (India)")
    (holiday-fixed 3 7   "Maha Shivaratri (India)")
    (holiday-fixed 3 10  "Ramakrishna Jayanti (India)")
    (holiday-fixed 3 20  "Parsi New Year (India)")
    (holiday-fixed 3 23  "Chhoti Holi, Holika Dahan, Chaitanya Mahaprabhu Jayanti (India)")
    (holiday-fixed 3 24  "Holi (India)")
    (holiday-fixed 3 25  "Good Friday (India)")
    (holiday-fixed 3 26  "Shivaji Jayanti (India)")
    (holiday-fixed 3 27  "Easter (India)")
    (holiday-fixed 4 1   "Bank's Holiday (India)")
    (holiday-fixed 4 8   "Gudi Padwa, Ugadi (India)")
    (holiday-fixed 4 9   "Jhulelal Jayanti (India)")
    (holiday-fixed 4 13  "Solar New Year, Baisakhi (India)")
    (holiday-fixed 4 14	 "Ambedkar Jayanti (India)")
    (holiday-fixed 4 15  "Rama Navami (India)")
    (holiday-fixed 4 19	 "Mahavir Swami Jayanti (India)")
    (holiday-fixed 4 21	 "Hazarat Ali's Birthday (India)")
    (holiday-fixed 5 3   "Vallabhacharya Jayanti (India)")
    (holiday-fixed 5 7   "Rabindranath Tagore Jayanti (India)")
    (holiday-fixed 5 11  "Shankaracharya Jayanti, Surdas Jayanti (India)")
    (holiday-fixed 5 21  "Buddha Purnima (India)")
    (holiday-fixed 6 7   "Maharana Pratap Jayanti (India)")
    (holiday-fixed 6 20  "Kabirdas Jayanti (India)")
    (holiday-fixed 6 21  "Longest Day of Year (India)")
    (holiday-fixed 7 1   "Jamat Ul-Vida (India)")
    (holiday-fixed 7 6   "Jagannath Rathyatra (India)")
    (holiday-fixed 7 7   "Eid al-Fitr, Ramadan (India)")
    (holiday-fixed 8 10  "Tulsidas Jayanti (India)")
    (holiday-fixed 8 15  "Independence Day (India)")
    (holiday-fixed 8 18  "Rakhi, Raksha Bandhan (India)")
    (holiday-fixed 8 25  "Krishna Janmashtami (India)")
    (holiday-fixed 9 5   "Ganesh Chaturthi (India)")
    (holiday-fixed 9 13  "Onam, Eid al-Adha, Bakrid (India)")
    (holiday-fixed 9 22  "Autumnal Equinox (India)")
    (holiday-fixed 10 1  "Maharaja Agresen Jayanti (India)")
    (holiday-fixed 10 2  "Gandhi Jayanti (India)")
    (holiday-fixed 10 3  "Al-Hijra, Islamic New Year (India)")
    (holiday-fixed 10 9  "Durga Ashtami (India)")
    (holiday-fixed 10 10 "Maha Navami (India)")
    (holiday-fixed 10 11 "Dussehra, Madhvacharya Jayanti (India)")
    (holiday-fixed 10 12 "Day of Ashura, Muharram (India)")
    (holiday-fixed 10 16 "Valmiki Jayanti, Meerabai Jayanti (India)")
    (holiday-fixed 10 19 "Karwa Chauth (India)")
    (holiday-fixed 10 29 "Narak Chaturdashi (India)")
    (holiday-fixed 10 30 "Diwali, Lakshmi Puja (India)")
    (holiday-fixed 10 31 "Gowardhan Puja (India)")
    (holiday-fixed 11 1  "Bhaiya Dooj (India)")
    (holiday-fixed 11 6  "Chhath Puja (India)")
    (holiday-fixed 11 14 "Guru Nanak Jayanti, Nehru Jayanti (India)")
    (holiday-fixed 12 12 "Milad an-Nabi, Id-e-Milad (India)")
    (holiday-fixed 12 21 "Shortest Day of Year (India)")
    (holiday-fixed 12 25 "Merry Christmas (India)"))
   )

(setq holiday-other-holidays (append holiday-other-holidays holiday-indian-holidays))

;; Calendar/Diary
(setq diary-file (concat config-root-path "/.emacs.d/diary"))
(setq view-diary-entries-initially t
	  mark-diary-entries-in-calendar t
	  number-of-diary-entries 7)
(add-hook 'diary-display-hook 'fancy-diary-display)
(add-hook 'today-visible-calendar-hook 'calendar-mark-today)

(add-hook 'list-diary-entries-hook 'sort-diary-entries t)
(setq diary-comment-start "#")
(setq diary-comment-end "#")

;; (setq calendar-week-start-day 1) ;; Monday

;; add week numbers
(setq calendar-intermonth-text
      '(propertize
        (format "%2d"
                (car
                 (calendar-iso-from-absolute
                  (calendar-absolute-from-gregorian (list month day year)))))
        'font-lock-face 'font-lock-warning-face))

(setq calendar-intermonth-header
      (propertize "Wk"                  ; or e.g. "KW" in Germany
                  'font-lock-face 'font-lock-keyword-face))


;; TO USE:
 ;; cfw:open-calendar-buffer
 ;; cfw:open-org-calendar
 ;; cfw:open-diary-calendar
