;;
;;
;; org-present - cf system crafters
;;
;;

;; Set reusable font name variables
(defvar my/fixed-width-font "JetBrains Mono"
  "The font to use for monospaced (fixed width) text.")

(defvar my/variable-width-font "Iosevka Aile"
  "The font to use for variable-pitch (document) text.")

;; Load org-faces to make sure we can set appropriate faces
(require 'org-faces)


;; change emacs appearance for presenting - faces for org levels, face attributes
;;
(defun present-prepare-emacs

  ;; Hide emphasis markers on formatted text
  (setq org-hide-emphasis-markers t)

  ;; Resize Org headings
  (dolist (face '((org-level-1 . 1.10)
                (org-level-2 . 1.05)
                (org-level-3 . 1.0)
                (org-level-4 . 1.0)
                (org-level-5 . 1.0)
                (org-level-6 . 1.0)
                (org-level-7 . 1.0)
                (org-level-8 . 1.0)))
    (set-face-attribute (car face) nil :font my/variable-width-font :weight 'medium :height (cdr face)))

  ;; Make the document title a bit bigger
  (set-face-attribute 'org-document-title nil :font my/variable-width-font :weight 'bold :height 1.3)

  ;; Make sure certain org faces use the fixed-pitch face when variable-pitch-mode is on
  (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-table nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-formula nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-code nil :inherit '( fixed-pitch))
  (set-face-attribute 'org-verbatim nil :inherit '(s fixed-pitch))
  (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch)) 
  (set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch)

  ;;; Centering Org Documents --------------------------------
)


;; Install visual-fill-column
(unless (package-installed-p 'visual-fill-column)
  (package-install 'visual-fill-column))


(defun my/org-present-prepare-slide (buffer-name heading)
  ;; Show only top-level headlines
  (org-overview)

  ;; Unfold the current entry
  (org-show-entry)

  ;; Show only direct subheadings of the slide but don't expand them
  (org-show-children)
)


(defun my/org-present-start ()

  (load-theme 'doom-palenight t)
  (menu-bar-mode 0)
  (tool-bar-mode 0)
  (scroll-bar-mode 0)

  ;; Tweak font sizes
  (setq-local face-remapping-alist '(
                                     (default (:height 1.2) variable-pitch)
                                     (header-line (:height 4.0) variable-pitch)
                                     (org-document-title (:height 1.4) org-document-title)
                                     (org-code (:height 1.2) org-code)
                                     (org-verbatim (:height 1.2) org-verbatim)
                                     (org-block (:height 1.2) org-block)
                                     (org-block-begin-line (:height 0.7) org-block)
									)
  )

  ;; Make the document title a bit bigger
;  (set-face-attribute 'org-document-title nil :font my/variable-width-font :weight 'bold :height 1.3)

  ;; Make sure certain org faces use the fixed-pitch face when variable-pitch-mode is on
;  (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
;  (set-face-attribute 'org-table nil :inherit 'fixed-pitch)
;  (set-face-attribute 'org-formula nil :inherit 'fixed-pitch)
;  (set-face-attribute 'org-code nil :inherit '(fixed-pitch))
;  (set-face-attribute 'org-verbatim nil :inherit '(fixed-pitch))
;  (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
;  (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch)) 
;  (set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch)

  ;; Configure fill width
  (setq visual-fill-column-width 60)
  (setq visual-fill-column-center-text t)

  (setq dfcim display-fill-column-indicator-mode)
  (display-fill-column-indicator-mode 0)
  
  (set-fringe-mode '(0 . 0))
  ;; Set a blank header line string to create blank space at the top
  (setq header-line-format " ")

  ;; Display inline images automatically
  (org-display-inline-images)

  ;; Center the presentation and wrap lines
  (visual-fill-column-mode 1)
  (visual-line-mode 1))

(defun my/org-present-end ()

  (menu-bar-mode t)
  ;(tool-bar-mode 0)
  (scroll-bar-mode t)
  (fringe-mode nil) ; nil=default
  ;; Reset font customizations
  (setq-local face-remapping-alist '((default fixed-pitch default)))

 (face-remap-reset-base 'org-document-title)
 (face-remap-reset-base 'default)
 (face-remap-reset-base 'header-line)
 (face-remap-reset-base 'org-code)
 (face-remap-reset-base 'org-verbatim)
 (face-remap-reset-base 'org-block)
 (face-remap-reset-base 'org-block-begin-line)
; (face-remap-reset-base 'org-table)
; (face-remap-reset-base ')

  ;; Clear the header line string so that it isn't displayed
  (setq header-line-format nil)

  ;; Stop displaying inline images
  (org-remove-inline-images)

  ;; Stop centering the document
  (visual-fill-column-mode 0)
  (visual-line-mode 0)
  (display-fill-column-indicator-mode dfcim)
  
  (disable-theme 'doom-palenight)

  )

;; Turn on variable pitch fonts in Org Mode buffers
(add-hook 'org-mode-hook 'variable-pitch-mode)

;; Register hooks with org-present
(add-hook 'org-present-mode-hook 'my/org-present-start)
(add-hook 'org-present-mode-quit-hook 'my/org-present-end)
(add-hook 'org-present-after-navigate-functions 'my/org-present-prepare-slide)
