(defvar dropbox-root-path "c:/rgd/dropbox") ;; moved dropbox to new d drive on this machine after got new SSD 3/29/18
(defvar config-root-path (getenv "HOME")) ;; 11/28/18 - move to gitlab for emacs config repo to get git-based version control
(defvar org-root-path dropbox-root-path)
(setq custom-file (concat config-root-path "/.emacs.d/emacs.custom-c.el")) ;; c drive version

;(setq org-directory (expand-file-name (concat dropbox-root-path "/org")))
(setq org-conf-el (expand-file-name (concat config-root-path "/.emacs.d/org.conf.el")))

(defvar emacs-conf-path (concat config-root-path "/.emacs.d/emacs.conf.el"))
;(when (file-exists-p custom-file)
;  (load custom-file))


(defvar user-home-directory (concat (getenv "HOME") "/"))
(setq user-emacs-directory (concat user-home-directory ".emacs.d/"))
(defvar local-site-lisp-path (concat user-home-directory "/.emacs.d/lisp"))
;(setq package-quickstart t)

(require 'package)
;(setq package-enable-at-startup nil)

(defvar config-package-path (concat config-root-path "/.emacs.d/elpa/"))
(setq package-user-dir config-package-path)

;; (setq package-archive-priorities
;;       '( ;; ("melpa-stable" . 20)
;;          ;; ("marmalade" . 20)
;;         ("org" . 5)
;;         ("gnu" . 10)
;;         ("melpa" . 20)
;; 		("notgnu" . 30)
;; 		("org" . 5)
;; 		))
;(package-initialize) 

(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/org-roam-20230307.1721")
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/dash-20230714.723") 
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/f-20230823.1159") 
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/s-20220902.1511") 
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/org-9.6.10") 
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/magit-section-20231014.1405") 
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/emacsql-20230417.1448")
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/emacsql-sqlite-builtin-20230409.1847") 
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/compat-29.1.4.2") 

(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/counsel-20231025.2311")
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/ivy-20231025.2311")
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/magit-20231024.1845")
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/swiper-20231025.2311")
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/transient-20231026.1048")
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/rg-20230430.721")
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/wgrep-20230203.1214")
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/spinner-1.7.4")
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/ivy-hydra-20231025.2311")


(package-initialize)

;; (unless package--initialized (package-initialize t))

;; (unless (assoc-default "melpa" package-archives)
;;    (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t))
;; (unless (assoc-default "notgnu" package-archives)
;;    (add-to-list 'package-archives '("notgnu" . "https://elpa.nongnu.org/nongnu/") t))
;; (unless (assoc-default "org" package-archives)
;;    (add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t))

;; (unless (package-installed-p 'use-package)
;;    (package-refresh-contents)
;;    (package-install 'use-package))
;; (setq use-package-verbose t)
;; (setq use-package-always-ensure t)
 
(require 'use-package)



(setq emacs-opt-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.opt.el")))
(load-file emacs-opt-el)

(setq emacs-cal-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.cal.el")))
(load-file emacs-cal-el)

(setq emacs-fn-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.fn.el")))
(load-file emacs-fn-el)
;; ok to here

(setq emacs-spec-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.spec.el")))
(load-file emacs-spec-el)

(setq emacs-key-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.key.el")))
(load-file emacs-key-el)


;(setq emacs-pkg-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.pkg.el")))
;(load-file emacs-pkg-el)

;(load-file org-conf-el)

(use-package org-roam
  :ensure t
  :custom
  (org-roam-directory (file-truename "c:/rgd/dropbox/org/roam/"))
  (org-roam-database-connector 'sqlite-builtin)
  :init
  (setq org-roam-db-location (concat dropbox-root-path "/org/roam/org-roam.db"))
  :bind
   (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n g" . org-roam-graph)
         ("C-c n i" . org-roam-node-insert)
         ("C-c n c" . org-roam-capture)
         ;; Dailies
         ("C-c n j" . org-roam-dailies-capture-today))
  :config
  (setq org-roam-node-display-template (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag)))
  (org-roam-db-autosync-mode)
)

;		    (use-package server
;			  :ensure t
;			  :config
;             (server-start))


;		    (use-package server
;			  :ensure t
;			  :config
;             (server-start))

;     (use-package edit-server
;		:ensure t
;		:config
;		(edit-server-start) )



;(setq emacs-ui-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.ui.el")))
;(load-file emacs-ui-el)

;(rgd/initial-frame-setup) ;; set initial-frame-alist parameters to match registry / ~/.XResources ;  defined in emacs.ui.el 
;(rgd/default-frame-setup) ;; set default-frame-alist to override registry/initial alist for subsequent frames
	  
;(setq emacs-ui-frame-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.ui-frame.el")))
;(load-file emacs-ui-frame-el)
   
;(rgd/post-setup-frame)
