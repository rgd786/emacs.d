(global-eldoc-mode -1)

(defvar home-root-path       (getenv "HOME"))
(defvar dropbox-root-path    "c:/rgd/dropbox")
(defvar cloud-root-path      (concat home-root-path "/gitlab"))
(defvar org-root-path        cloud-root-path)
  
(defvar config-root-path     home-root-path)
(defvar org-root-path cloud-root-path) ;; add /org

(setq org-directory (file-truename (expand-file-name (concat cloud-root-path "/org"))))

(setq org-conf-el (expand-file-name (concat config-root-path "/.emacs.d/org.conf.el")))

(defvar emacs-conf-path (concat config-root-path "/.emacs.d/emacs.conf.el"))

(setq custom-file (concat config-root-path "/.emacs.d/emacs.custom-c.el"))
;(when (file-exists-p custom-file)
  (load custom-file)


(defvar user-home-directory (concat (getenv "HOME") "/"))
(setq user-emacs-directory (concat user-home-directory ".emacs.d/"))
(defvar local-site-lisp-path (concat user-home-directory "/.emacs.d/lisp"))
;(setq package-quickstart t)

(require 'package)
;(setq package-enable-at-startup nil)

(defvar config-package-path (concat config-root-path "/.emacs.d/elpa/"))
(setq package-user-dir config-package-path)

;; (setq package-archive-priorities
;;       '( ;; ("melpa-stable" . 20)
;;          ;; ("marmalade" . 20)
;;         ("org" . 5)
;;         ("gnu" . 10)
;;         ("melpa" . 20)
;; 		("notgnu" . 30)
;; 		("org" . 5)
;; 		))
;(package-initialize) 

(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/org-roam-20240114.1941")
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/dash-20240103.1301") 
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/f-20231219.750") 
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/s-20220902.1511") 
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/org-9.6.19") 
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/magit-20240218.530") 
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/emacsql-20240124.1601")
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/emacsql-sqlite-builtin-20240119.2314") 
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/compat-29.1.4.4") 

(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/counsel-20240214.2118")
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/ivy-20240214.2118")
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/magit-20240218.530")
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/swiper-20240214.2118")
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/transient-20240226.2332")
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/rg-20231202.1023")
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/wgrep-20230203.1214")


(package-initialize)

 
(require 'use-package)



(setq emacs-opt-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.opt.el")))
(load-file emacs-opt-el)
(message "emacs-cal-el")
(setq emacs-cal-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.cal.el")))
(load-file emacs-cal-el)
(message "emacs-fn-el")
(setq emacs-fn-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.fn.el")))
(load-file emacs-fn-el)
;; ok to here
(message "emacs-spec-el")
(setq emacs-spec-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.spec.el")))
(load-file emacs-spec-el)
(message "emacs-key-el")
(setq emacs-key-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.key.el")))
(load-file emacs-key-el)


;(setq emacs-pkg-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.pkg.el")))
;(load-file emacs-pkg-el)

;(load-file org-conf-el) 
(message "loading org")
(use-package org
	:ensure t
	:init
      (setq org-directory (file-truename (expand-file-name (concat cloud-root-path "/org"))))
	  ;;(setq org-directory (expand-file-name (concat org-root-path "/org") ))
	:bind
	  (("\C-cl" . org-store-link)
	   ("\C-cc" . org-capture)
	   ("\C-ca" . org-agenda)
	   ("\C-cb" . org-switchb))
    :config
	  (setq org-link-file-path-type 'relative)
	  (setq org-use-speed-commands t) 
	     ;; use when on headline, '?' to see list; customize 'org-speed-commands-user' to modify
	  (setq org-agenda-include-diary t)
;      (setq org-mobile-directory (concat dropbox-root-path "/Apps/MobileOrg"))  ;; dropbox/org/mobile ??
;      (setq org-mobile-inbox-for-pull (concat org-root-path "/org/mobileorg.org"))
      (setq org-goto-interface 'outline-path-completion) ;; For Ivy support in org-goto:
      (setq org-outline-path-complete-in-steps nil)
	  (setq org-clock-clocked-in-display 'frame-title)
	  (setq org-hide-emphasis-markers t)
      (setq org-adapt-indentation 'headline-data)

      (setq org-hide-block-startup t)
      (setq org-startup-folded "fold")
      
	  ;; Place tags close to the right-hand side of the window
	  (add-hook 'org-finalize-agenda-hook 'place-agenda-tags)
	  (defun place-agenda-tags ()
		"Put the agenda tags by the right border of the agenda window."
		(setq org-agenda-tags-column (- 4 (window-width)))
		(org-agenda-align-tags))
	  
      (set-face-attribute 'org-tag nil :foreground "goldenrod" :height 0.8)
	  
	  ; from: https://zzamboni.org/post/beautifying-org-mode-in-emacs/
	  ; prettify the faces used for headlines
	  (let* ((variable-tuple
			  (cond ((x-list-fonts "Source Sans Pro") '(:font "Source Sans Pro"))
					((x-list-fonts "Lucida Grande")   '(:font "Lucida Grande"))
					((x-list-fonts "Verdana")         '(:font "Verdana"))
					((x-family-fonts "Sans Serif")    '(:family "Sans Serif"))
					(nil (warn "Cannot find a Sans Serif Font.  Install Source Sans Pro."))))
			 (base-font-color     (face-foreground 'default nil 'default))
			 (headline           `(:inherit default :weight bold :foreground ,base-font-color)))

		(custom-theme-set-faces
		 'user
		 `(variable-pitch ((t (:family "Source Sans Pro" :height 120 :weight light))))
		 `(fixed-pitch ((t (:family "Consolas" :slant normal :weight normal :height 1.0 :width normal))))
		 `(org-level-8 ((t (,@headline ,@variable-tuple))))
		 `(org-level-7 ((t (,@headline ,@variable-tuple))))
		 `(org-level-6 ((t (,@headline ,@variable-tuple))))
		 `(org-level-5 ((t (,@headline ,@variable-tuple))))
		 `(org-level-4 ((t (,@headline ,@variable-tuple :height 1.1))))
		 `(org-level-3 ((t (,@headline ,@variable-tuple :height 1.25))))
		 `(org-level-2 ((t (,@headline ,@variable-tuple :height 1.5))))
		 `(org-level-1 ((t (,@headline ,@variable-tuple :height 1.75))))
		 `(org-document-title ((t (,@headline ,@variable-tuple :height 2.0 :underline nil))))
		 `(org-block ((t (:inherit fixed-pitch))))
		 `(org-code ((t (:inherit shadow fixed-pitch))))
		 `(org-document-info ((t (:foreground "dark orange"))))
         `(org-document-info-keyword ((t (:inherit (shadow fixed-pitch)))))
		 `(org-headline-done ((t (:weight bold :strike-through "red"))))
	     `(org-indent ((t :inherit (org-hide fixed-pitch))))
		 `(org-link ((t (:foreground "royal blue" :underline t))))
		 `(org-meta-line ((t (:inherit (font-lock-comment-face fixed-pitch)))))
		 `(org-property-value ((t (:inherit fixed-pitch))) t)
		 `(org-special-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)))))
		 `(org-table ((t (:inherit fixed-pitch :foreground "#83a598"))))
		 `(org-tag ((t (:inherit (shadow fixed-pitch) :weight bold :height 0.8))))
		 `(org-verbatim ((t (:inherit (shadow fixed-pitch)))))
		 ))
	  
	  (setq org-agenda-text-search-extra-files (list
												 (expand-file-name (concat org-directory "/work/ABBTodo.org_archive"))
												 ) )
      (setq org-agenda-files (list
						  (expand-file-name (concat org-directory "/work/ABBAdmin.org"))
					   ))

     (setq org-capture-templates
      '(
        ("m" "Scheduled work meeting (ABBtodo Calendar)" entry
              (file+headline (lambda () (concat org-directory "/work/ABBtodo.org")) "Calendar")
              "* TODO %^{Brief description} %^g
              SCHEDULED: %^T
              %i
              %?
              %a")
        )
      )

     (setq org-capture-templates
      (append org-capture-templates
       '(
           ("h" "home")
           ("ht" "Todo (todo.org) Tasks" entry (file+headline (lambda () (concat org-directory "/todo.org")) "Tasks")
                "* TODO %?\n  %i\n  %a")
         )
       )
     )

      (setq org-tag-persistent-alist '(
				 ("ABB" .       ?A)
				 ("Computer" .  ?z)
				 ))
)

	 
(require 'org-mouse)	

(require 'org-protocol)
(setq org-default-notes-file (concat org-directory "/notes.org")) 

(require 'org-tempo)

(use-package org-wc
  :ensure t
  :after (org)
  )

(use-package org-superstar
  :ensure t
  :after (org)
  :hook (org-mode . org-superstar-mode)
  :config
  (setq org-superstar-headline-bullets-list '("⚫" "◉" "◎" "●" "○" "⯍" "⯎" "⦁" "⬥" "⬦" "⬧" "⬨" "⬩" "⬪" "⬫" )  )


  )

(setq org-ellipsis "…");; ⤵ ≫

(use-package org-bookmark-heading
  :ensure t
  :after (org)
  )

(use-package org-treeusage
  :ensure t
  :bind ("C-c u". org-treeusage-mode)
  :after (org)
  )

(use-package org-super-agenda
  :ensure t
  :after (org)
  :config
  (setq org-agenda-span 'day)
  (setq org-agenda-window-setup 'only-window)
  (setq org-super-agenda-groups
		'(
		  (:name "Log" :log t :order 1) ;; group logged items in own group here, not spreadout in Schedule grid
          (:name "Schedule"
                 :time-grid t
				 :order 2)
          (:name "Scheduled earlier"
                :scheduled past
				:order 3)
		  (:name "Work Today"
		         :and (:scheduled today :tag "work" :priority "A")
				 :order 4)
          (:name "Work"
		         :and (:tag "work" :priority<= "A")
				 :order 5)
		  (:name "Home" :tag "home" :order 6)
		  (:name "Mom" :tag "mom" :order 7)
		  (:name "Overdue"   :deadline past :order 8)
		  (:name "Due today" :deadline today :order 9)
          (:name "Due soon"  :deadline future :order 10)
		  (:name "Habits" :habit t :order 11)
          )
  )
  (org-super-agenda-mode 1)
)
(defun org-roam-rg-search ()  
  "Search org-roam directory using consult-ripgrep. With live-preview."  
  (interactive) 
  (require 'org-roam)
  (counsel-rg nil org-roam-directory nil nil))

(message "loading org-roam")
(setq org-roam-directory (file-truename (concat org-directory "/roam")))
(setq org-roam-db-location (file-truename (concat org-roam-directory "/org-roam.db")))

(use-package org-roam
  :ensure t
  :custom
;  (org-roam-directory (file-truename (concat org-directory "/roam")))
  (org-roam-database-connector 'sqlite-builtin)
  :init
;  (setq org-roam-db-location (file-truename (concat org-directory "/org/roam/org-roam.db")))
  :bind
   (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n g" . org-roam-graph)
         ("C-c n i" . org-roam-node-insert)
         ("C-c n c" . org-roam-capture)
         ;; Dailies
         ("C-c n j" . org-roam-dailies-capture-today))
  :config
  (setq org-roam-node-display-template (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag)))
  (org-roam-db-autosync-mode)
)
(message "org-roam loaded")
;		    (use-package server
;			  :ensure t
;			  :config
;             (server-start))


;		    (use-package server
;			  :ensure t
;			  :config
;             (server-start))

;     (use-package edit-server
;		:ensure t
;		:config
;		(edit-server-start) )



;(setq emacs-ui-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.ui.el")))
;(load-file emacs-ui-el)

;(rgd/initial-frame-setup) ;; set initial-frame-alist parameters to match registry / ~/.XResources ;  defined in emacs.ui.el 
;(rgd/default-frame-setup) ;; set default-frame-alist to override registry/initial alist for subsequent frames
	  
;(setq emacs-ui-frame-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.ui-frame.el")))
;(load-file emacs-ui-frame-el)
   
;(rgd/post-setup-frame)
;(find-file (concat cloud-root-path "/org/roam/20231110110618-test0.org"))
(message "org-directory= %s" org-directory)
(message "org-roam-directory= %s" org-roam-directory)
(message "org-roam-db-location= %s" org-roam-db-location)
(message "ort.el done")
