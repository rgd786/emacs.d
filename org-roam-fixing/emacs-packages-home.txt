  amx                            20230413.1210  installed             Alternative M-x with extra features.
  async                          20231105.1713  installed             Asynchronous processing in Emacs
  avy                            20230420.404   installed             Jump to arbitrary positions in visible text and select text quickly.
  bbdb                           20231023.544   installed             Big Brother DataBase
  browse-kill-ring               20231104.1450  installed             interactively insert items from kill-ring
  calfw                          20180118.45    installed             Calendar view framework on Emacs
  calfw-cal                      20170411.220   installed             calendar view for emacs diary
  calfw-org                      20170411.220   installed             calendar view for org-agenda
  counsel                        20231025.2311  installed             Various completion functions using Ivy
  deft                           20210707.1633  installed             quickly browse, filter, and edit plain text notes
  delight                        1.7            installed             A dimmer switch for your lighter text
  edit-server                    20220908.1014  installed             server that responds to edit requests from Chrome
  elgrep                         20230814.1215  installed             Searching files for regular expressions
  emacsql-sqlite                 20230225.2205  installed             EmacSQL back-end for SQLite
  emacsql-sqlite-builtin         20230409.1847  installed             EmacSQL back-end for SQLite using builtin support
  emacsql-sqlite-module          20230409.1847  installed             EmacSQL back-end for SQLite using a module
  gcode-mode                     20230823.2141  installed             Simple G-Code major mode
  git-timemachine                20230630.1214  installed             Walk through git revisions of a file
  golden-ratio                   20230912.1825  installed             Automatic resizing of Emacs windows to the golden ratio
  google-this                    20170810.1215  installed             A set of functions and bindings to google under point.
  google-translate               20220921.245   installed             Emacs interface to Google Translate
  helpful                        20231028.516   installed             A better *help* buffer
  hydra                          20220910.1206  installed             Make bindings that stick around.
  ivy                            20231025.2311  installed             Incremental Vertical completYon
  ivy-posframe                   20211217.234   installed             Using posframe to show Ivy
  magit                          20231103.1516  installed             A Git porcelain inside Emacs.
  markdown-mode                  20231028.853   installed             Major mode for Markdown-formatted text
  mode-icons                     20230911.20    installed             Show icons for modes
  org-bookmark-heading           20230517.1141  installed             Emacs bookmark support for Org mode
  org-contrib                    0.4.2          installed             Unmaintained add-ons for Org-mode
  org-roam                       20230307.1721  installed             A database abstraction layer for Org-mode
  org-roam-timestamps            20221104.1544  installed             Keep track of modification times for org-roam
  org-roam-ui                    20221105.1040  installed             User Interface for Org-roam
  org-super-agenda               20230924.5     installed             Supercharge your agenda
  org-superstar                  20230116.1358  installed             Prettify headings and plain lists in Org mode
  org-treeusage                  20221011.1301  installed             Examine the usage of org headings in a tree-like manner
  org-wc                         20200731.2244  installed             Count words in org mode trees.
  page-break-lines               20230804.658   installed             Display ^L page breaks as tidy horizontal lines
  pkg-info                       20150517.1143  installed             Information about packages
  popup                          20230819.2306  installed             Visual Popup User Interface
  powerline                      20221110.1956  installed             Rewrite of Powerline
  projectile                     20231013.1509  installed             Manage and navigate projects in Emacs easily
  rg                             20230430.721   installed             A search tool based on ripgrep
  sr-speedbar                    20161025.831   installed             Same frame speedbar
  sunshine                       20200306.1711  installed             Provide weather and forecast information.
  treemacs                       20231107.2049  installed             A tree style file explorer package
  treemacs-icons-dired           20230930.1141  installed             Treemacs icons for dired
  treemacs-magit                 20230830.1936  installed             Magit integration for treemacs
  treemacs-projectile            20230703.1929  installed             Projectile integration for treemacs
  undo-tree                      0.8.2          installed             Treat undo history as a tree
  use-package                    20230426.2324  installed             A configuration macro for simplifying your .emacs
  which-key                      20230905.2128  installed             Display available keybindings in popup
  yasnippet                      20230914.1400  installed             Yet another snippet extension for Emacs
  yasnippet-snippets             20230815.820   installed             Collection of yasnippet snippets
