;; org.conf.el
;;
;;  see org.conf.archive.el - for previous stuff and extra comments, hints, etc.

(message "Configuring for org-mode")
(use-package org
	:ensure t
	:init
	  (setq org-directory (downcase(expand-file-name (concat org-root-path "/org") )))
	:bind
	  (("\C-cl" . org-store-link)
	   ("\C-cc" . org-capture)
	   ("\C-ca" . org-agenda)
	   ("\C-cb" . org-switchb))
    :config
	  (setq org-link-file-path-type 'relative)
	  (setq org-use-speed-commands t) ;; use when on headline, '?' to see list; customize 'org-speed-commands-user' to modify
	  (setq org-agenda-include-diary t)
      (setq org-mobile-directory (concat dropbox-root-path "/Apps/MobileOrg"))  ;; dropbox/org/mobile ?? not needed since organice
      (setq org-mobile-inbox-for-pull (concat org-root-path "/org/mobileorg.org"))
      (setq org-goto-interface 'outline-path-completion) ;; For Ivy support in org-goto:
      (setq org-outline-path-complete-in-steps nil)
	  (setq org-clock-clocked-in-display 'frame-title)
	  (setq org-hide-emphasis-markers t)
      (setq org-adapt-indentation 'headline-data)

      (setq org-hide-block-startup t)
      (setq org-startup-folded "fold")
      
	  ;; Place tags close to the right-hand side of the window
	  (add-hook 'org-finalize-agenda-hook 'place-agenda-tags)
	  (defun place-agenda-tags ()
		"Put the agenda tags by the right border of the agenda window."
		(setq org-agenda-tags-column (- 4 (window-width)))
		(org-agenda-align-tags))
	  
      (set-face-attribute 'org-tag nil :foreground "goldenrod" :height 0.8)
	  
	  ; from: https://zzamboni.org/post/beautifying-org-mode-in-emacs/
	  ; prettify the faces used for headlines
	  (let* ((variable-tuple
			  (cond ((x-list-fonts "Source Sans Pro") '(:font "Source Sans Pro"))
					((x-list-fonts "Lucida Grande")   '(:font "Lucida Grande"))
					((x-list-fonts "Verdana")         '(:font "Verdana"))
					((x-family-fonts "Sans Serif")    '(:family "Sans Serif"))
					(nil (warn "Cannot find a Sans Serif Font.  Install Source Sans Pro."))))
			 (base-font-color     (face-foreground 'default nil 'default))
			 (headline           `(:inherit default :weight bold :foreground ,base-font-color)))

		(custom-theme-set-faces
		 'user
		 `(variable-pitch ((t (:family "Source Sans Pro" :height 120 :weight light))))
		 `(fixed-pitch ((t (:family "Consolas" :slant normal :weight normal :height 1.0 :width normal))))
		 `(org-level-8 ((t (,@headline ,@variable-tuple))))
		 `(org-level-7 ((t (,@headline ,@variable-tuple))))
		 `(org-level-6 ((t (,@headline ,@variable-tuple))))
		 `(org-level-5 ((t (,@headline ,@variable-tuple))))
		 `(org-level-4 ((t (,@headline ,@variable-tuple :height 1.1))))
		 `(org-level-3 ((t (,@headline ,@variable-tuple :height 1.25))))
		 `(org-level-2 ((t (,@headline ,@variable-tuple :height 1.5))))
		 `(org-level-1 ((t (,@headline ,@variable-tuple :height 1.75))))
		 `(org-document-title ((t (,@headline ,@variable-tuple :height 2.0 :underline nil))))
		 `(org-block ((t (:inherit fixed-pitch))))
		 `(org-code ((t (:inherit shadow fixed-pitch))))
		 `(org-document-info ((t (:foreground "dark orange"))))
         `(org-document-info-keyword ((t (:inherit (shadow fixed-pitch)))))
		 `(org-headline-done ((t (:weight bold :strike-through "red"))))
	     `(org-indent ((t :inherit (org-hide fixed-pitch))))
		 `(org-link ((t (:foreground "royal blue" :underline t))))
		 `(org-meta-line ((t (:inherit (font-lock-comment-face fixed-pitch)))))
		 `(org-property-value ((t (:inherit fixed-pitch))) t)
		 `(org-special-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)))))
		 `(org-table ((t (:inherit fixed-pitch :foreground "#83a598"))))
		 `(org-tag ((t (:inherit (shadow fixed-pitch) :weight bold :height 0.8))))
		 `(org-verbatim ((t (:inherit (shadow fixed-pitch)))))
		 ))
	  
	  (setq org-agenda-text-search-extra-files (list
												 (expand-file-name (concat org-directory "/work/ABBTodo.org_archive"))
												 (expand-file-name (concat org-directory "/work/HarmonyAPI.org_archive"))
												 (expand-file-name (concat org-directory "/work/NG.org_archive"))
												 (expand-file-name (concat org-directory "/work/SPE.org_archive"))
												 (expand-file-name (concat org-directory "/todo.org_archive"))
												 (expand-file-name (concat org-directory "/notes.org_archive"))
												 ) )
      (setq org-agenda-files (list
						  (expand-file-name (concat org-directory "/work/ABBAdmin.org"))
						  (expand-file-name (concat org-directory "/projects.org"))
						  (expand-file-name (concat org-directory "/rel.org"))
						  (expand-file-name (concat org-directory "/rgd.org"))
						  (expand-file-name (concat org-directory "/todo.org"))
						  (expand-file-name (concat org-directory "/home.org"))
						  (expand-file-name (concat org-directory "/mom.org"))
						  (expand-file-name (concat org-directory "/writing.org"))
						  (expand-file-name (concat org-directory "/work/ABBtodo.org"))
						  (expand-file-name (concat org-directory "/work/Composer.org"))
						  (expand-file-name (concat org-directory "/dev/dev.org"))
						  (expand-file-name (concat org-directory "/work/HarmonyAPI.org"))
						  (expand-file-name (concat org-directory "/work/NG.org"))
						  (expand-file-name (concat org-directory "/work/SPE.org"))
						  (expand-file-name (concat org-directory "/work/Training.org"))
					   ))

	  ;; org capture templates
	  ;; https://orgmode.org/manual/Template-elements.html 
	  ;; ( "<key>"      - key to activate this template
	  ;;   "description"
	  ;;   <type>       - entry      - org node
	  ;;                - item       - plain list item
	  ;;                - checkitem  - checkbox list item
	  ;;                - table-line - new line in table
	  ;;                - plain      - text inserted as is
	  ;;  <target>      - '(file "path/to/file")'
	  ;;                - '(id "id of existing org entry")'
	  ;;                - '(file+headline "filename" "node headline")'
	  ;;                - '(file+olp "filename" "level1 heading" "level2" ...)'
	  ;;                - '(file+regexp "filename" "regexp to locate location")'
	  ;;                - '(file+olp+datetree "filename" [ "Level 1 heading" ...])’
	  ;;                - '(file+function "filename" function-finding-location)'
	  ;;                - '(clock)'  - file to entry currently being clocked
	  ;;                - '(function function-finding-location)'
	  ;; <template>

;;	            ("d" "Diary" entry
;;		   (file+olp+datetree (lambda () (concat org-directory "/todo.org")) "Calendar")
;;;;            "%K - %a\n%i\n%?\n")
;;		   "* Diary %^g
;;            %i
;;            %?")

     (setq org-capture-templates
      '(
        ("m" "Scheduled work meeting (ABBtodo Calendar)" entry
              (file+headline (lambda () (concat org-directory "/work/ABBtodo.org")) "Calendar")
              "* TODO %^{Brief description} %^g
              SCHEDULED: %^T
              %i
              %?
              %a")

        ("i" "Work task - interruption (ABBtodo Calendar)" entry
             (file+headline (lambda () (concat org-directory "/work/ABBtodo.org")) "Calendar")
             "* TODO %^{Brief description} %^g
              SCHEDULED: %^T
              %i
              %?
              %a")

        ("s" "SPE task - (SPE.org Calendar)" entry
             (file+headline (lambda () (concat org-directory "/work/SPE.org")) "Calendar")
             "* TODO %^{Brief description} %^g
              SCHEDULED: %^T
              %i
              %?
              %a")

        ("c" "CAPI task - (HarmonyAPI.org Calendar)" entry
             (file+headline (lambda () (concat org-directory "/work/HarmonyAPI.org")) "Calendar")
             "* TODO %^{Brief description} %^g
              SCHEDULED: %^T
              %i
              %?
              %a")


        ("e" "Email (ABBtodo email)" entry
             (file+headline (lambda () (concat org-directory "/work/ABBtodo.org")) "general email questions etc.")
             "* TODO %^{Brief description} %^g
              SCHEDULED: %^T
              %i
              %?
              %a")
        )
      )

     (setq org-capture-templates
      (append org-capture-templates
       '(
           ("h" "home")

           ("hd" "Diary (todo.org Calendar)" entry
       (file+olp+datetree (lambda () (concat org-directory "/todo.org")) "Calendar")
       "* Diary %a\n%i\n%?\n")

           ("hn" "Note (notes.org) Notes" entry (file+headline "" "Notes") "* Note: %?\n  %U\n  %i\n  %a\n\n\n\n" :empty-lines 1)
           ("hL" "Link (notes.org) Links" entry (file+headline "" "Links") "* Link: %?\n  %U\n  %i\n  %a\n\n\n\n" :empty-lines 1)
           ("hp" "Protocol" entry (file+headline "" "Inbox") "* %^{Title}\nSource: %u, %c\n #+BEGIN_QUOTE\n%i\n#+END_QUOTE\n\n\n%?")

           ("ha" "Appointment (todo.org) Calendar" entry
       (file+olp+datetree (lambda () (concat org-directory "/todo.org")) "Calendar")
       "* TODO %^{Brief description} %^g
              SCHEDULED: %^T
              %i
              %?")

           ("ho" "mOm stuff (mom.org) Calendar" entry (file+headline (lambda () (concat org-directory "/mom.org")) "Calendar")
       "* TODO %^{Brief description} %^g
              SCHEDULED: %^T
              %i
              %?
              %a")
           ("ht" "Todo (todo.org) Tasks" entry (file+headline (lambda () (concat org-directory "/todo.org")) "Tasks")
                "* TODO %?\n  %i\n  %a")
         )
       )
     )

      (setq org-tag-persistent-alist '(
				 ("ABB" .       ?A)
				 ("Cars" .      ?a)
				 ("call" .      ?c)
				 ("Craig" .     ?C)
				 ("Deb" .       ?D)
				 ("Emacs" .     ?e)
				 ("Finances" .  ?F)
				 ("Family" .    ?f)
				 ("get" .       ?g)
				 ("HAPI" .      ?H)
				 ("Home" .      ?h)
				 ("Insurance" . ?i)
				 ("Jamie" .     ?J)
				 ("lookup" .    ?l)
				 ("Mom" .       ?M)
				 ("Medical" .   ?m)
				 ("org" .       ?o)
				 ("Church" .    ?p) ;; pioneer
				 ("Rel" .       ?r)
				 ("Sean" .      ?S)
				 ("College" .   ?s) ;; school
				 ("Traveller" . ?T)
				 ("UNCC" .      ?U)
				 ("School" .    ?w)
				 ("Composer" .  ?y) ;; coyote
				 ("Computer" .  ?z)
				 ))
)
	 
(require 'org-mouse)	

(require 'org-protocol)
(setq org-default-notes-file (concat org-directory "/notes.org")) 

(require 'org-tempo)

(use-package org-wc
  :ensure t
  :after (org)
  )

(use-package org-superstar
  :ensure t
  :after (org)
  :hook (org-mode . org-superstar-mode)
  :config
  (setq org-superstar-headline-bullets-list '("⚫" "◉" "◎" "●" "○" "⯍" "⯎" "⦁" "⬥" "⬦" "⬧" "⬨" "⬩" "⬪" "⬫" )  )


  )

(setq org-ellipsis "…");; ⤵ ≫
;; or
;;right arrows
;; “↝” “⇉” “⇝” “⇢” “⇨” “⇰” “➔” “➙” “➛” “➜” “➝” “➞”
;; “➟” “➠” “➡” “➥” “➦” “➧” “➨”
;; “➩” “➪” “➮” “➯” “➱” “➲”
;; “➳” “➵” “➸” “➺” “➻” “➼” “➽”
;; arrow heads
;; “➢” “➣” “➤” “≪”, “≫”, “«”, “»”
;; other arrows
;; “↞” “↠” “↟” “↡” “↺” “↻”
;; lightening
;; “⚡”
;; other symbols
;; …, ▼, ↴, , ∞, ⬎, ⤷, ⤵


;; (use-package org-bullets 
;;   :ensure t
;;   :init
;;   (setq org-bullets-bullet-list  ;; most of these are from Symbola font q.v.
;;   ;; '("◉" "◎" "○" "►" "◇" "⚫"))
;;   ;; '("⓵" "⓶" "⓷" "⓸" "⓹" "⓺" "⓻" "⓼" "⓽" "⓾" "⨀" "⭙" "⨷" "◈"))
;;   ;; '("➊" "➋" "➌" "➍" "➎" "➏" "➐" "➑" "➒" "➓"))
;;   ;; '("⬤" "☀" "⬢" "⬣" "⬟" "⬠" "⬡" "⯁" "⯀" "⯍" "⯎" "⦁" "⬥" "⬦" "⬧" "⬨" "⬩" "⬪" "⬫"))
;; 		;; '("⬤" "◯" "⚫" "◉" "◎" "●" "○" "⯍" "⯎" "⦁" "⬥" "⬦" "⬧" "⬨" "⬩" "⬪" "⬫"))
;; 		;; - big bullet just a tad too big... (custom font size?)
;;  	 '("⚫" "◉" "◎" "●" "○" "⯍" "⯎" "⦁" "⬥" "⬦" "⬧" "⬨" "⬩" "⬪" "⬫"))
;;   :after (org)
;;   :config
;;   (setcdr org-bullets-bullet-map nil)
;;   (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
;; ;;   (org-bullets-mode 1)
;;   )
(use-package org-bookmark-heading
  :ensure t
  :after (org)
  )

(use-package org-treeusage
  :ensure t
  :bind ("C-c u". org-treeusage-mode)
  :after (org)
  )

(use-package org-super-agenda
  :ensure t
  :after (org)
  :config
  (setq org-agenda-span 'day)
  (setq org-agenda-window-setup 'only-window)
  (setq org-super-agenda-groups
		'(
		  (:name "Log" :log t :order 1) ;; group logged items in own group here, not spreadout in Schedule grid
          (:name "Schedule"
                 :time-grid t
				 :order 2)
          (:name "Scheduled earlier"
                :scheduled past
				:order 3)
		  (:name "Work Today"
		         :and (:scheduled today :tag "work" :priority "A")
				 :order 4)
          (:name "Work"
		         :and (:tag "work" :priority<= "A")
				 :order 5)
		  (:name "Home" :tag "home" :order 6)
		  (:name "Mom" :tag "mom" :order 7)
		  (:name "Overdue"   :deadline past :order 8)
		  (:name "Due today" :deadline today :order 9)
          (:name "Due soon"  :deadline future :order 10)
		  (:name "Habits" :habit t :order 11)
          )
  )
  (org-super-agenda-mode 1)
)
(defun org-roam-rg-search ()  
  "Search org-roam directory using consult-ripgrep. With live-preview."  
  (interactive) 
  (require 'org-roam)
  (counsel-rg nil org-roam-directory nil nil))

(message "loading org-roam")

(setq org-roam-directory (downcase (file-truename (concat cloud-root-path "/org/roam/")))) 
(setq org-roam-database-connector 'sqlite-builtin)
(message "org-roam-directory: %s\n" org-roam-directory)
(message "dropbox-root-path: %s" dropbox-root-path)
(message "cloud-root-path: %s" cloud-root-path)

(use-package org-roam
;    :after org
    :ensure t
    :custom
	                 ;; diffs for home and work laptop - 
					 ;; org-roam dir 
					 ;;    having it in dropbox could be useful but how often would I use it? 
					 ;;    not in gitlab - or  should I? make new org-roam gitlab project 
					 
					 ;; leave org-roam.db not in cloud (db/gl), copies on each machine?
					 ;;   may want to break up org files to work / home, diff agendas, org-roams
					 
					 ;; custom file - since org-agenda-files are absolute - need diff for the two laptops :( 
					 ;;    unless can use "~/gitlab/org" paths for all
					 ;;    but... have D: on home laptop 100s of GB free could switch home to custom-d
					 ;;
    (org-roam-directory (downcase (file-truename (concat cloud-root-path "/org/roam/")))) 
	                     ; (file-truename "c:/rgd/dropbox/org/roam/")) -- case seems to matter in org-roam-file-p
    (org-roam-database-connector 'sqlite-builtin)
    (message "org-roam-directory: " org-roam-directory)
    (message "dropbox-root-path: " dropbox-root-path)
	(message "cloud-root-path: " cloud-root-path)
    :init
    (setq org-roam-db-location (downcase (concat user-emacs-directory "/org-roam/org-roam.db")))
    :bind 
     (("C-c n f" . org-roam-node-find)
           ("C-c n r" . org-roam-node-random)
           ("C-c n l" . org-roam-buffer-toggle)
           ("C-c n s" . org-roam-db-sync)
           (:map org-mode-map
                 (
                  ("C-c n i" . org-roam-node-insert)
                  ("C-c n o" . org-id-get-create)
                  ("C-c n t" . org-roam-tag-add)
                  ("C-c n a" . org-roam-alias-add)
                  ("C-c n g" . org-roam-graph)
                  ("C-c n c" . org-roam-capture)
                  ("C-c n j" . org-roam-dailies-capture-today)
                  ("C-c n r" . org-roam-ref-find)
                  ("C-c n R" . org-roam-ref-add)
                  ("C-c n x" . org-roam-rg-search)
                 )
            ))
    :config
       (org-roam-db-autosync-enable)     
)


	
(use-package org-download
  :ensure t
  :after org
  :custom
  (org-download-method 'directory)
  (org-download-heading-level nil)
  (org-download-timestamp "%Y%m%d-%H%M%S_")
  (org-image-actual-width 600)
  (org-download-screenshot-method "nircmd clipboard saveimage %s")
  :bind
  ("C-M-y" . rgd-org-download-screenshot)
  )

;;(require 'org-download)
(defun rgd-org-download-screenshot (&optional basename)
   "Call org-download-screenshot after setting the org-download-image-dir based on current org buffer."
   (interactive)
   (progn
        (message "rgd-org-download-screenshot start")
        (setq-local org-download-image-dir (concat (file-name-directory (buffer-file-name)) "/img/" (file-name-base buffer-file-name) "/") )
		(org-download-screenshot basename)
        (message "rgd-org-download-screenshot end")
	)
)

(defun rgd-org-screenshot ()
  (interactive)
                                        ; create .pic directory if does not exist.
  (unless (file-exists-p "./img")
    (make-directory "./img"))
  (setq filename
        (concat
         (make-temp-name
          (concat (buffer-file-name)
                  "_"
                  (format-time-string "%Y%m%d_%H%M%S_")) ) ".png"))
  (setq filename (concat "./img/" (file-name-nondirectory filename)))
  (shell-command "powershell.exe snippingtool /clip")
  (shell-command (concat "powershell.exe -command \"Add-Type -AssemblyName System.Windows.Forms;if ([System.Windows.Forms.Clipboard]::ContainsImage()) {\$image = [System.Windows.Forms.Clipboard]::GetImage();[System.Drawing.Bitmap]\$image.Save('" filename "',[System.Drawing.Imaging.ImageFormat]::Png); Write-Output 'clipboard content saved as file'} else {Write-Output 'clipboard does not contain image data'}\""))
  (insert (concat "[[file:" filename "]]"))
  (org-display-inline-images))
;; from Jeff-avatar: https://gist.github.com/cdaven/135298087efb0ffc5dab93fd56249b38?permalink_comment_id=4242464 

(global-set-key [s-f7] 'rgd-org-screenshot)
