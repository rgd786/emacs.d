(defvar dropbox-root-path "c:/rgd/dropbox") ;; moved dropbox to new d drive on this machine after got new SSD 3/29/18
(defvar config-root-path (getenv "HOME")) ;; 11/28/18 - move to gitlab for emacs config repo to get git-based version control
(defvar org-root-path dropbox-root-path)
(setq custom-file (concat config-root-path "/.emacs.d/emacs.custom-c.el")) ;; c drive version

(setq org-directory (expand-file-name (concat dropbox-root-path "/org")))
(setq org-conf-el (expand-file-name (concat config-root-path "/.emacs.d/org.conf.el")))

(defvar emacs-conf-path (concat config-root-path "/.emacs.d/emacs.conf.el"))
;(when (file-exists-p custom-file)
;  (load custom-file))

(defvar user-home-directory (concat (getenv "HOME") "/"))
(setq user-emacs-directory (concat user-home-directory ".emacs.d/"))
(defvar local-site-lisp-path (concat user-home-directory "/.emacs.d/lisp"))
;(setq package-quickstart t)

(require 'package)
;(setq package-enable-at-startup nil)

(defvar config-package-path (concat config-root-path "/.emacs.d/elpa/"))
(setq package-user-dir config-package-path)


(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/org-roam-20230307.1721")
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/dash-20230714.723") 
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/f-20230823.1159") 
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/s-20220902.1511") 
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/org-9.6.10") 
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/magit-section-20231014.1405") 
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/emacsql-20230417.1448")
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/emacsql-sqlite-builtin-20230409.1847") 
(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/compat-29.1.4.2") 

;(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/counsel-20231025.2311")
;(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/ivy-20231025.2311")
;(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/magit-20231024.1845")
;(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/swiper-20231025.2311")
;(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/transient-20231026.1048")
;(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/rg-20230430.721")
;(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/wgrep-20230203.1214")
;(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/spinner-1.7.4")
;(add-to-list 'load-path "c:/users/USRODAV2/.emacs.d/elpa/ivy-hydra-20231025.2311")


(package-initialize)

 
(require 'use-package)


(setq emacs-opt-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.opt.el")))
(load-file emacs-opt-el);

(setq emacs-cal-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.cal.el")))
(load-file emacs-cal-el)

(setq emacs-fn-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.fn.el")))
(load-file emacs-fn-el)
;; ok to here

;(setq emacs-spec-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.spec.el")))
;(load-file emacs-spec-el)

;(setq emacs-key-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.key.el")))
;(load-file emacs-key-el)


;(setq emacs-pkg-el (expand-file-name (concat config-root-path "/.emacs.d/emacs.pkg.el")))
;(load-file emacs-pkg-el)

;(load-file org-conf-el) 

(use-package org
	:ensure t
	:init
	  (setq org-directory (expand-file-name (concat org-root-path "/org") ))
	:bind
	  (("\C-cl" . org-store-link)
	   ("\C-cc" . org-capture)
	   ("\C-ca" . org-agenda)
	   ("\C-cb" . org-switchb))
    :config
      (setq org-link-file-path-type 'relative)
      (setq org-use-speed-commands t)
      (setq org-agenda-include-diary t)
      (setq org-goto-interface 'outline-path-completion) ;; For Ivy support in org-goto:
      (setq org-outline-path-complete-in-steps nil)
      (setq org-clock-clocked-in-display 'frame-title)
      (setq org-hide-emphasis-markers t)
      (setq org-adapt-indentation 'headline-data)

      (setq org-hide-block-startup t)
      (setq org-startup-folded "fold")
)


(use-package org-roam
  :ensure t
  :custom
  (org-roam-directory "c:/rgd/dropbox/org/roam/")
  (org-roam-database-connector 'sqlite-builtin)
  :init
  (setq org-roam-db-location "c:/rgd/dropbox/org/roam/org-roam.db")
  :bind
   ( ("C-c n l" . org-roam-buffer-toggle)
     ("C-c n f" . org-roam-node-find)
     ("C-c n g" . org-roam-graph)
     ("C-c n i" . org-roam-node-insert)   )
  :config
  (org-roam-db-autosync-mode)
)

