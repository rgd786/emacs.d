; Emacs version: GNU Emacs 29.2 (build 2, x86_64-w64-mingw32) of 2024-02-01
; from: https://ftp.gnu.org/gnu/emacs/windows/emacs-29/
;
; PC => Windows 10 Home 222H2 19045.4046     Lenovo Legion 5 17IMH05H (i7; 16GB)

; Start emacs with 
; c:\sys\emacs\bin\emacs.exe --no-splash -q ort26.el

; My ~/.emacs.d/elpa contained nothing, then eval-buffer'd ort26.el below and then it contains:
; compat-29.1.4.4
; dash-20240103.1301
; emacsql-20240124.1601
; gnupg
; magit-section-20240307.2052
; org-roam-20240114.1941

; My 'configuration' file (ort26.el - org-roam test):

;------------------------
(setq org-directory (file-truename "C:/Users/rgd/gitlab/org"))
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)
(package-initialize)
(require 'use-package)
(setq org-roam-directory (file-truename "C:/Users/rgd/gitlab/org/roam"))
(setq org-fold-core-style "overlays")
(setq org-roam-db-location (file-truename "C:/Users/rgd/gitlab/org/roam/org-roam.db"))
(setq org-roam-completion-everywhere t)

(use-package org-roam
  :ensure t
  :custom
   (org-roam-database-connector 'sqlite-builtin)
   (org-roam-directory (file-truename "C:/Users/rgd/gitlab/org/roam")) 
  :bind
   ( ("C-c n l" . org-roam-buffer-toggle)
     ("C-c n f" . org-roam-node-find)      )
  :config
   (org-roam-db-autosync-mode) )

(message "ortmin.el done")
; end of ort26.el
;-------------------------

; org/roam directory has 4 node files and the org-roam.db:
;
; 20231101210530-orgroambuffernotupdating.org
; 20231110110618-test0.org
; 20231110110642-test1.org
; 20231110110647-test2.org
; org-roam.db

; It evals without error.  Then I do "C-c n f", it prompts for "node:".  I type test1<TAB>, it completes. Hit <RET>.
; The test1 node loads as expected. 
;  
; Type "C-c n l" and the org-roam buffer appears in a split window below the test1 window. 
; Seems ok.
; Type "C-c n f" and enter node 'test2' - it loads as expected, replacing the test1 node in the same window
; BUT... the org-roam buffer has not updated - it still shows the backlinks for node test1.

; Keep checking value of post-command-hook with different configs - never does it contain the setup-redisplay-h...
; and org-roam-db-autosync-mode - enabled 

; Have spent days trying to figure this out.  Learned how to use debug and edebug basics (but not enough to figure 
; it out).  
;
; Googled for possible solutions 
;
; possible solution #1
; https://org-roam.discourse.group/t/org-roam-buffer-do-not-refresh-automaticly/2873

; config above seems to match recommended (I hope).
; Open test1 node, open org-roam buffer, org-roam should be loaded (but not org etc.)
; M-x org-roam-db-autosync-mode -> say "disabled".  Do again, processes files.  Post-command-hook still same:
; missing org-roam-buffer--setup-redisplay-h.

; Checking post-command-hook in org-roam buffer - local:

;post-command-hook is a variable defined in ‘C source code’.
;
;Its value is (t magit-section-post-command-hook)
;Local in buffer *org-roam*; global value is 
;(global-font-lock-mode-check-buffers global-eldoc-mode-check-buffers)

; Checking mode, C-h m for *org-roam* buffer:

;Minor mode enabled in this buffer: Font-Lock
;The major mode is Org-roam mode defined in org-roam-mode.el:

; start org-roam before org,etc. But seems missing setup-redisplay hook and minor mode. 


; possible solution #2
; https://org-roam.discourse.group/t/org-roam-buffer-does-not-update-on-click-link-buffer-switch/2364

; Not using org-roam-buffer-display-dedicated, using org-roam-buffer-toggle 
; pop-up-windows -> t
; display-buffer-base-action -> nil
; 
; suggests maybe for display-buffer-base-action:
;  ((display-buffer-reuse-window display-buffer-use-some-window))
; added:
; (setq display-buffer-base-action
;                         '((display-buffer-reuse-window display-buffer-use-some-window)
;                          (reusable-frames . t)))
; no effect - post-command-hook still missing setup redisplay, and mode missing org-roam minor mode.

