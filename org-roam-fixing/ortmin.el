(defvar home-root-path       (getenv "HOME"))
(defvar cloud-root-path      (concat home-root-path "/gitlab"))
(defvar org-root-path        cloud-root-path)

(setq org-directory (file-truename (expand-file-name (concat cloud-root-path "/org"))))

(defvar user-home-directory (concat (getenv "HOME") "/"))
(setq user-emacs-directory (concat user-home-directory ".emacs.d/"))

(require 'package)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)

(package-initialize)
 
(require 'use-package)

(setq org-roam-directory (file-truename (concat org-directory "/roam")))

(setq org-fold-core-style "overlays")

;(use-package org
;  :ensure t
;  :init
;  (setq org-directory (file-truename (expand-file-name (concat cloud-root-path "/org"))))
;)

(use-package org-roam
  :ensure t
  :custom
   (org-roam-database-connector 'sqlite-builtin)
   (org-roam-directory (file-truename (concat org-directory "/roam")))
  :init
   (setq org-roam-db-location (file-truename (concat org-roam-directory "/org-roam.db")))
   (setq org-roam-completion-everywhere t)
  :bind
   ( ("C-c n l" . org-roam-buffer-toggle)
     ("C-c n f" . org-roam-node-find)      )
   :config
   (org-roam-db-autosync-mode)
)

;(org-roam-db-autosync-mode)

(message "ortmin.el done")
