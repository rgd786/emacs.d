;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; emacs.wm.el -- window management elisp
;
; cf
;    emacs.pkg.elisp       golden-ratio
;    emacs.comp-ivy.el  emacs.comp-vert.el  for posframe settings for ivy/vertico
;    emacs.fn.el - set-frame-...resolutions()


(defun ignore-error-wrapper (fn)                     ; add to emacs.fn.el ??
  "Funtion return new function that ignore errors.
   The function wraps a function with `ignore-errors' macro."
  (lexical-let ((fn fn))
    (lambda ()
      (interactive)
      (ignore-errors
        (funcall fn)))))


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;  windmove  - S-<arrow>   - S-<arrow> used by org so disabling this
;
;    use super-<arrow> (Win+arrow)
;
;(when (fboundp 'windmove-default-keybindings)
;  (windmove-default-keybindings))                  
; uses S-<arrow> which break org agenda functions so disabling

; super-<arrow> to move point to other windows within frame (windmove)
;
(global-set-key [s-left] (ignore-error-wrapper 'windmove-left))
(global-set-key [s-right] (ignore-error-wrapper 'windmove-right))
(global-set-key [s-up] (ignore-error-wrapper 'windmove-up))
(global-set-key [s-down] (ignore-error-wrapper 'windmove-down))


; want simple key to quickly rotate through windows/frames 
; hard to find a free key in all modes C-= fits my fingers, so quick 
; 
(global-set-key (kbd "C-=") 'next-window-any-frame) ; hard finding free key for all modes

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; winner - undo window config changes/redo
;
;  C-c <left> to go back to previous layout; (with repeat-mode <left> is repeatable)
;  C-c <right> to go back to recent layout; (not repeatable more than once.)

(winner-mode)



;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;  ace-window   M-o <num-on-screen>  -- move point to window <num>
;               C-u M-o <num>        -- swap window w/cur and num windmove
;               C-u C-u M-o <num>    -- delete window <num>
(use-package ace-window
   :ensure t
   :bind ("M-o" . ace-window)
   :custom-face (aw-background-face ((t (:foreground "plum"))))
   :config 
     (ace-window-display-mode 1)
)
(setq aw-dispatch-always t)

; this hydra uses ace-window ??
;(add-to-list 'aw-dispatch-alist '(?\; hydra-window-frame/body) t)

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;  rg - ripgrep display 
;
(with-eval-after-load 'rg
(add-to-list
 'display-buffer-alist
 '("\\*rg\\*" (lambda (buffer alist) 
                (select-window  (display-buffer-pop-up-window buffer alist)))))  )

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; embark window mgmt
;
(with-eval-after-load 'embark
  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none))))  )

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;  popper - managing popup windows - C-` M-` C-M-`
;
;(with-eval-after-load 'popper
;  (setq popper-display-function #'display-buffer-in-child-frame) 
;; popups display in child frame -- ugh - too weird on windows		  


;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;  org
;
(with-eval-after-load 'org
    (add-to-list 'display-buffer-alist
                '( (mode . org-mode)
                   (display-buffer-reuse-mode-window display-buffer-use-least-recent-window   
				    display-buffer-use-some-window)
				 ) ) )

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;  org-roam
;
(with-eval-after-load 'org-roam
    (add-to-list 'display-buffer-alist
                '( "\\*org-roam\\*"
                   (display-buffer-in-side-window)
                   (side . right)
                   (slot . 0)
                   (window-width . 0.25)
                   (window-parameters . ((no-delete-other-windows . t)))
				 )  ) )



; trying to get org-roam to only open links in org-roam-buffer into main window
; don't split main window!

; tried this as recommended on several sites, but doesn't seem to work. :( 
;(setq org-link-frame-setup
;  '((vm . vm-visit-folder-other-frame)
;    (vm-imap . vm-visit-imap-folder-other-frame)
;    (gnus . org-gnus-no-new-news)
;    (file . find-file)
;    (wl . wl-other-frame)))

; (inhibit-same-window . x)
;   nil - current window can be used; t - current window can't be used 

; (reusable-frames . x)
;   nil - only consider windows on current frame; t - any frame; visible - only visible frames 
;   0 - only windows on visible or iconified frames 
;   pass frame obj to restrict to that frame 

; (inhibit-switch-frame . x)
;    prevents non-current frame from being 'raised' if t 

; (window-width . x)
; (window-height . x)

; display-buffer-fallback-action:
;  display-buffer--maybe-same-window
;  display-buffer-reuse-window
;  display-buffer--maybe-pop-up-frame-or-window
;  display-buffer-in-previous-window
;  display-buffer-use-some-window
;  display-buffer-pop-up-frame

; (after-eval-load 'org-mode ??
; set display-buffer-alist or something so opening links in backlink buffer doesn't split windows!!
;;                '(".*\\.org"                            ;  (eq major-mode 'org-mode)     

; org/org-roam action function so that links that open buffers will open buffers reusing org-mode windows
; not splitting windows

(with-eval-after-load 'org-roam                         ; or org??
  (add-to-list 'display-buffer-alist
        '( (mode . (org-mode))
           (display-buffer-reuse-mode-window . ((mode . (org-mode)))) 
		   (reusable-frames . visible)  ) ) )
						   

;(defun his-tracing-function (orig-fun &rest args)
;  (let ((res (apply orig-fun args)))
;    (message "display-buffer returned %S" res)
;    res))
;
; enable:
; (advice-add 'display-buffer :around #'his-tracing-function)
; disable:
; (advice-remove 'display-buffer #'his-tracing-function)


; display-buffer - what is the mode of buffer from link-click 
;    so what is the display-buffer-alist that applies? Make sure it specifies display-buffer-reuse-mode-window
; 

; so clicking link in org-roam buffer, it decides to split the window.
;  debug on entry shows it in display-buffer-fallback-action 
;    so display-buffer-alist fails everything.  And it gets down to:
;      window--try-to-split-window
;      display-buffer-pop-up-window
;      display-buffer--maybe-pop-up-window
;      display-buffer--maybe-pop-up-frame-or-window
;
;  so these aren't working:
;    display-buffer-reuse-window
;    display-buffer-in-previous-window
;    display-buffer-same-window 

; need an action that reuses an existing org-mode window.
;    display-buffer-reuse-mode-window 

; display-buffer--maybe-same-window
;     display-buffer-reuse-window
;
; fails - along with display-buffer-alist - hmm

;; display-buffer-alist -- modifications on window management behavior

;; trying to keep Compile-Log in *one* window, and not keep splitting into multiple
(add-to-list 'display-buffer-alist
                '( "\\*Compile-Log\\*"
                   (display-buffer-same-window) ) )