;; UI setup
;;

(defun rgd/initial-frame-setup (&optional frame)
  "rgd: set the initial-frame-alist parameters - minimal acceptable settings, trying to match ones set in registry/~/.XResources"
  (progn
    (add-to-list 'initial-frame-alist '(height . 24))
    (add-to-list 'initial-frame-alist '(width . 80))  
	;;(add-to-list 'initial-frame-alist '(font . "Lucida Console"))  
  ;  (message "initial-frame-setup done")  - or get this to not show up in vertico-posframe...
  )
)



(defun rgd/default-frame-setup (&optional frame)
  "rgd: setup default-frame-alist parameters frame"
  (progn 
;	 (message "rgd/default-frame-setup")
	 
     ;; https://www.reddit.com/r/emacs/comments/25v0eo/you_emacs_tips_and_tricks/
     (setq frame-title-format '(buffer-file-name "Emacs: %b (%f)" "Emacs: %b"))
	 
 ;   (message "rgd/default-frame-setup - done") - or get this to not show up in vertico-posframe
  )
)


(declare-function rgd/pre-setup-frame emacs-ui-frame-el)
(declare-function rgd/post-setup-frame emacs-ui-frame-el)

(defun rgd/invoke-pre-setup-frame ()
	"called in before-make-frame-hook if in daemon mode; otherwise call rgd/setup-frame directly"
	(progn
;		(message "invoking pre frame setup code")
		
	    (rgd/pre-setup-frame (selected-frame))
	)
)

(defun rgd/invoke-post-setup-frame (frame)
	"called in after-make-frame-functions if in daemon mode; otherwise call rgd/setup-frame directly"
	(with-selected-frame frame
;		(message "invoking post frame setup code")

	    (rgd/post-setup-frame frame)
	)
)
